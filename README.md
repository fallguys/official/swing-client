# Swing Web Client

Proyecto de frontend para la aplicación web para cliente.
Se presenta la app que utilizarán los usuarios:

- administrador
- beneficiario de bono
- simulador de banco

## Instalación

**Obs:** Se usa `yarn` como gestor de paquetes.

### Dependencias

Instalar las dependencias:

```bash
yarn
```

### Variables de entorno

Configurar las variables de entorno del archivo `.env` usando el archivo `.env.example`:

Generar una copia del archivo `.env.example`:

```bash
cp .env.example .env
```

Ingresar las credenciales correctas en el archivo creado `.env`:

```.dotenv
# url de la api rest de producción, staging o desarrollo (remoto o local)
NEXT_PUBLIC_API_URL=http://localhost:3000 

# nombre de la aplación, será usada en los títulos de las páginas del navegador
NEXT_PUBLIC_APP_NAME="Sistema Swing"

# variable con el id de la incidencia para registrar puntos de retiro
NEXT_PUBLIC_APP_INCIDENT_ID="1"

# nombre del administrador responsable
NEXT_PUBLIC_ADMINISTRATOR_NAME="ROCHI SAN SAN"
```


### Modo desarrollo

Correr en modo desarrollo:

```bash
yarn dev
```

## Indicaciones para desarrollo

### Imports en código

La mayor parte del desarrollo debe realizarse dentro de la carpeta `src`.
Las importaciones del código deben realizarse iniciando desde la carpeta `src`.
Ejemplo:

```jsx
import styles from "styles/Home.module.css";
import HelloWorld from "components/HelloWorld";
```

### Resources (imágenes, audio, video, etc)

Los recursos deben ser guardados en la carpeta `public/` y ser referenciados
de la siguiente manera:

```jsx
return (
  <div>
    <img src="/imgs/my-image.png" alt="my image" />
  </div>
);
```

El archivo `my-image.png` se encuentra en la carpeta `public/imgs/`.
