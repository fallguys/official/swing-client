import * as services from './services/users';

const servicesControlAdministrator = {
  async getPosts(path) {
    const response = await services.get(path);
    return response;
  },
};

export default servicesControlAdministrator;
