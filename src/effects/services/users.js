import axios from 'axios';
import config from '../../config';

export async function get(path) {
  try {
    const response = await axios.get(`${config.api}/${path}`);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function post(path) {
  try {
    const response = await axios.post(`${config.api}/${path}`);
    return response;
  } catch (error) {
    console.error(error);
  }
}
