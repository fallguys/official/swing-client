import HomeIcon from '@material-ui/icons/Home';
import SyncIcon from '@material-ui/icons/Sync';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import FlagIcon from '@material-ui/icons/Flag';

const itemsContent = [
  { id: 1, view: 'home', name: 'Dashboard de cronograma actual', icon: (<HomeIcon />) },
  { id: 2, view: 'synch', name: 'Sincronización', icon: (<SyncIcon />) },
  { id: 3, view: 'schedule', name: 'Cronograma', icon: (<CalendarTodayIcon />) },
  { id: 4, view: 'incidents', name: 'Incidentes', icon: (<FlagIcon />) },
];

export default itemsContent;
