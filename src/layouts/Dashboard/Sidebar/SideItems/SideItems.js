import { List, ListItem, ListItemIcon, Tooltip } from '@material-ui/core';
import useStyles from './SideItems.styles';


function SideItems(props) {
  const { items, handleClick } = props;
  const classes = useStyles();

  return (
    <div className={classes.sideItems}>
      <List>
        {items.map((item, index) => (
          <ListItem
            button
            alignItems="center"
            key={item.id}
            onClick={() => handleClick(item)}
          >
            <ListItemIcon className={classes.item}>
              <Tooltip title={item.name} placement="right">
                {item.icon}
              </Tooltip>
            </ListItemIcon>
          </ListItem>
        ))}
      </List>
    </div>
  );
}

export default SideItems;
