import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  sideItems: {
    width: '22%',
    //maxWidth: theme.spacing(8) + 1,
    height: 'calc(100vh - 64px)',
    backgroundColor: theme.palette.tertiary.dark,
    color: theme.palette.common.white,
  },
  item: {
    padding: '1rem 0 1rem',
    color: theme.palette.common.white,
  },
  icon: {
    fontSize: '2.2rem',
    [theme.breakpoints.down('lg')]: {
      fontSize: '1rem',
    },
  },
}));

export default useStyles;
