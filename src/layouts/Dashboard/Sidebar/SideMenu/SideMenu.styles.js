import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  sideMenu: {
    width: '78%',
    //maxWidth: 300,
    //minWidth: 220,
    height: 'calc(100vh - 64px)',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  large: {
    width: theme.spacing(6),
    height: theme.spacing(6),
    color: theme.palette.common.white,
  },
}));

export default useStyles;
