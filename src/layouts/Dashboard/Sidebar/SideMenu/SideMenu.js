import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { Box } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Link from 'next/link';
import { useRouter } from 'next/router';
import config from 'config';
import useStyles from './SideMenu.styles';

function ListItemLink({ href, children, ...rest }) {
  const router = useRouter();

  return (
    <ListItem
      button
      component="span"
      onClick={() => router.replace(href)}
      {...rest}
    >
      {children}
    </ListItem>
  );
}

function SideMenu(props) {
  const { views, firstView, handleClick } = props;
  const classes = useStyles();
  const adminName = config.app.adminName;

  return (
    <Box className={classes.sideMenu}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem button>
          <ListItemAvatar>
            <Avatar
              className={classes.large}
              alt="User"
              src="https://images.unsplash.com/photo-1545696968-1a5245650b36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1317&q=80"
            />
          </ListItemAvatar>
          <ListItemText primary={adminName} secondary="Administrador" />
        </ListItem>
      </List>
      <Divider />
      <List component="nav" aria-label="secondary mailbox folders">
        {views[firstView].map((item) => {
          return (
            <ListItemLink key={item.id} href={item.path}>
              <ListItemText
                primary={item.name}
                onClick={() => handleClick(item)}
              />
            </ListItemLink>
          );
        })}
      </List>
    </Box>
  );
}

export default SideMenu;
