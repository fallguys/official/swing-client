const menuContent = {
  home: [
    { id: 1, path: '/admin/home', name: 'Dashboard' },
  ],
  synch: [
    { id: 1, path: '/admin/synch/points', name: 'Puntos de retiro' },
    { id: 2, path: '/admin/synch/benef', name: 'Beneficiarios' },
    { id: 3, path: '/admin/synch/areas', name: 'Zonas de contagio' },
    { id: 4, path: '/admin/synch/history', name: 'Historial' },
  ],
  schedule: [
    { id: 1, path: '/admin/schedule/history', name: 'Historial de cronogramas' },
    { id: 2, path: '/admin/schedule/new', name: 'Nuevo cronograma' },
  ],
  incidents: [
    { id: 1, path: '/admin/incident/reported-incidents', name: 'Incidencias reportadas' },
    { id: 2, path: '/admin/incident/incident-types', name: 'Tipos de incidencias' },
  ],
};

export default menuContent;
