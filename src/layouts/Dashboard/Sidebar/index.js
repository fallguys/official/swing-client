import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import SideItems from './SideItems/SideItems';
import SideMenu from './SideMenu/SideMenu';
import useStyles from './index.styles';
import itemsContent from './itemsContent';
import menuContent from './menuContent';
import { setFirstView, setSecondView } from '../../../redux/actions/layout/dashboardActions';

function Sidebar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const dashboard = useSelector((state) => state.dashboard);

  const handleClickItem = (item) => {
    dispatch(setFirstView(item.view, item.name));
  };

  const handleClickMenu = (view) => {
    dispatch(setSecondView(view.path, view.name));
  };

  return (
    <Box display="flex" className={classes.root}>
      <SideItems items={itemsContent} handleClick={handleClickItem} />
      <SideMenu views={menuContent} firstView={dashboard.firstView} handleClick={handleClickMenu} />
    </Box>
  );
}

export default Sidebar;
