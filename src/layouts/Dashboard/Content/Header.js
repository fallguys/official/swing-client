import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import Typography from '@material-ui/core/Typography';
import React from 'react';
// import { useSelector } from 'react-redux';

function Header(props) {
  // const dashboard = useSelector((state) => state.dashboard);
  const { headers = [] } = props;

  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
      {/*<Typography color="inherit">{dashboard.header.view1}</Typography>*/}
      {/*<Typography color="textPrimary">{dashboard.header.view2}</Typography>*/}
      { headers.map((item) => (
        <Typography key={item} color="inherit">
          { item }
        </Typography>
      ))}
    </Breadcrumbs>
  );
}

export default Header;
