import React from 'react';
import { Box } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Header from './Header';
import useStyles from './Content.styles';

function Content(props) {
  const { headers, children } = props;
  const classes = useStyles();

  return (
    <Box p={2} width={1} className={classes.content}>
      <Header headers={headers} />
      <Divider />
      {children}
    </Box>
  );
}

export default Content;
