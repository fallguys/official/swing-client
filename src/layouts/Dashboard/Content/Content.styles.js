import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  content: {
    height: 'calc(100vh - 64px)',
    //backgroundColor: theme.palette.background.paper,
    overflow: 'auto',
  },
}));

export default useStyles;
