import React from 'react';
import Head from 'next/head';
import Navbar from 'layouts/Dashboard/Navbar';
import Content from 'layouts/Dashboard/Content';
import config from 'config';
import { Box } from '@material-ui/core';
import Sidebar from './Sidebar';

class Dashboard extends React.Component {
  render() {
    const { children, title, headers } = this.props;
    const barTitle = `${title} - ${config.app.name}`;

    return (
      <div>
        <Head>
          <title>{barTitle}</title>
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
          <link rel="manifest" href="/site.webmanifest" />
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff" />
        </Head>

        <Navbar />
        <Box display="flex">
          <Sidebar />
          <Content headers={headers}>
            {children}
          </Content>
        </Box>
      </div>
    );
  }
}

export default Dashboard;
