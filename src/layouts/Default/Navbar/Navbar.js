import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import useStyles from './Navbar.styles';

export default function DenseAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar variant="dense">
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            {/*<MenuIcon />*/}
            <img src="/imgs/swing_logo.svg" width="40px" height="40px" alt="S" />
          </IconButton>
          <Typography variant="h6" color="inherit">
            Swing
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
