import axios from 'axios';
import config from 'config';

const chooseWithdrawalPointService = {
  getPlace: async (id, type) => {
    const url = `${config.api.baseUrl}/ubigees`;
    const res = await axios.get(url, { params: { id, type } });
    return res;
  },
  getWithdrawalPoints: async (ubigee) => {
    const url = `${config.api.baseUrl}/withdrawalPoints/ubigee`;
    const res = await axios.get(url, { params: { ubigee } });
    return res;
  },
  postWithdrawalPoints: async (data) => {
    const url = `${config.api.baseUrl}/selectWithdrawalPoints`;
    const res = await axios.post(url, data);
    return res;
  },
};

export default chooseWithdrawalPointService;
