import axios from 'axios';
import config from 'config';

const consultService = {
  getInfoBonus: async (code) => {
    const url = `${config.api.baseUrl}/infoBonus`;
    const res = await axios.get(url, { params: { code } });
    return res;
  },
};

export default consultService;
