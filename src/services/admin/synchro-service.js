import axios from 'axios';
import config from 'config';

const synchroService = {
  getSynchroDetails: async (idSynchro) => {
    const url = `${config.api.baseUrl}/synchroDetail?id=${idSynchro}`;
    const res = await axios.get(url);
    return res;
  },
  sendSynchroData: async (url, datafile) => {
    const formData = new FormData();
    formData.append('file', datafile);
    const res = await axios.post(url, formData, { headers: { 'Content-Type': 'multipart/form-data' } });
    return res;
  },
};

export default synchroService;
