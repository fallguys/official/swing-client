import axios from 'axios';
import config from 'config';

const homeService = {
  getHomeDetails: async () => {
    const url = `${config.api.baseUrl}/dashboard`;
    const res = await axios.get(url);
    return res;
  },
};

export default homeService;
