import axios from 'axios';
import config from 'config';
import FileDownload from 'js-file-download';

const scheduleService = {
  getScheduleByType: async ({ type, page, pagesize }) => {
    const url = `${config.api.baseUrl}/schedules`;
    const res = await axios.get(url, { params: { type, page, pagesize } });
    return res;
  },
  getCheckForInfo: async () => {
    const url = `${config.api.baseUrl}/schedules/checking`;
    const res = await axios.get(url);
    return res;
  },
  postNewSchedule: async (schedule) => {
    console.log(schedule);
    const url = `${config.api.baseUrl}/schedules`;
    const res = await axios.post(url, schedule);
    return res;
  },
  getScheduleDetailBeneficiary: async ({ codSchedule, page, pagesize, initialDate, finalDate, beneficiaryCode, ubigee, withdrawalPointCode, state }) => {
    const url = `${config.api.baseUrl}/schedule/beneficiaries`;
    const res = await axios.get(url, { params: { scheduleId: codSchedule, page, pagesize, initialDate, finalDate, beneficiaryCode, withdrawalPointCode, ubigee, state } });
    return res;
  },
  getScheduleDetailWithdrawal: async ({ codSchedule, page, pagesize, ubigee, withdrawalPointCode }) => {
    const url = `${config.api.baseUrl}/schedules/withdrawalPoint`;
    const res = await axios.get(url, { params: { scheduleId: codSchedule, page, pagesize, ubigee, withdrawalPointCode } });
    return res;
  },
  getScheduleDetailInformationBox: async (codSchedule) => {
    const url = `${config.api.baseUrl}/schedule_details`;
    const res = await axios.get(url, { params: { scheduleId: codSchedule } });
    return res;
  },
  getScheduleBeneficiaryReport: async (codSchedule, type, selectedInitialDate, selectedFinalDate, benefCode, state, ubigee, withdrawalPointCode) => {
    const url = `${config.api.baseUrl}/schedule/beneficiaries/export`;
    const res = await axios.get(url,
      { responseType: 'blob',
        headers: { ContentType: 'application/octet-stream', ContentDisposition: 'attachment', Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', responseType: 'blob' },
        params: { scheduleId: codSchedule, beneficiaryCode: benefCode, withdrawalPointCode, ubigee, state, initialDate: selectedInitialDate, finalDate: selectedFinalDate, type } });
    return res;
  },
  getScheduleWithdrawalReport: async (codSchedule, type, ubigee, withdrawalPointCode) => {
    const url = `${config.api.baseUrl}/schedule/withdrawalPoint/export`;
    const res = await axios.get(url,
      { responseType: 'blob',
        headers: { ContentType: 'application/octet-stream', ContentDisposition: 'attachment', Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', responseType: 'blob' },
        params: { scheduleId: codSchedule, type, ubigee, withdrawalPointCode } });
    return res;
  },
};

export default scheduleService;
