import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import * as locales from '@material-ui/core/locale';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  container: {
    maxHeight: 1200,
  },
}));

const ColumnsHeader = ({ columns }) => (
  <TableHead>
    <TableRow>
      {columns.map((column) => (
        <TableCell
          key={column.id}
          align={column.align}
          style={{ minWidth: column.minWidth }}
        >
          {column.label}
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
);

const RowsBody = ({ rows = [], columns }) => {
  return (
    <TableBody>
      {rows?.map((row) => {
        return (
          <TableRow key={row.id} hover role="checkbox" tabIndex={-1}>
            {columns.map((column) => {
              const value = row[column.id];
              return (
                <TableCell key={column.id} align={column.align}>
                  {column.format ? column.format(value) : value}
                </TableCell>
              );
            })}
          </TableRow>
        );
      })}
    </TableBody>
  );
};

function DataTable(props) {
  const classes = useStyles();
  const { rows, columns, total = 0, onChangePage, pageSize } = props;
  const [page, setPage] = React.useState(0);
  const [locale, setLocale] = React.useState('esES');

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    onChangePage(newPage, pageSize);
  };

  return (
    <ThemeProvider theme={(outerTheme) => createMuiTheme(outerTheme, locales[locale])}>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <ColumnsHeader columns={columns} />
            <RowsBody rows={rows} columns={columns} />
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10]}
          component="div"
          count={total}
          rowsPerPage={pageSize ?? 10}
          page={page}
          onChangePage={handleChangePage}
        />
      </Paper>
    </ThemeProvider>
  );
}

export default DataTable;
