import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
  { id: 'code', label: 'Código', minWidth: 40 },
  { id: 'dep', label: 'Departamento', minWidth: 100 },
  { id: 'prov', label: 'Provincia', minWidth: 100 },
  { id: 'dist', label: 'Distrito', minWidth: 100 },
  { id: 'ubigee', label: 'Ubigeo', minWidth: 100 },
  { id: 'contagion', label: 'Contagio(%)', minWidth: 50 },
  { id: 'register', label: 'Fecha de registro', minWidth: 120 },
  { id: 'actua', label: 'Fecha de actualización', minWidth: 100 },
];

const rows = [
  { code: 1, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 2, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 3, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 4, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 5, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 6, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 7, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 8, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 9, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },
  { code: 10, dep: 'Lima', prov: 'Lima', dist: 'Lima', ubigee: 512525, contagion: 13, register: '15/11/2020', actua: '15/11/2020' },

];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  container: {
    maxHeight: 262,
  },
}));

function BeneficiarieTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}

export default BeneficiarieTable;
