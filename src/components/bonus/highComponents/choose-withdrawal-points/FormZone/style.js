import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: theme.spacing(5),
    [theme.breakpoints.down('md')]: {
      margin: theme.spacing(1),
    },
  },
  container: {
    width: '100%',
    padding: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      width: '70%',
      padding: theme.spacing(2),
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      padding: theme.spacing(1),
    },
  },
  btn: {
    marginTop: theme.spacing(2),
  },
}));

export default useStyles;
