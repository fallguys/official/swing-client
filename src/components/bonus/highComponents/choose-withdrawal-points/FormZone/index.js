import React, { useEffect, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { Button, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import BeneficiaryCodeInput from '../../../lowComponents/inputs/BeneficiaryCodeInput';
import UbicationSelect from '../../../lowComponents/selects/UbicationSelect';
import useStyles from './style';
import { fetchDepartment, fetchDistrict, fetchProvince, fetchWithdrawalPoints, setFamilyCode } from '../../../../../redux/actions/bonus/chooseWithdrawalPointActions';


export default function FormZone(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const chooseWithdrawalPoint = useSelector((state) => state.chooseWithdrawalPoint);
  const [zone, setZone] = useState({
    department: null,
    province: null,
    district: null,
  });

  const { department, province, district } = zone;

  useEffect(() => {
    dispatch(fetchDepartment());
  }, []);

  useEffect(() => {
    if (department !== null) {
      dispatch(fetchProvince(department));
    }
  }, [department]);

  useEffect(() => {
    if (province !== null) {
      dispatch(fetchDistrict(province));
    }
  }, [province]);

  function handleSubmit(e) {
    e.preventDefault();
    dispatch(fetchWithdrawalPoints(district));
  }

  return (
    <Paper className={classes.container} variant="outlined">
      <form className={classes.form} onSubmit={handleSubmit}>
        <Typography variant="h4" align="center">
          Buscar puntos de retiro
        </Typography>
        <BeneficiaryCodeInput onChange={(e) => dispatch(setFamilyCode(e.target.value))} />
        <UbicationSelect
          id="select-department"
          text="Departamento"
          items={chooseWithdrawalPoint.department.data}
          onChange={(e) => setZone({ ...zone, department: e.target.value })}
        />
        <UbicationSelect
          id="select-province"
          text="Provincia"
          items={chooseWithdrawalPoint.province.data}
          onChange={(e) => setZone({ ...zone, province: e.target.value })}
        />
        <UbicationSelect
          id="select-district"
          text="Distrito"
          items={chooseWithdrawalPoint.district.data}
          onChange={(e) => {
            setZone({ ...zone, district: e.target.value });
          }}
        />
        <Button
          color="primary"
          type="submit"
          variant="contained"
          size="large"
          className={classes.btn}
        >
          Buscar puntos de retiros
        </Button>
      </form>
    </Paper>
  );
}
