import Checkbox from '@material-ui/core/Checkbox';
import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useDispatch, useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import WithdrawalPointsTable from '../../../lowComponents/tables/WithdrawalPointsTable';
import useStyles from './style';
import { postSelectedPoints, setSelectedPointsError, setSelectedPointsSuccess } from '../../../../../redux/actions/bonus/chooseWithdrawalPointActions';
import SelectWithdrawalPointsSanckbar from '../../../lowComponents/snackbars/SelectWithdrawalPointsSanckbar';

export default function RetirementPointsZone(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const chooseWithdrawalPoint = useSelector((state) => state.chooseWithdrawalPoint);
  const { withdrawalPoints, familyCode, selectedPoints } = chooseWithdrawalPoint;
  const [checked, setChecked] = React.useState({});
  const [rows, setRows] = React.useState([]);

  const handleChange = (event) => {
    let count = 0;
    Object.keys(checked).reduce((r, e) => {
      if (checked[e] === true) count = count + 1;
    }, {});
    if (count === 3 && event.target.checked === false) {
      setChecked({
        ...checked,
        [event.target.value]: event.target.checked,
      });
      count = count - 1;
    } else if (count <= 2) {
      setChecked({
        ...checked,
        [event.target.value]: event.target.checked,
      });
      count = count + 1;
    }
  };

  useEffect(() => {
    if (withdrawalPoints.data.length !== 0) {
      let checks = {};
      withdrawalPoints.data.forEach((item) => {
        checks = {
          ...checks,
          [item.code]: false,
        };
      });
      setChecked(checks);
    }
  }, [withdrawalPoints.data]);

  useEffect(() => {
    if (checked !== {}) {
      const aux = withdrawalPoints.data.map((item) => {
        return {
          id: item.code,
          name: item.name,
          address: item.address,
          selected: (<Checkbox value={item.code} checked={checked[item.code]} onChange={handleChange} />),
        };
      });
      setRows(aux);
    }
  }, [checked]);

  function handleSubmit(e) {
    e.preventDefault();
    let count = 0;
    const points = [];
    Object.keys(checked).reduce((r, e) => {
      if (checked[e] === true) {
        count = count + 1;
        points.push(e);
      };
    }, {});
    if (count === 3) {
      const data = {
        beneficiaryCode: familyCode,
        withdrawalPointCodes: points,
      };
      dispatch(postSelectedPoints(data));
    } else if (count <= 2 && count !== 0) {
      for (let i = 0; i < 3 - count; i++) {
        points.push('-1');
      }
      const data = {
        beneficiaryCode: familyCode,
        withdrawalPointCodes: points,
      };
      dispatch(postSelectedPoints(data));
    } else {
      dispatch(setSelectedPointsError(true, 'Seleccione al menos un punto de retiro'))
    }
  }

  return (
    <>
      <SelectWithdrawalPointsSanckbar
        severity="success"
        open={selectedPoints.success.status}
        handleClose={() => dispatch(setSelectedPointsSuccess(false))}
      >
        Se registraron correctamente los puntos de retiro
      </SelectWithdrawalPointsSanckbar>
      <SelectWithdrawalPointsSanckbar
        severity="error"
        open={selectedPoints.error.status}
        handleClose={() => dispatch(setSelectedPointsError(false))}
      >
        {selectedPoints.error.response}
      </SelectWithdrawalPointsSanckbar>
      <Paper variant="outlined">
        <form className={classes.container} onSubmit={handleSubmit}>
          {withdrawalPoints.loading && <LinearProgress />}
          <Typography variant="h5" align="center">
            Puntos de retiro
          </Typography>
          {withdrawalPoints.data.length !== 0 ?
            (
              <Typography variant="subtitle1" align="center">
                Eliga un máximo de 3 puntos de retiro
              </Typography>
            ) : (
              <Typography variant="subtitle1" align="center">
                Complete su código de familia. Luego, seleccione un departamento, provincia y distrito para buscar puntos de retiro
              </Typography>
            )}
          {withdrawalPoints.loading && <CircularProgress />}
          {withdrawalPoints.data.length !== 0 && <WithdrawalPointsTable rows={rows} className={classes.table} />}
          {withdrawalPoints.data.length !== 0 && (
            <Button type="submit" variant="contained" color="primary" size="large">
              Seleccionar puntos de retiro
            </Button>
          )}
        </form>
      </Paper>
    </>
  );
}
