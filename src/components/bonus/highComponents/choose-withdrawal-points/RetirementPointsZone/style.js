import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    margin: theme.spacing(2),
    width: '95%',
    maxHeight: '320px',
    [theme.breakpoints.down('md')]: {
      margin: theme.spacing(1),
      width: '100%',
    },
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(1),
      width: '100%',
      maxHeight: '100%',
    },
  },
  container: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
  },
}));

export default useStyles;
