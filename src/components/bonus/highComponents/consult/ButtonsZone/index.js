import React from 'react';
import { Box } from '@material-ui/core';
import { useRouter } from 'next/router';
import IncidentButton from '../../../lowComponents/buttons/IncidentButton';
// import WithdrawalPointButton from '../../../lowComponents/buttons/WithdrawalPointButton';

export default function ButtonsZone() {
  const router = useRouter();

  return (
    <Box display="flex" justifyContent="center" alignItems="center" mb={5} flexWrap="wrap">
      <Box m={2}>
        <IncidentButton
          onClick={() => router.replace('/bonus/incident')}
        >
          Reportar una incidencia
        </IncidentButton>
      </Box>
      {/*<Box m={2}>*/}
      {/*  <WithdrawalPointButton*/}
      {/*    onClick={() => router.replace('/bonus/choose-withdrawal-points')}*/}
      {/*  >*/}
      {/*    Elegir punto de retiro*/}
      {/*  </WithdrawalPointButton>*/}
      {/*</Box>*/}
    </Box>
  );
}
