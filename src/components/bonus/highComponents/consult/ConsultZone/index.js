import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchScheduleData, setFamilyCode, setScheduleError } from '../../../../../redux/actions/bonus/consultActions';
import ConsultSearch from '../../../lowComponents/searchs/ConsultSearch';
import ConsultButton from '../../../lowComponents/buttons/ConsultButton';
import ConsultFailure from '../../../lowComponents/snackbars/ConsultFailure';

export default function ConsultZone(props) {
  const dispatch = useDispatch();
  const consult = useSelector((state) => state.bonusConsult);

  function handleSubmit(e) {
    e.preventDefault();
    dispatch(fetchScheduleData(consult.familyCode));
  }

  return (
    <>
      <ConsultFailure
        open={consult.schedule.error.status}
        handleClose={() => dispatch(setScheduleError(false))}
      >
        Código erroneo
      </ConsultFailure>
      <Grid container direction="column" spacing={5} justify="center" alignItems="center">
        <Grid item xs={12}>
          <Typography variant="h2" align="center">
            Consulta aquí si eres beneficiario del bono del estado
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <form autoComplete="off" onSubmit={handleSubmit}>
            <Grid container direction="column" spacing={3} justify="center" alignItems="center">
              <Grid item xs={12}>
                <ConsultSearch
                  label="Ingresar codigo de hogar"
                  onChange={(event) => dispatch(setFamilyCode(event.target.value))}
                />
              </Grid>
              <Grid item xs={12}>
                <ConsultButton type="submit">
                  Consultar
                </ConsultButton>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    </>
  );
}
