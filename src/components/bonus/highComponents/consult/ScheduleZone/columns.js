const withdrawData = [
  { id: 1, rowName: 'Punto de retiro', value: null },
  { id: 2, rowName: 'Dirección', value: null },
  { id: 3, rowName: 'Fecha asignada', value: null },
  { id: 4, rowName: 'Hora asignada', value: null },
];

export default withdrawData;
