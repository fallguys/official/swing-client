import { useSelector } from 'react-redux';
import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import ScheduleTable from '../../../lowComponents/tables/ScheduleTable';

export default function ScheduleZone(props) {
  const consult = useSelector((state) => state.bonusConsult);
  const aux = consult.schedule.data?.message === 'Ok' ? consult.schedule.data : null;

  return (
    <>
      {
        consult.schedule.loading && <CircularProgress />
      }
      {
        aux !== null && (
          <ScheduleTable data={aux} />
        )
      }
    </>
  );
}
