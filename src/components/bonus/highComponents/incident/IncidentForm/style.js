import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '10px',
    borderColor: theme.palette.primary,
    padding: theme.spacing(40),
    paddingTop: theme.spacing(10),
    border: 2,
    margin: theme.spacing(5),
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(1),
      padding: theme.spacing(1),
    },
  },
  linear: {
    width: '100%',
  },
}));

export default useStyles;
