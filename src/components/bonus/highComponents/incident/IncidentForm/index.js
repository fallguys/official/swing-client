import { Box, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import BeneficiaryCodeInput from '../../../lowComponents/inputs/BeneficiaryCodeInput';
import IncidentTypeSelect from '../../../lowComponents/selects/IncidentTypeSelect';
import MessageInput from '../../../lowComponents/inputs/MessageInput';
import useStyles from './style';
import ReportIncidentButton from '../../../lowComponents/buttons/ReportIncidentButton';
import { fetchIncidentsTypes, postReportIncident, setAlertError, setAlertSuccess, setFamilyCode, setIncidentType, setMessage } from '../../../../../redux/actions/bonus/reportIncidentActions';
import ReportIncidentFailure from '../../../lowComponents/snackbars/ReportIncidentFailure';
import ReportIncidentSuccess from '../../../lowComponents/snackbars/ReportIncidentSuccess';
import IncidentDialog from '../../../lowComponents/dialogs/IncidentDialog';
import { useRouter } from 'next/router';
import config from '../../../../../config';

export default function IncidentForm() {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();
  const reportIncident = useSelector((state) => state.reportIncident);

  useEffect(() => {
    dispatch(fetchIncidentsTypes());
    console.log(config.app.incidentID);
    console.log(config.app.name);
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();
    const data = {
      code: reportIncident.familyCode,
      incidentTypeId: reportIncident.form.data.incidentTypeId,
      message: reportIncident.form.data.message,
    };
    await dispatch(postReportIncident(data));
  }

  return (
    <Box>
      <ReportIncidentFailure
        open={reportIncident.form.alertError}
        handleClose={() => dispatch(setAlertError(false))}
      />
      { reportIncident.form.data.incidentTypeId !== config.app.incidentID && (
        <ReportIncidentSuccess
          open={reportIncident.form.alertSuccess}
          handleClose={() => dispatch(setAlertSuccess(false))}
        />
      )}
      {
        reportIncident.form.loading && <LinearProgress />
      }
      { reportIncident.form.data.incidentTypeId === config.app.incidentID && (
        <IncidentDialog
          open={reportIncident.form.alertSuccess}
          handleClose={() => dispatch(setAlertSuccess(false))}
          handleSi={() => router.replace('/bonus/choose-withdrawal-points')}
        />
      )}
      <form className={classes.form} onSubmit={handleSubmit}>
        <Typography variant="h4" align="center">
          Reportar una incidencia
        </Typography>
        <BeneficiaryCodeInput
          onChange={(e) => dispatch(setFamilyCode(e.target.value))}
        />
        <IncidentTypeSelect
          items={reportIncident.incidentsTypes.data}
          onChange={(e) => dispatch(setIncidentType(e.target.value))}
        />
        <MessageInput
          onChange={(e) => dispatch(setMessage(e.target.value))}
        />
        <ReportIncidentButton type="submit">
          Enviar incidencia
        </ReportIncidentButton>
      </form>
    </Box>
  );
}
