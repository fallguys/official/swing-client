import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import React from 'react';

export default function WithdrawalPointButton(props) {
  const { children, ...rest } = props;

  return (
    <Button
      variant="outlined"
      color="primary"
      size="large"
      startIcon={<LocationOnIcon />}
      {...rest}
    >
      { children }
    </Button>
  );
}
