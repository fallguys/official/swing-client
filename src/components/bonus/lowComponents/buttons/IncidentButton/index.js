import ReportProblemIcon from '@material-ui/icons/ReportProblem';
import Button from '@material-ui/core/Button';
import React from 'react';

export default function IncidentButton(props) {
  const { children, ...rest } = props;

  return (
    <Button
      variant="outlined"
      color="primary"
      size="large"
      startIcon={<ReportProblemIcon />}
      {...rest}
    >
      { children }
    </Button>
  );
}
