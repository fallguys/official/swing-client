import Button from '@material-ui/core/Button';
import React from 'react';

export default function ConsultButton(props) {
  const { children, ...rest } = props;
  return (
    <Button
      variant="contained"
      color="primary"
      size="large"
      {...rest}
    >
      { children }
    </Button>
  );
}
