import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles({
  table: {
    minWidth: 'auto',
  },
});

export default useStyles;
