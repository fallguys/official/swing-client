import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    width: '30%',
    [theme.breakpoints.down('sm')]: {
      width: '95%',
    },
  },
}));

export default useStyles;
