import Box from '@material-ui/core/Box';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import useStyles from './style';
import { formatOnlyDate, formatOnlyTime } from '../../../../../utils/format/timer';

export default function ScheduleTable(props) {
  const { data } = props;
  const classes = useStyles();

  return (
    <Box className={classes.table}>
      <TableContainer component={Paper}>
        <Table aria-label="Benficiarie table">
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="h6">
                  Punto de retiro
                </Typography>
              </TableCell>
              <TableCell>
                {data.withdrawalPointName}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="h6">
                  Dirección
                </Typography>
              </TableCell>
              <TableCell>
                {data.address}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="h6">
                  Fecha asignada
                </Typography>
              </TableCell>
              <TableCell>
                {formatOnlyDate(data.assignedInitialDate)}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="h6">
                  Hora asignada
                </Typography>
              </TableCell>
              <TableCell>
                {formatOnlyTime(data.assignedInitialDate)}
                {' - '}
                {formatOnlyTime(data.assignedFinalDate)}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}
