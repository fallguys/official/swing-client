import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import React from 'react';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function ReportIncidentFailure(props) {
  const { open, handleClose } = props;
  const vertical = 'top';
  const horizontal = 'center';

  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
      anchorOrigin={{ vertical, horizontal }}
    >
      <Alert onClose={handleClose} severity="error">
        Ocurrió un error
      </Alert>
    </Snackbar>
  );
}
