import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import React from 'react';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function ReportIncidentSuccess(props) {
  const { open, handleClose } = props;
  const vertical = 'top';
  const horizontal = 'center';

  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
      anchorOrigin={{ vertical, horizontal }}
    >
      <Alert onClose={handleClose} severity="success">
        El incidente se registró correctamente
      </Alert>
    </Snackbar>
  );
}
