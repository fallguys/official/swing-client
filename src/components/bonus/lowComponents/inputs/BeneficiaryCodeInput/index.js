import React from 'react';
import TextField from '@material-ui/core/TextField';
import useStyles from './style';

export default function BeneficiaryCodeInput(props) {
  const classes = useStyles();

  return (
    <TextField
      id="outlined-basic-family-code"
      label="Código de familia"
      variant="outlined"
      required
      fullWidth
      className={classes.root}
      {...props}
    />
  );
}
