import TextField from '@material-ui/core/TextField';
import React from 'react';
import useStyles from './style';

export default function MessageInput(props) {
  const classes = useStyles();

  return (
    <TextField
      id="outlined-multiline-static-message"
      label="Mensaje"
      multiline
      rows={4}
      fullWidth
      variant="outlined"
      required
      className={classes.root}
      {...props}
    />
  );
}
