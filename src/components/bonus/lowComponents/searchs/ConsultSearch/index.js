import TextField from '@material-ui/core/TextField';
import React from 'react';
import useStyles from './style';

export default function ConsultSearch(props) {
  const classes = useStyles();

  return (
    <TextField
      className={classes.input}
      //onChange={(event) => dispatch(setFamilyCode(event.target.value))}
      id="outlined-basic"
      label="Ingresar codigo de hogar"
      variant="outlined"
      {...props}
    />
  );
}
