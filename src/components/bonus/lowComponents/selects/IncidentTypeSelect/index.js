import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import React from 'react';
import useStyles from './style';

export default function IncidentTypeSelect(props) {
  const { items = [], ...rest } = props;
  const classes = useStyles();

  return (
    <FormControl variant="outlined" fullWidth className={classes.root}>
      <InputLabel id="simple-select-incident-label">Tipo de incidente*</InputLabel>
      <Select
        native
        id="simple-select-incident"
        label="Tipo de incidente"
        required
        {...rest}
      >
        <option aria-label="None" value="" />
        {
          items.map((item) => (
            <option key={item.id} value={item.id}>
              {item.name}
            </option>
          ))
        }
      </Select>
    </FormControl>
  );
}
