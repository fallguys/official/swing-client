import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import React from 'react';
import useStyles from './style';

export default function UbicationSelect(props) {
  const { items = [], text, ...rest } = props;
  const classes = useStyles();

  return (
    <FormControl variant="outlined" fullWidth className={classes.root}>
      <InputLabel id="simple-select-incident-label">
        { text }
      </InputLabel>
      <Select
        native
        id="simple-select-ubication"
        label={text}
        required
        {...rest}
      >
        <option aria-label="None" value="" />
        {
          items.map((item) => (
            <option key={item.id} value={text === 'Distrito' ? item.code : item.id} name={item.code}>
              {item.name}
            </option>
          ))
        }
      </Select>
    </FormControl>
  );
}
