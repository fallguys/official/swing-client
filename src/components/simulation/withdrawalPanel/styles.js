import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '425px',
    textAlign: 'center',
    background: '#ECECEC',
    marginTop: theme.spacing(6),
    borderRadius: theme.shape.borderRadius,
  },
}));

export default useStyles;
