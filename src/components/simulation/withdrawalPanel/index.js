import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchWithdrawalData,
  setInitialBeneficiarie,
  setInitialWithdrawal,
  setShow,
  setWithdrawalCode
} from 'redux/actions/bank/bankActions';
import SearchField from '../search/searchField';
import useStyles from './styles';
import Results from '../results';
import AlertSimulation from '../alert';

export default function WithdrawalPanel() {
  const classes = useStyles();
  const bankConsult = useSelector((state) => state.bank);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchWithdrawalData(0));
  }, []);

  return (
    <Grid item xs={3}>
      <div className={classes.root}>
        <SearchField
          name="Ingrese el código del punto de retiro"
          type="withdrawal"
          onChange={(event) => {
            dispatch(fetchWithdrawalData(event.target.value));
            dispatch(setShow(false));
            dispatch(setInitialWithdrawal(false));
          }}
        />
        {
          bankConsult.withdrawal.loading && <CircularProgress />
        }
        {
          !bankConsult.initialWithdrawal && bankConsult.withdrawal.error === '' && (
            <Results namePoint={bankConsult.withdrawal.data.name} addressPoint={bankConsult.withdrawal.data.address} type="Dirección" />
          )
        }
        {
          !bankConsult.initialWithdrawal && bankConsult.withdrawal.error !== '' && (
            <AlertSimulation text="No existe un Punto de retiro para este código" />
          )
        }
      </div>
    </Grid>
  );
};
