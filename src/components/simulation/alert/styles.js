import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  alertWithdrawal: {
    width: '80%',
    marginLeft: theme.spacing(4),
  },
  alertBeneficiarie: {
    width: '80%',
    marginLeft: theme.spacing(7),
  },
}));

export default useStyles;
