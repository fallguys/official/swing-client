import React from 'react';
import Alert from '@material-ui/lab/Alert';
import useStyles from './styles';

function getStyle(type) {
  const classes = useStyles();
  if (type === 'Beneficiarie') return classes.alertBeneficiarie;
  return classes.alertWithdrawal;
}

export default function AlertSimulation(props) {
  const { text, type, retirement } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {!retirement && (
        <Alert className={getStyle(type)} severity="error">{ text }</Alert>
      )}
      {retirement && (
        <Alert className={getStyle(type)} severity="info">{ text }</Alert>
      )}
    </div>
  );
}
