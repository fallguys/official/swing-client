import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchBeneficiarieData, fetchRegisterRetirement, setInitialBeneficiarie, setRetired,
  setShow,
  fetchRegisterIncidence, setChanged,
} from 'redux/actions/bank/bankActions';
import { formatOnlyDate } from 'utils/format/timer';
import { useSnackbar } from 'notistack';
import useStyles from './styles';
import SearchField from '../search/searchField';
import Results from '../results';
import SearchButton from '../search/searchButton';
import AlertSimulation from '../alert';

export default function BeneficiariePanel() {
  const classes = useStyles();
  const bankConsult = useSelector((state) => state.bank);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    dispatch(fetchBeneficiarieData(0, 0));
  }, []);

  return (
    <Grid item xs={5}>
      <div className={classes.root}>
        <SearchField
          id="SearchBeneficiarie"
          name="Ingrese el código del beneficiario"
          type="beneficiarie"
          onChange={(event) => {
            dispatch(setRetired(false));
            dispatch(fetchBeneficiarieData(event.target.value, bankConsult.withdrawalCode));
            dispatch(setShow(true));
            dispatch(setInitialBeneficiarie(false));
            dispatch(setChanged(false));
          }}
          value={bankConsult.changed && ('')}
        />
        {
          bankConsult.beneficiarie.loading && <div className={classes.loading}><CircularProgress /></div>
        }
        {
          !bankConsult.initialBeneficiarie && bankConsult.beneficiarie.error === '' && !bankConsult.retired && bankConsult.show && (
            <div>
              <Results
                namePoint={
                  bankConsult.beneficiarie.data.namePointAssigned
                }
                addressPoint={`Desde: ${formatOnlyDate(bankConsult.beneficiarie.data.initialDateAssigned)} - Hasta: ${formatOnlyDate(bankConsult.beneficiarie.data.finalDateAssigned)}`}
                type="Fecha de retiro:"
              />
              { bankConsult.beneficiarie.data.isPointAssigned && bankConsult.beneficiarie.data.isValidateDate && (
                <SearchButton
                  type="Cobro"
                  onClick={() => {
                    dispatch(fetchRegisterRetirement(bankConsult.beneficiarieCode, bankConsult.withdrawalCode));
                    dispatch(setShow(false));
                    enqueueSnackbar('Se registró el cobro exitosamente', { variant: 'sucess' });
                    dispatch(setChanged(true));
                  }}
                />
              )}
              { (bankConsult.beneficiarie.data.isPointAssigned !== true || bankConsult.beneficiarie.data.isValidateDate !== true) && (
                <SearchButton
                  type="Incidencia"
                  onClick={(e) => {
                    dispatch(fetchRegisterIncidence(bankConsult.beneficiarieCode, bankConsult.withdrawalCode));
                    dispatch(setShow(false));
                    enqueueSnackbar('Se registró la incidencia exitosamente', { variant: 'success' });
                    dispatch(setChanged(true));
                  }}
                />
              )}
            </div>
          )
        }
        {
          !bankConsult.initialBeneficiarie && bankConsult.beneficiarie.error !== '' && !bankConsult.retired && (
            <AlertSimulation text="No se ha encontrado un beneficiario para este código" type="Beneficiarie" retirement={false} />
          )
        }
        {
          !bankConsult.initialBeneficiarie && bankConsult.beneficiarie.error === '' && bankConsult.retired && (
            <AlertSimulation text="Este beneficiario ya cobró su bono" type="Beneficiarie" retirement={true} />
          )
        }
      </div>
    </Grid>
  );
};
