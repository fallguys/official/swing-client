import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';

const currentdate = new Date();
const currentTime = `${
  currentdate.getDate()}/${
  currentdate.getMonth() + 1}/${
  currentdate.getFullYear()}`;

class Clock extends Component {
  constructor() {
    super();
    this.state = { time: new Date() };
  }

  currentTime() {
    this.setState({
      time: new Date(),
    });
  }

  componentDidMount() {
    setInterval(() => this.currentTime(), 1000);
  }

  render() {
    return (
      <Typography variant="h3" align="right">
        {this.state.time.toLocaleTimeString()}
        {' - '}
        {currentTime}
      </Typography>
    );
  }
}

export default Clock;
