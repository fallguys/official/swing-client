import { withSnackbar } from 'notistack';

class ActionAlert extends Component {
  registerRetirement = () =>{
    this.key = this.props.enqueueSnackbar('Se registró el cobro exitosamente', { variant: 'success' });
  }
  registerIncidence = () =>{
    this.key = this.props.enqueueSnackbar('Se registró la incidencia exitosamente', { variant: 'success' });
  }

  handleBackOnline = () => {
    this.props.closeSnackbar(this.key);
  };

  render() {
    //...
  };
}

export default withSnackbar(ActionAlert);