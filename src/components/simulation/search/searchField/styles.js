import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  input: {
    width: '80%',
    textAlign: 'center',
    margin: theme.spacing(4),
  },
  icon: {
    color: '#245269',
  },
}));

export default useStyles;
