import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonIcon from '@material-ui/icons/Person';
import useStyles from './styles';
import {useSelector} from "react-redux";
function getValue(type) {
  const bankConsult = useSelector((state) => state.bank);
  if (bankConsult.changed && type !== 'withdrawal') return '';
  return null;
}
export default function SearchField(props) {
  const classes = useStyles();
  const { name, onChange, type } = props;
  let iconComponent;
  if (type === 'withdrawal') {
    iconComponent = <AccountBalanceIcon className={classes.icon} />;
  } else {
    iconComponent = <PersonIcon className={classes.icon} />;
  }

  return (
    <TextField
      className={classes.input}
      onChange={onChange}
      label={name}
      value={getValue(type)}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            {iconComponent}
          </InputAdornment>
        ),
      }}
    />
  );
};
