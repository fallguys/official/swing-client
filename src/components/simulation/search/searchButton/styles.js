import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  buttonIncidence: {
    marginTop: theme.spacing(5),
    backgroundColor: '#CD3436',
  },
  buttonRetirement: {
    marginTop: theme.spacing(5),
    backgroundColor: 'primary',
  },
}));

export default useStyles;
