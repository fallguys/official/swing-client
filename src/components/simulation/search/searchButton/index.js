import Button from '@material-ui/core/Button';
import React from 'react';
import useStyles from './styles';

function getSituation(type) {
  const classes = useStyles();
  if (type === 'Incidencia') {
    return classes.buttonIncidence;
  }
  return classes.buttonRetirement;
}

export default function SearchButton(props) {
  const { type } = props;
  const { onClick } = props;

  return (
    <Button
      variant="contained"
      color="primary"
      size="large"
      onClick={onClick}
      className={getSituation(type)}
    >
      { type === 'Incidencia' && ('Registrar Incidencia') }
      { type === 'Cobro' && ('Registrar Cobro') }
    </Button>
  );
}
