import makeStyles from '@material-ui/core/styles/makeStyles';
import red from '@material-ui/core/colors/red';
import blueGrey from '@material-ui/core/colors/blueGrey';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  card: {
    width: '80%',
    textAlign: 'left',
  },
  stateAccepted: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    fontSize: 32,
    color: '#29985E',
  },
  stateDeclined: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    fontSize: 32,
    color: '#CD3436',
  },
  cardComponent: {
    marginTop: theme.spacing(2),
    width: '80%',
    textAlign: 'left',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  avatar: {
    backgroundColor: red[500],
  },
  avatar2: {
    backgroundColor: blueGrey[500],
  },
}));
export default useStyles;
