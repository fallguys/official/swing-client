import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import React from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import { useSelector } from 'react-redux';
import useStyles from './styles';
import Line from '../../Line';

function stateIcon(state) {
  const classes = useStyles();
  if (state) {
    return <CheckCircleIcon className={classes.stateAccepted} />;
  }
  return <ErrorIcon className={classes.stateDeclined} />;
}

export default function Results(props) {
  const { namePoint = 'Banco de la nación' } = props;
  const { addressPoint = 'Av.Primavera 202' } = props;
  const { type } = props;
  const classes = useStyles();
  const bankConsult = useSelector((state) => state.bank);

  return (
    <div className={classes.root}>
      <Card className={classes.card} elevation={0}>
        <CardHeader
          avatar={(
            <Avatar className={classes.avatar}>
              P
            </Avatar>
          )}
          title="Punto de Retiro"
          subheader={namePoint}
          action={type !== 'Dirección' && (
            stateIcon(bankConsult.beneficiarie.data.isPointAssigned)
          )}
        />
        <Line />
        <CardHeader
          avatar={(
            <Avatar className={classes.avatar2}>
              { type[0] }
            </Avatar>
          )}
          title={type}
          subheader={addressPoint}
          action={type !== 'Dirección' && (
            stateIcon(bankConsult.beneficiarie.data.isValidateDate)
          )}
        />
      </Card>
    </div>
  );
}
