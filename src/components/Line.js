import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  line: {
    height: '1px',
    width: '100%',
    color: 'gray',
    background: '#ECECEC',
  },
}));

function DivisionLine(props = 'ninguna') {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <hr className={classes.line} />
    </div>
  );
}

export default DivisionLine;
