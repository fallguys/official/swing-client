import Button from '@material-ui/core/Button';
import React from 'react';

function PrimaryButton(props) {
  const { text } = props;
  return (
    <div>
      <Button variant="contained" color="primary">
        { text }
      </Button>
    </div>
  );
}

export default PrimaryButton;
