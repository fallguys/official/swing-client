import React from 'react';
import TextField from '@material-ui/core/TextField';
import config from 'config';
import { getDayBefore } from 'utils';
import useStyles from './styles';

function SearchDateRange(props = 'Rango de fechas') {
  const classes = useStyles();
  const { text, onChange } = props;
  const useBefore = config.app.allowScheduleInPast;
  console.log('USEBEFORE', useBefore);
  const thisDay = getDayBefore(useBefore);
  return (
    <form className={classes.container} noValidate>
      <TextField
        id="date"
        label={text}
        type="date"
        inputProps={{
          min: thisDay,
        }}
        color="primary"
        onChange={(e) => {
          onChange(e.target.value);
        }}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </form>
  );
}

export default SearchDateRange;
