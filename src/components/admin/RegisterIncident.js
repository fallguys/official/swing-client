import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  content_dialog: {
    width: '100%',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
}));

export default function RegisterIncident(props) {
  const classes = useStyles();
  const { open, setOpen } = props;
  const putClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <Dialog open={open} onClose={putClose} fullWidth={true} maxWidth="md">
        <Typography variant="h4" gutterBottom align="center">
          Registrar nuevo tipo de incidente
        </Typography>
        <Divider />
        <DialogContent className={classes.content_dialog}>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <Typography variant="h5">
                Nombre
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <TextField id="outlined-basic" variant="outlined" fullWidth={true} />
            </Grid>
            <Grid item xs={4}>
              <Typography variant="h5">
                Descripción
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <TextField id="outlined-basic" variant="outlined" fullWidth={true} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={putClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={putClose} color="primary" variant="contained">
            Registrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
