
export default function (props) {
  const { onClick } = props;
  return (
    <button onClick={onClick} type="primary">
      Exportar
    </button>
  );
}
