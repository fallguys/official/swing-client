import React from 'react';
import Chip from '@material-ui/core/Chip';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
    width: '70%',
    margin: '10px',
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  chipSignal: {
    marginLeft: theme.spacing(10),
  },
  container: {
    margin: '10px',
  },
}));

export default function CustomizedProgressBars(props) {
  const classes = useStyles();
  const { percent, state, initialDate, finalDate } = props;

  return (
    <div className={classes.container}>
      <div className={classes.root}>
        <BorderLinearProgress variant="determinate" value={percent} className={classes.bar} />
        <Chip
          size="medium"
          label={state}
          color="primary"
          className={classes.chipSignal}
        />
      </div>
      <Grid container spacing={1}>
        <Grid item xs={8}>
          <Typography variant="body2" gutterBottom>
            {initialDate}
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="body2" gutterBottom>
            {finalDate}
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}
