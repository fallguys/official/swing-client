//import { makeStyles } from '@material-ui/core/styles';

const useStyles = ((theme) => ({
  root: {
    display: 'flex',
    marginTop: theme.spacing(2),
    justify: 'space-between',
    alignItems: 'center',
  },
  search: {
    flexDirection: 'revert',
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
}));

export default useStyles;
