import React from 'react';
import SearchButton from 'components/admin/search/SearchButton';
import SearchDateRange from 'components/admin/search/SearchDateRange';
//import SynchSearch from 'components/Synchro/SynchSearch';
//import SynchDateSearch from 'components/Synchro/SynchDateSearch';

import withStyles from '@material-ui/core/styles/withStyles';
import useStyles from './styles';

class SearchOptions extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <SearchButton text="Buscar por código" />
        <div className={classes.divider} />
        <SearchDateRange text="Rango de fechas" />
      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(SearchOptions);
