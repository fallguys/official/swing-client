import { formatOnlyDate } from 'utils/format/timer';
import { translateScheduleState } from 'utils/format/translator';

const columns = [
  { id: 'id', label: 'Código', minWidth: 40 },
  { id: 'responsible', label: 'Responsable', minWidth: 100 },
  { id: 'creationDate', label: 'Creado', minWidth: 100, format: formatOnlyDate },
  { id: 'initialDate', label: 'Fecha de Inicio', minWidth: 100, format: formatOnlyDate },
  { id: 'finalDate', label: 'Fecha de Fin', minWidth: 100, format: formatOnlyDate },
  { id: 'state', label: 'Estado', minWidth: 50, format: translateScheduleState },
];

export default columns;
