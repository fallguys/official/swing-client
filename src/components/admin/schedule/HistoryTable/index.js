import React from 'react';
import { getAll } from 'redux/actions/admin/scheduleActions';
import { connect } from 'react-redux';
import Router from 'next/router';
import ActionableDataTable from './ActionableDataTable';
import columns from './columns';

class HistoryTable extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pagesize) {
    const { getOfficialSchedules, getTestSchedules, type } = this.props;
    if (type === 'OFFICIAL') {
      getOfficialSchedules(page, pagesize);
    }
    if (type === 'TEST') {
      getTestSchedules(page, pagesize);
    }
    console.log('HANDLE CHANGE PAGE!', page, pagesize);
  }

  handleSelected(event) {
    console.log('EVENT', event.target.id);
    Router.push('/admin/schedule/history/[id]', `/admin/schedule/history/${event.target.id}`);
  }

  componentDidMount() {
    const { getOfficialSchedules, getTestSchedules, type } = this.props;
    if (type === 'OFFICIAL') {
      getOfficialSchedules();
    }
    if (type === 'TEST') {
      getTestSchedules();
    }
  }

  render() {
    const { type, schedulesOfficial, schedulesTest, totalOfficials, totalTests } = this.props;
    const schedules = type === 'OFFICIAL' ? schedulesOfficial : schedulesTest;
    const totalSchedules = type === 'OFFICIAL' ? totalOfficials : totalTests;
    return (
      <div>
        <ActionableDataTable
          count={totalSchedules}
          columns={columns}
          rows={schedules}
          pageSize={10}
          onChangePage={this.handleChangePage}
          handleSelected={this.handleSelected}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    schedulesOfficial: state.schedule.schedules.officials,
    schedulesTest: state.schedule.schedules.tests,
    totalTests: state.schedule.schedules.totalTests,
    totalOfficials: state.schedule.schedules.totalOfficials,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getOfficialSchedules: (page, pagesize) => dispatch(getAll({ type: 'OFFICIAL', page, pagesize })),
    getTestSchedules: (page, pagesize) => dispatch(getAll({ type: 'TEST', page, pagesize })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryTable);
