import React from 'react';
import Button from '@material-ui/core/Button';

function ScheduleStateButton({ schedule }) {

  if (schedule.type === 'OFFICIAL') {
    if (schedule.state === 'PROGRAMMED') {
      return (
        <Button color="primary">
          CANCELAR
        </Button>
      );
    }
    if (schedule.state === 'CANCELED') {
      return (
        <Button color="primary">
          DISPONIBILIZAR
        </Button>
      );
    }
    if (schedule.state === 'ACTIVE') {
      return (
        <p> </p>
      );
    }
    if (schedule.state === 'FINISHED') {
      return (
        <p> </p>
      );
    }
  }

  if (schedule.type === 'TEST') {
    if (schedule.state === 'AVAILABLE') {
      return (
        <div>
          <Button color="primary">
            DESCARTAR
          </Button>
          <Button color="primary">
            OFICIALIZAR
          </Button>
        </div>
      );
    }
    if (schedule.state === 'DISCARTED') {
      return (
        <div>
          <Button color="primary">
            DISPONIBILIZAR
          </Button>
        </div>
      );
    }
  }

  return (
    <p> </p>
  );
}

export default ScheduleStateButton;
