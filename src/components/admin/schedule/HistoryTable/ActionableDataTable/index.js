import React from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import useStyles from './style';
import ScheduleStateButton from '../ScheduleStateButton';

const ColumnsHeader = ({ columns }) => (
  <TableHead>
    <TableRow>
      {columns.map((column) => (
        <TableCell
          key={column.id}
          align={column.align}
          style={{ minWidth: column.minWidth }}
        >
          {column.label}
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
);

const RowsBody = ({ rows, columns, selected }) => {
  return (
    <TableBody>
      {rows.map((row) => {
        return (
          <TableRow key={row.id} id={row.id} onClick={selected} hover role="checkbox" tabIndex={-1}>
            {columns.map((column) => {
              const value = row[column.id];
              return (
                <TableCell key={column.id} id={row.id} align={column.align}>
                  {column.format ? column.format(value) : value}
                </TableCell>
              );
            })}
          </TableRow>
        );
      })}
    </TableBody>
  );
};

function ActionableDataTable({ count, rows = [], columns, pageSize, onChangePage, handleSelected }) {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    onChangePage(newPage, 10);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <ColumnsHeader columns={columns} />
          <RowsBody rows={rows} columns={columns} selected={handleSelected} />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10]}
        component="div"
        count={count}
        rowsPerPage={pageSize ?? 10}
        page={page}
        onChangePage={handleChangePage}
      />
    </Paper>
  );
}

export default ActionableDataTable;
