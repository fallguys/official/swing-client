import React from 'react';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import PersonIcon from '@material-ui/icons/Person';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import BusinessRoundedIcon from '@material-ui/icons/BusinessRounded';
import { GroupRounded } from '@material-ui/icons';
import { getCheckInfo } from 'redux/actions/admin/scheduleActions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { formatOnlyDate, formatOnlyTime } from 'utils/format/timer';
import Alert from '@material-ui/lab/Alert';
import useStyles from './style';
import { NO_ON_GOING_STUFF, ON_GOING_SCHEDULE, ON_GOING_SYNCHRO } from './states';

class InformationBox extends React.Component {

  componentDidMount() {
    const { getCheckInfoSchedule } = this.props;
    getCheckInfoSchedule();
  }

  textAlert() {
    const { checkInfo } = this.props;

    if (checkInfo.synchroProcessing) {
      return ON_GOING_SYNCHRO;
    }
    if (checkInfo.scheduleProcessing) {
      return ON_GOING_SCHEDULE;
    }
    return NO_ON_GOING_STUFF;
  }

  render() {
    const { classes } = this.props;
    const { checkInfo } = this.props;
    const { date } = this.props;

    return (
      <div>
        <Alert className={classes.alertContainer} severity={checkInfo.scheduleProcessing ? 'warning' : 'success'}>{this.textAlert()}</Alert>
        <Grid container spacing={2} className={classes.root}>
          <Grid item xs={12} className={classes.containerFirst}>
            <PersonIcon />
            <div className={classes.auxText}>
              Nombre Apellido
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerMid}>
            <QueryBuilderIcon />
            <div className={classes.auxText}>
              { date.date }
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerMid}>
            <GroupRounded />
            <div className={classes.auxText}>
              { `${checkInfo.data?.activeBeneficiaries} beneficiarios participantes`}
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerEnd}>
            <BusinessRoundedIcon />
            <div className={classes.auxText}>
              { `${checkInfo.data?.activeWithdrawalPoints} puntos de retiro participantes` }
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    checkInfo: state.schedule.checkInfo,
    date: state.schedule.newSchedule,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCheckInfoSchedule: () => dispatch(getCheckInfo()),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(InformationBox);
