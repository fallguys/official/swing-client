export const ON_GOING_SYNCHRO = 'Una sincronización se encuentra en marcha, no es posible generar un nuevo cronograma hasta que culmine la sincronización';
export const ON_GOING_SCHEDULE = 'Un cronograma se encuentra en proceso, no es posible generar un nuevo cronograma hasta que culmine el procesamiento';
export const NO_ON_GOING_STUFF = 'No existen sincronizaciones en marcha, puede crear un nuevo cronograma';
