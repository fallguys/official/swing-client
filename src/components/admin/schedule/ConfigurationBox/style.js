
const useStyles = ((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
}));

export default useStyles;
