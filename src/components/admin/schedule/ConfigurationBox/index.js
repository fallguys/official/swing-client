import React from 'react';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import { compose } from 'redux';
import useStyles from './style';
import ConfigurationAdvice from './ConfigurationAdvice';
import ConfigurationForm from './ConfigurationForm';

class ConfigurationBox extends React.Component {

  render() {
    const { classes } = this.props;
    return (

      <div className={classes.root}>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <ConfigurationForm />
          </Grid>
          <Grid item xs={6}>
            <ConfigurationAdvice />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default compose(
  withStyles(useStyles, { withTheme: true }),
)(ConfigurationBox);
