import { withRouter } from 'next/router';
import React from "react";

function Page(props) {
  const { router } = props;
  return <p>{router.pathname}</p>;
}

export default withRouter(Page)