import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
  oneRow: {
    flexGrow: 1,
    display: 'flex',
    justify: 'space-between',
    alignItems: 'center',
  },
  button: {
    marginTop: theme.spacing(6),
  },
}));

export default useStyles;
