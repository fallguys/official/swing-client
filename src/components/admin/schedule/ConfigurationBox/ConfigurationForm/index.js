import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';
import SearchDateRange from 'components/admin/search/SearchDateRange';
//import DatePicker from 'components/admin/search/DatePicker';
import { connect } from 'react-redux';
import { postNewSchedule, setDate, setType } from 'redux/actions/admin/scheduleActions';
import Button from '@material-ui/core/Button';
import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import useStyles from './style';
import Modal from "../../../modal";
import {formatOnlyDate} from "utils/format/timer";
import { withRouter } from 'next/router';
import moment from "moment";
//

class ConfigurationForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      initialDate: null,
      type: 'TEST',
      open: false,
      text: '',
      content: '',
      registered: false,
    };
  }

  onChangeInitialDate = (newInitialDate) => {
    this.setState({
      initialDate: newInitialDate,
    });
  };

  onChangeInitialType = (event) =>{
    this.setState({
      type: event.target.checked ? 'OFFICIAL' : 'TEST',
    });
  };

  setModal = async () => {
    const { newSchedule, checkInfo } = this.props;
    const { router } = this.props;
    this.setState({
      title: newSchedule.error === null ? 'Generación exitosa': 'Generación inválida',
      content: newSchedule.error === null ? 'Se ha generado el cronograma con fecha de inicio ' + formatOnlyDate(this.state.initialDate)
        : checkInfo.scheduleProcessing ? 'No se puede generar el cronograma, ya que hay un cronograma oficial en curso'
          : 'No se puede generar el cronograma, ya que hay una sincronización en curso',
      open: true,
    });
    this.state = {
      initialDate: newSchedule.error === null ? null : this.state.initialDate,
      type: newSchedule.error === null ? 'TEST': this.state.type,
      registered: newSchedule.error === null,
    };
    if (this.state.registered) router.push('/admin/schedule/history');
  };

  registerSchedule = async () => {
    const { registerNewSchedule, setDateSchedule } = this.props;
    const schedule = {
      adminId: 1,
      initialDate: this.state.initialDate,
      scheduleType: this.state.type,
    };
    setDateSchedule(moment().format('DD/MM/YY - hh:mm'));
    await registerNewSchedule(schedule);
    await this.setModal();
    setDateSchedule('--- - ---');
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Modal title={this.state.title} content={this.state.content} open={this.state.open} handleClose={this.handleClose}/>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Grid container spacing={1} className={classes.oneRow}>
              <Grid item xs={4}>
                <Typography variant="body2" gutterBottom>
                  Fecha de Inicio:
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <SearchDateRange text="Fecha de Inicio" onChange={this.onChangeInitialDate} />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} className={classes.oneRow}>
            <Grid container spacing={1} className={classes.oneRow}>
              <Grid item xs={4}>
                <Typography variant="body2" gutterBottom>
                  Oficializar:
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Switch
                  name="checkedA"
                  color="primary"
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                  onChange={this.onChangeInitialType}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <br />
        <Button
          className={classes.button}
          variant="contained"
          color="primary"
          onClick={this.registerSchedule}
        >
          Generar nuevo cronograma
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    newSchedule: state.schedule.newSchedule,
    checkInfo: state.schedule.checkInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setDateSchedule: (date) => dispatch(setDate(date)),
    setTypeSchedule: (type) => dispatch(setType(type)),
    registerNewSchedule: (schedule) => dispatch(postNewSchedule(schedule)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(ConfigurationForm);
