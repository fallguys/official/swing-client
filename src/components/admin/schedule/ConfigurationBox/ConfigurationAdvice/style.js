import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: theme.spacing(0),
    borderStyle: 'groove',
    height: '100px',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default useStyles;