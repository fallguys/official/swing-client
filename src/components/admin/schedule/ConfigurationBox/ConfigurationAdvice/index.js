import React from 'react';
import Container from '@material-ui/core/Container';
import { useDispatch, useSelector } from 'react-redux';
import { formatOnlyDate } from 'utils/format/timer';
import moment from 'moment';
import useStyles from './style';

function getText() {
  const infoConsult = useSelector((state) => state.schedule);
  let dateEnd = '',
    dateStart;
  if (infoConsult.checkInfo.data?.officialScheduleFinalDate !== null) {
    dateEnd = formatOnlyDate(infoConsult.checkInfo.data?.officialScheduleFinalDate);
    dateStart = moment(infoConsult.checkInfo.data?.officialScheduleFinalDate).add(1, 'day');
    dateStart = formatOnlyDate(dateStart);
    return `El último cronograma oficial termina el ${dateEnd}. Para oficializar este calendario, la fecha de inicio debe de ser desde el ${dateStart} en adelante. Puedes crear este calendario como prueba y oficializarlo después`;
  }
  if (infoConsult.checkInfo.data?.scheduleProcessing || infoConsult.checkInfo.data?.synchroProcessing) {
    return '';
  }
  return 'No hay un cronograma oficial vigente o programado. Puede seleccionar cualquier fecha a partir de la actual.';

}

function ConfigurationColumnTwo(props) {
  const classes = useStyles();

  return (
    <div>
      <Container className={classes.root}>
        {getText()}
      </Container>
    </div>
  );
}

export default ConfigurationColumnTwo;
