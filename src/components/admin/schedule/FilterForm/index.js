import Grid from '@material-ui/core/Grid';
import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import { useDispatch, useSelector } from 'react-redux';
import { fetchDepartment, fetchDistrict, fetchProvince, fetchWithdrawalPoints } from 'redux/actions/bonus/chooseWithdrawalPointActions';
import { getAllDetailBeneficiary } from 'redux/actions/admin/scheduleDetailBeneficiaryActions';
import useStyles from './style';
import UbicationSelect from './UbicationSelect';
import SimpleMenu from '../../../Synchro/DropdownList';
import { getAllDetailWithdrawal } from '../../../../redux/actions/admin/scheduleDetailWithdrawalActions';
import scheduleService from '../../../../services/admin/schedule-service';
import { getBeneficiaryReport, getWithdrawalReport } from '../../../../redux/actions/admin/scheduleReportsActions';

function FilteringForm(props) {
  const { type, codSchedule } = props;
  const classes = useStyles();
  const [selectedInitialDate, handleInitialDateChange] = useState(null);
  const [selectedFinalDate, handleFinalDateChange] = useState(null);
  const [beneficiaryCode, setBeneficiaryCode] = useState(null);
  const [open, setOpen] = useState(false);
  const [point, setPoint] = React.useState('');
  const [state, setState] = React.useState('');
  const dispatch = useDispatch();
  const chooseWithdrawalPoint = useSelector((state) => state.chooseWithdrawalPoint);

  const [zone, setZone] = useState({
    department: null,
    province: null,
    district: null,
  });

  const { department, province, district } = zone;

  useEffect(() => {
    dispatch(fetchDepartment());
  }, []);

  useEffect(() => {
    if (department !== null) {
      dispatch(fetchProvince(department));
    }
  }, [department]);

  useEffect(() => {
    if (province !== null) {
      dispatch(fetchDistrict(province));
    }
  }, [province]);

  const handleInitialDate = (e) => {
    handleInitialDateChange(e.target.value);
  };

  const handleFinalDate = (e) => {
    handleFinalDateChange(e.target.value);
  };

  const setBeneficiaryCodes = (e) => {
    setBeneficiaryCode(e.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function handleSubmit(e) {
    e.preventDefault();
    dispatch(fetchWithdrawalPoints(district));
    handleClose();
  }

  const handleChangePoint = (event) => {
    setPoint(event.target.value);
  };

  const handleChangeState = (event) => {
    setState(event.target.value);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  function handleSubmitResults(e) {
    e.preventDefault();
    if (type === 'Beneficiary') {
      const ubigee = district === undefined ? null : district;
      const withdrawalPointCode = point === undefined ? null : point === '' ? null : point.toString();
      const page = 0;
      const pagesize = 10;
      const benefCode = beneficiaryCode === '' ? null : beneficiaryCode;
      dispatch(getAllDetailBeneficiary({ codSchedule, page, pagesize, initialDate: selectedInitialDate, finalDate: selectedFinalDate, beneficiaryCode: benefCode, state, ubigee, withdrawalPointCode }));
    } else {
      const ubigee = district === undefined ? null : district;
      const withdrawalPoint = point === undefined ? null : point === '' ? null : point.toString();
      const withdrawalPointCode = withdrawalPoint !== null ? withdrawalPoint : beneficiaryCode !== null ? beneficiaryCode : null;
      const page = 0;
      const pagesize = 10;
      dispatch(getAllDetailWithdrawal({ codSchedule, page, pagesize, ubigee, withdrawalPointCode }));
    }
  }

  function ReportBeneficiary(type) {
    const ubigee = district === undefined ? null : district;
    const withdrawalPointCode = point === undefined ? null : point === '' ? null : point.toString();
    const benefCode = beneficiaryCode === '' ? null : beneficiaryCode;
    dispatch(getBeneficiaryReport(codSchedule, type, selectedInitialDate, selectedFinalDate, benefCode, state, ubigee, withdrawalPointCode));
  }

  function ReportWithdrawal(type) {
    const ubigee = district === undefined ? null : district;
    const withdrawalPointCode = point === undefined ? null : point === '' ? null : point.toString();
    dispatch(getWithdrawalReport(codSchedule, type, ubigee, withdrawalPointCode));
  }

  return (
    <div>
      <Grid
        container
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={5}>
          <Typography variant="h6">
            Filtrar por:
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <SimpleMenu
            onClickPDF={type === 'Beneficiary' ? ReportBeneficiary : ReportWithdrawal}
            onClickExcel={type === 'Beneficiary' ? ReportBeneficiary : ReportWithdrawal}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={4}
        className={classes.root}
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={6} className={classes.containerFirst}>
          <TextField
            id="standard-helperText"
            label={type === 'Withdrawal' ? 'Código de punto de retiro' : 'Código de beneficiario'}
            placeholder="Ingrese un Código"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={setBeneficiaryCodes}
            className={classes.textFieldInput}
          />
        </Grid>
        {
          type === 'Beneficiary' && (
            <Grid item xs={2} className={classes.containerMid}>
              <TextField
                id="date"
                label="Fecha Inicial"
                type="date"
                defaultValue={selectedInitialDate}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={handleInitialDate}
              />
            </Grid>
          )
        }
        {
          type === 'Beneficiary' && (
            <Grid item xs={2} className={classes.containerEnd}>
              <TextField
                id="date"
                label="Fecha Final"
                type="date"
                defaultValue={selectedFinalDate}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={handleFinalDate}
              />
            </Grid>
          )
        }
        <Grid item xs={6}>
          <Button onClick={handleClickOpen} className={classes.buttonUbigee}>Elegir Zona</Button>
          <Dialog disableBackdropClick disableEscapeKeyDown open={open} onClose={handleClose}>
            <DialogTitle>Elegir Zona</DialogTitle>
            <DialogContent>
              <form className={classes.container}>
                <UbicationSelect
                  id="select-department"
                  text="Departamento"
                  items={chooseWithdrawalPoint.department.data}
                  onChange={(e) => setZone({ ...zone, department: e.target.value })}
                />
                <UbicationSelect
                  id="select-province"
                  text="Provincia"
                  items={chooseWithdrawalPoint.province.data}
                  onChange={(e) => setZone({ ...zone, province: e.target.value })}
                />
                <UbicationSelect
                  id="select-district"
                  text="Distrito"
                  items={chooseWithdrawalPoint.district.data}
                  onChange={(e) => {
                    setZone({ ...zone, district: e.target.value });
                  }}
                />
              </form>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={(e) => handleSubmit(e)} color="primary">
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </Grid>
        <Grid item xs={5}>
          {
            type === 'Beneficiary' && (
              <FormControl className={classes.formState}>
                <InputLabel shrink id="demo-simple-select-placeholder-label-label" placeholder="Ingrese un Punto de Retiro">
                  Estado de Retiro
                </InputLabel>
                <Select
                  labelId="demo-simple-select-placeholder-label-label"
                  id="demo-simple-select-placeholder-label"
                  value={state}
                  onChange={handleChangeState}
                  displayEmpty
                  className={classes.selectEmpty}
                >
                  <option aria-label="None" value={null} />
                  <option value={true}>Recogió</option>
                  <option value={false}>No Recogió</option>
                </Select>
              </FormControl>
            )
          }
        </Grid>
        <Grid item xs={6}>
          <FormControl className={classes.formPoint}>
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
              Punto de Retiro
            </InputLabel>
            <Select
              labelId="demo-simple-select-placeholder-label-label"
              id="demo-simple-select-placeholder-label"
              value={point}
              onChange={handleChangePoint}
              displayEmpty
              className={classes.selectEmpty}
            >
              <option aria-label="None" value="" />
              {
                chooseWithdrawalPoint.withdrawalPoints?.data.map((item) => (
                  <option key={item.id} value={item.id} name={item.code}>
                    {item.name}
                  </option>
                ))
              }
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={4}>
          <Button variant="contained" onClick={(e) => handleSubmitResults(e)} color="primary" disableElevation className={classes.buttonShow}>
            Mostrar Resultados
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default FilteringForm;
