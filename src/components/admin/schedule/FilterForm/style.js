import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#ECECF6',
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '20px 0px 20px',
    width: '100%',
  },
  containerFirst: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(5),
  },
  textFieldInput: {
    width: '65%',
  },
  containerMid: {
    display: 'flex',
    marginLeft: theme.spacing(8),
  },
  listItem: {
    marginLeft: theme.spacing(5),
  },
  containerEnd: {
    display: 'flex',
    marginRight: theme.spacing(6),
  },
  auxText: {
    color: '#6E777E',
    marginLeft: theme.spacing(2),
  },
  alertContainer: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: 130,
  },
  selectEmpty: {
    width: '100%',
  },
  formState: {
    marginLeft: theme.spacing(3.5),
    width: '35%',
  },
  buttonUbigee: {
    marginLeft: theme.spacing(4),
  },
  formPoint: {
    marginLeft: theme.spacing(5),
    marginBottom: theme.spacing(3),
    width: '65%',
  },
  buttonShow: {
    marginLeft: theme.spacing(12),
  },
}));

export default useStyles;
