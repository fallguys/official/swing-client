import React from 'react';
import { connect } from 'react-redux';
import DataTable from 'components/admin/data/DataTable';
import { getAllDetailBeneficiary } from 'redux/actions/admin/scheduleDetailBeneficiaryActions';
import * as R from 'ramda';
import columnsBeneficiary from './columnsBeneficiary';
import FilteringForm from '../../FilterForm';

const addDatesColumn = (beneficiary) => R.assoc('dates', R.pick(['initialDate', 'finalDate'], beneficiary), beneficiary);
const addHourToCollect = (beneficiaries) => R.map(addDatesColumn, beneficiaries);

class DataTableBeneficiary extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pagesize) {
    const { getBeneficiaries, codSchedule } = this.props;
    getBeneficiaries(codSchedule, page, pagesize);
  }

  componentDidMount() {
    const { getBeneficiaries, codSchedule } = this.props;
    getBeneficiaries(codSchedule);
  }

  render() {
    const { beneficiaries, totalBeneficiaries, codSchedule } = this.props;
    const rows = addHourToCollect(beneficiaries);
    console.log('DATA BENEFICIARIES IN INDEX', beneficiaries);
    console.log('TOTAL DE BENEFICIARIOS', totalBeneficiaries);
    return (
      <div>
        <FilteringForm type="Beneficiary" codSchedule={codSchedule} />
        <DataTable
          count={totalBeneficiaries}
          rows={rows}
          columns={columnsBeneficiary}
          pageSize={10}
          onChangePage={this.handleChangePage}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    beneficiaries: state.scheduleDetailBeneficiary.beneficiaries,
    totalBeneficiaries: state.scheduleDetailBeneficiary.totalBeneficiaries,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBeneficiaries: (codSchedule, page, pagesize) => dispatch(getAllDetailBeneficiary({ codSchedule, page, pagesize })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DataTableBeneficiary);
