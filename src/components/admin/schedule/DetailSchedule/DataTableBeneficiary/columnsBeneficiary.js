import { formatOnlyDate, formatHoursInterval } from 'utils/format/timer';
import { validationBoolean } from 'utils/format/boolean';
import { formatOnlyTime } from '../../../../../utils/format/timer';

const columnsBeneficiary = [
  { id: 'id', label: 'Código', width: 30 },
  { id: 'withdrawalPoint', label: 'Ubicación', width: 30 },
  { id: 'initialDate', label: 'Día asignado', width: 50, format: formatOnlyDate },
  { id: 'dates', label: 'Hora asginada', width: 30, format: formatHoursInterval },
  { id: 'bonusRetiredDate', label: 'Fecha de recojo', width: 30, format: (value) => {return `${formatOnlyDate(value)}  -  ${formatOnlyTime(value)}`; } },
  { id: 'bonusRetired', label: 'Recogió bono', width: 30, format: validationBoolean },
  { id: 'mistakes', label: 'Desatinos', width: 20, format: (value) => (value === null ? '' : value) },
];

export default columnsBeneficiary;
