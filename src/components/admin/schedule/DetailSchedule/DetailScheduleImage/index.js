import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(20),
    alignItems: 'center',
    width: '100%',
  },
  image: {
    display: 'block',
    margin: 'auto',
    width: '50%',
  },
  text: {
    display: 'flex',
    alignItems: 'center',
  },
  container: {
    maxHeight: 262,
  },
}));

function DetailScheduleImage() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img src="/imgs/notInformation.png" alt="" className={classes.image} />
      <Typography variant="h6" gutterBottom align="center">
        El cronograma se está generando actualmente, vuelva en unos minutos.
      </Typography>
    </div>
  );
}

export default DetailScheduleImage;
