
const columnsWithdrawalPoint = [
  { id: 'id', label: 'Código', minWidth: 40 },
  { id: 'address', label: 'Ubicación', minWidth: 150 },
  { id: 'ubigee', label: 'Ubigeo', minWidth: 50 },
  { id: 'cantAssigned', label: 'Cant. Asignados', minWidth: 50 },
  { id: 'cantAttended', label: 'Cant. Atendidos', minWidth: 50 },
];

export default columnsWithdrawalPoint;
