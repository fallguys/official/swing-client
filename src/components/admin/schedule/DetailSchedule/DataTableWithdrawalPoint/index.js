import React from 'react';
import { connect } from 'react-redux';
import DataTable from 'components/admin/data/DataTable';
import { getAllDetailWithdrawal } from 'redux/actions/admin/scheduleDetailWithdrawalActions';
import columnsWithdrawalPoint from './columnsWithdrawalPoint';
import FilteringForm from '../../FilterForm';

class DataTableWithdrawalPoint extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pagesize) {
    const { getWithdrawalPoints, codSchedule } = this.props;
    getWithdrawalPoints(codSchedule, page, pagesize);
  }

  componentDidMount() {
    const { getWithdrawalPoints, codSchedule } = this.props;
    getWithdrawalPoints(codSchedule);
  }

  render() {
    const { withdrawals, totalWithdrawals, codSchedule } = this.props;
    console.log('total de puntos de retiro', totalWithdrawals);
    return (
      <div>
        <FilteringForm type="Withdrawal" codSchedule={codSchedule}/>
        <DataTable
          count={totalWithdrawals}
          rows={withdrawals}
          columns={columnsWithdrawalPoint}
          pageSize={10}
          onChangePage={this.handleChangePage}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    withdrawals: state.scheduleDetailWithdrawal.withdrawals,
    totalWithdrawals: state.scheduleDetailWithdrawal.totalWithdrawals,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWithdrawalPoints: (codSchedule, page, pagesize) => dispatch(getAllDetailWithdrawal({ codSchedule, page, pagesize })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DataTableWithdrawalPoint);
