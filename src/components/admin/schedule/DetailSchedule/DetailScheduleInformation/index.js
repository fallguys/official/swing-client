import React from 'react';
import InformationBox from 'components/admin/schedule/DetailSchedule/InformationBox';
import DetailScheduleTabs from 'components/admin/tabs/DetailScheduleTabs';

function DetailScheduleInformation(props) {
  const { codSchedule, informationBox } = props;
  return (
    <div>
      <InformationBox informationBox={informationBox} />
      <DetailScheduleTabs codSchedule={codSchedule} />
    </div>
  );
}

export default DetailScheduleInformation;
