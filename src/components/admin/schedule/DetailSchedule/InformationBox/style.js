//import { makeStyles } from '@material-ui/core/styles';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#ECECEC',
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '20px 0px 20px',
  },
  containerFirst: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  containerMid: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
  },
  containerEnd: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },

  auxText: {
    color: '#6E777E',
    marginLeft: theme.spacing(2),
  },
}));

export default useStyles;
