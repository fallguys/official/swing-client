import React from 'react';
import Grid from '@material-ui/core/Grid';
import PersonIcon from '@material-ui/icons/Person';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import BusinessRoundedIcon from '@material-ui/icons/BusinessRounded';
import { GroupRounded } from '@material-ui/icons';
import { formatOnlyDate } from 'utils/format/timer';
import ProcessBar from 'components/admin/schedule/ProcessBar';
import moment from 'moment';
import { translateScheduleState } from 'utils/format/translator';
import progressPercentage from 'utils';
import useStyles from './style';

function InformationBox(props) {
  const classes = useStyles();
  const { informationBox } = props;
  const initialDate = moment(informationBox?.initialDate);
  const finalDate = moment(informationBox?.finalDate);
  const percent = progressPercentage(initialDate, finalDate);
  return (
    <div>
      <ProcessBar
        percent={percent}
        initialDate={informationBox?.initialDate}
        finalDate={informationBox?.finalDate}
        state={translateScheduleState(informationBox?.state)}
      />
      <div className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.containerFirst}>
            <PersonIcon />
            <div className={classes.auxText}>
              {informationBox?.adminName}
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerMid}>
            <QueryBuilderIcon />
            <div className={classes.auxText}>
              {formatOnlyDate(informationBox?.creationDate)}
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerMid}>
            <GroupRounded />
            <div className={classes.auxText}>
              {`${informationBox?.beneficiaries} beneficiarios participantes`}
            </div>
          </Grid>
          <Grid item xs={12} className={classes.containerEnd}>
            <BusinessRoundedIcon />
            <div className={classes.auxText}>
              {`${informationBox?.withdrawalPoints} puntos de retiro participantes`}
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

export default InformationBox;
