import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import Router from 'next/router';
import useStyles from './style';

class formLogin extends React.Component {
  handleChange() {
    Router.push('/admin/home', '/admin/home');
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Grid container justify="center">
          <Grid item xs={12} className={classes.containerFirst}>
            <Typography variant="h4">
              Bienvenido
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h5">
              Inicia sesión para empezar
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.containerFirst}>
            <TextField
              label="Correo"
              margin="normal"
              fullWidth={true}
              placeholder="Ingrese su correo"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} className={classes.containerMid}>
            <TextField
              label="Contraseña"
              type="password"
              margin="normal"
              fullWidth={true}
              placeholder="Ingrese su Contraseña"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
          </Grid>
          <Grid container className={classes.containerCheckLink}>
            <Grid item xs={12} sm={6}>
              <FormControlLabel
                control={(
                  <Checkbox
                    name="checkedB"
                    color="primary"
                  />
                )}
                label="Recuerdame"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Link
                component="button"
                variant="body2"
                color="primary"
              >
                ¿Olvidaste tu contraseña?
              </Link>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleChange}
          >
            Iniciar Sesión
          </Button>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(formLogin);
