const useStyles = ((theme) => ({
  root: {
    backgroundColor: '#ECECEC',
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '20px 0px 20px',
  },
  containerFirst: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(5),
    width: '200px',
  },
  containerMid: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(2),
    width: '200px',
  },
  containerCheckLink: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(5),
  },
  button: {
    marginTop: theme.spacing(5),
    width: '350px',
  },
  alertContainer: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
}));

export default useStyles;