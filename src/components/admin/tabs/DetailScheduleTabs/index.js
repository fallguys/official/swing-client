import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import DataTableBeneficiary from 'components/admin/schedule/DetailSchedule/DataTableBeneficiary';
import DataTableWithdrawalPoint from 'components/admin/schedule/DetailSchedule/DataTableWithdrawalPoint';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary,
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  tabs: {
    MuiTabsIndicator: theme.palette.tertiary,
  },
}));

export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { codSchedule } = props;
  const handleChange = (event, newValue) => {
    console.log('Cambio de estado');
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          className={classes.tabs}
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
          indicatorColor="secondary"
          variant="fullWidth"
        >
          <Tab label="Detalle de beneficiarios" {...a11yProps(0)} />
          <Tab label="Detalle de puntos de retiro" {...a11yProps(1)} />
          <Tab label="Detalles" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <DataTableBeneficiary codSchedule={codSchedule} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <DataTableWithdrawalPoint codSchedule={codSchedule} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <p>Detalles</p>
      </TabPanel>
    </div>
  );
}
