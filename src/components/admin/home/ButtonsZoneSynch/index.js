import React from 'react';
import ScheduleImage from 'components/admin/home/ScheduleImage';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Router from 'next/router';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  alertContainer: {
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
    width: '70vw',
    height: '50px',
  },
  button: {
    marginTop: theme.spacing(2),
    width: '350px',
    display: 'block',
    margin: 'auto',
  },
  buttonAlert: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '65vw',
  },
  textAlert: {

  },
}));

function ButtonsZoneSynch(props) {
  const { valueBenef, valueWithdrawal, valueContagion } = props;
  const classes = useStyles();
  const handleChange = () => {
    Router.push('/admin/schedule/new', '/admin/schedule/new');
  };

  const handleChangeBenef = () => {
    Router.push('/admin/synch/benef', '/admin/synch/benef');
  };

  const handleChangeWith = () => {
    Router.push('/admin/synch/points', '/admin/synch/points');
  };

  const handleChangeContagion = () => {
    Router.push('/admin/synch/areas', '/admin/synch/areas');
  };

  return (
    <div>
      <ScheduleImage existSchedule={false} />
      {!valueBenef && (
        <Alert className={classes.alertContainer} severity="info">
          <div className={classes.buttonAlert}>
            <div className={classes.textAlert}>
              Necesitas sincronizar los datos de los
              {' '}
              <strong> Beneficarios</strong>
              {' '}
            </div>
            <Button
              size="small"
              color="primary"
              onClick={handleChangeBenef}
            >
              Sincronizar
            </Button>
          </div>
        </Alert>
      )}
      {!valueWithdrawal && (
        <Alert className={classes.alertContainer} severity="info">
          <div className={classes.buttonAlert}>
            <div className={classes.textAlert}>
              Necesitas sincronizar los datos de los
              {' '}
              <strong>Puntos de retiro</strong>
              {' '}
            </div>
            <Button
              size="small"
              color="primary"
              onClick={handleChangeWith}
            >
              Sincronizar
            </Button>
          </div>
        </Alert>
      )}
      {!valueContagion && (
        <Alert className={classes.alertContainer} severity="info">
          <div className={classes.buttonAlert}>
            <div className={classes.textAlert}>
              Necesitas sincronizar los datos de los
              {' '}
              <strong> Zonas de Contagio</strong>
              {' '}
            </div>
            <Button
              size="small"
              color="primary"
              onClick={handleChangeContagion}
            >
              Sincronizar
            </Button>
          </div>
        </Alert>
      )}
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        onClick={handleChange}
      >
        Generar Nuevo Cronograma
      </Button>
    </div>
  );
}

export default ButtonsZoneSynch;
