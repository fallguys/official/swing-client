import { Typography } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(20),
    alignItems: 'center',
    width: '100%',
  },
  imageTrue: {
    display: 'block',
    margin: 'auto',
    width: '50%',
  },
  imageFalse: {
    display: 'block',
    margin: 'auto',
    width: '30%',
    height: '90%',
  },
}));

function ScheduleImage(props) {
  const { existSchedule } = props;
  const classes = useStyles();
  let text;
  if (existSchedule) {
    text = 'Existe un cronograma oficial vigente';
  } else {
    text = 'Aún no existe un cronograma oficial vigente';
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        { existSchedule ? <img src="/imgs/notInformation.png" alt="" className={classes.imageTrue} /> : <img src="/imgs/notInformation.png" alt="" className={classes.imageFalse} /> }
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h6" gutterBottom align="center">
          {text}
        </Typography>
      </Grid>
    </Grid>
  );
}

export default ScheduleImage;
