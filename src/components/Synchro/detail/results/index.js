import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import Card from '@material-ui/core/Card';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import GetAppIcon from '@material-ui/icons/GetApp';
import TimelapseOutlinedIcon from '@material-ui/icons/TimelapseOutlined';
import ErrorOutlineOutlinedIcon from '@material-ui/icons/ErrorOutlineOutlined';
import { getSynchroDetails } from 'redux/actions/admin/synchroObtainDetailsActions';
import useStyles from './styles';
import Line from '../../../Line';
import { translateSynchType } from "utils/format/translator";
import { validationNumber } from 'utils/format/boolean';

class Results extends React.Component {
  componentDidMount() {
    const { getDetail, idSynchro } = this.props;
    getDetail(idSynchro);
  }

  getText(state, type, amount, typeCharge) {
    if (state === 'IN_PROCESS') {
      return '---';
    }
    if (state === 'INCOMPLETE') {
      return '---';
    }
    if (typeCharge === "charge") {
      return `${validationNumber(amount)} ${translateSynchType(type)} a procesar`;
    }
    if (typeCharge === "error" && amount > 0) {
      return `${validationNumber(amount)} ${translateSynchType(type)} con errores`;
    }
    if (typeCharge === "error" && amount === 0) {
      return `sin errores`;
    }
  }

  stateIcon(amount, typeCharge, state) {
    const { classes } = this.props;
    const value = validationNumber(amount);
    if (state === 'IN_PROCESS') {
      return <TimelapseOutlinedIcon className={classes.stateProcessing} />;
    }
    if (state === 'INCOMPLETE') {
      return <TimelapseOutlinedIcon className={classes.stateProcessing} />;
    }
    if (value > 0 && typeCharge === "charge") {
      return <CheckCircleOutlineIcon className={classes.stateAccepted} />;
    }
    if (state === 'FAILED' && value === 0) {
      return <ErrorOutlineOutlinedIcon className={classes.stateDeclined} />;
    }
    if (value > 0 && typeCharge === "error") {
      return <ErrorOutlineOutlinedIcon className={classes.stateDeclined} />;
    }
    if (value === 0 && typeCharge === "error" && state === 'COMPLETED') {
      return <CheckCircleOutlineIcon className={classes.stateAccepted} />;
    }
    if (value === 0 && typeCharge === "error" && state === 'FAILED') {
      return <ErrorOutlineOutlinedIcon className={classes.stateDeclined} />;
    }
  }

  downloadFile = () => {
    const { details } = this.props;
    if (details.data?.status === 'COMPLETED') window.location.assign(details.data?.sourceDataFile);
  }

  render() {
    const { classes } = this.props;
    const { details } = this.props;

    return (
      <div className={classes.root}>
        Resultados
        <Grid container className={classes.rootContainer} direction="column">
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            className={classes.containerChild}
          >
            <Grid item xs={6}>
              <Card className={classes.card} elevation={0}>
                <CardHeader
                  avatar={this.stateIcon(details.data?.cantTotal, "charge", details.data?.status)}
                  title="Archivo de datos a cargar"
                />
              </Card>
            </Grid>
            <Grid item xs={3}>
              {this.getText(details.data?.status, details.data?.type, details.data?.cantTotal, "charge")}
            </Grid>
            <Grid item xs={1}>
              <IconButton onClick={this.downloadFile}>
                <GetAppIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Line />
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item xs={6}>
              <Card className={classes.card} elevation={0}>
                <CardHeader
                  avatar={this.stateIcon(details.data?.cantFailed, "error", details.data?.status)}
                  title="Datos no cargados por errores"
                />
              </Card>
            </Grid>
            <Grid item xs={3}>
              {this.getText(details.data?.status, details.data?.type, details.data?.cantFailed, "error")}
            </Grid>
            <Grid item xs={1}>
              <IconButton>
                <GetAppIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    details: state.synchroDetail.synchro,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (idSynchro) => dispatch(getSynchroDetails(idSynchro)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(Results);
