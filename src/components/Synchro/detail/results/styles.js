import red from '@material-ui/core/colors/red';
import blueGrey from '@material-ui/core/colors/blueGrey';

const useStyles = ((theme) => ({
  root: {
    marginTop: theme.spacing(6),
  },
  rootContainer: {
    marginTop: theme.spacing(1),
  },
  containerChild: {
    marginTop: theme.spacing(0),
    marginBottom: theme.spacing(0),
  },
  card: {
    width: '100%',
    textAlign: 'left',
  },
  stateAccepted: {
    marginTop: theme.spacing(0.7),
    marginRight: theme.spacing(2),
    fontSize: 32,
    color: '#29985E',
  },
  stateDeclined: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    fontSize: 32,
    color: '#CD3436',
  },
  stateProcessing: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    fontSize: 32,
    color: '#F0AD4E',
  },
  cardComponent: {
    marginTop: theme.spacing(0),
    width: '80%',
    textAlign: 'left',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  avatar: {
    backgroundColor: red[500],
  },
  avatar2: {
    backgroundColor: blueGrey[500],
  },
}));

export default useStyles;
