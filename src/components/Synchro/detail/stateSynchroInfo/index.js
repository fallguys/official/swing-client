import Alert from '@material-ui/lab/Alert';
import React from 'react';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Chip from '@material-ui/core/Chip';
import { getSynchroDetails } from 'redux/actions/admin/synchroObtainDetailsActions';
import { translateSynchType } from 'utils/format/translator';
import { validationNumber } from 'utils/format/boolean';
import useStyles from './styles';

class StateSynchroInfo extends React.Component {
  componentDidMount() {
    const { getSynchroDetails, idSynchro } = this.props;
    getSynchroDetails(idSynchro);
  }

  getState(state = 'IN_PROCESS') {
    if (state === 'IN_PROCESS') {
      const StateSynch = withStyles({
        root: {
          backgroundColor: '#F0AD4E',
          color: '#FFFFFF',
        },
      })(Chip);
      return <StateSynch label="En Proceso" />;
    }
    if (state === 'FAILED') {
      const StateSynch = withStyles({
        root: {
          backgroundColor: '#F04E4E',
          color: '#FFFFFF',
        },
      })(Chip);
      return <StateSynch label="Fallido" />;
    }
    if (state === 'COMPLETED') {
      const StateSynch = withStyles({
        root: {
          backgroundColor: '#29985E',
          color: '#FFFFFF',
        },
      })(Chip);
      return <StateSynch label="Completado" />;
    }
  }

  getText(type) {
    return (
      <div>
        <div>
          La carga de
          {' '}
          <strong>{`${translateSynchType(type)}`}</strong>
          {' '}
          se está realizando actualmente.
        </div>
        <div>
          Por favor,
          {' '}
          <strong>no generar ningún cronograma</strong>
          {' '}
          hasta que terminen todas las sincronizaciones pendientes.
        </div>
      </div>
    );

  }

  render() {
    const { classes } = this.props;
    const { details } = this.props;

    return (
      <div>
        <Grid
          container
          direction="row"
          justify="space-between"
          className={classes.root}
        >
          <Grid item xs={8}>
            Sincronización de
            {' '}
            {`${validationNumber(details.data?.cantTotal)}`}
            {' '}
            { `${translateSynchType(details.data?.type)}` }
          </Grid>
          <Grid item xs={1}>
            {this.getState(details.data?.status)}
          </Grid>
        </Grid>
        {details.data?.status === 'IN_PROCESS' && (
          <Alert className={classes.alertContainer} severity="info">
            {this.getText(details.data?.type)}
          </Alert>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    details: state.synchroDetail.synchro,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSynchroDetails: (idSynchro) => dispatch(getSynchroDetails(idSynchro)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(StateSynchroInfo);
