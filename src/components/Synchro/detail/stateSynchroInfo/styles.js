const useStyles = ((theme) => ({
  root: {
    marginTop: theme.spacing(2),
  },
  alertContainer: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
}));

export default useStyles;

