const useStyles = ((theme) => ({
  root: {
    backgroundColor: '#ECECEC',
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '20px 0px 20px',
    width: '100%',
  },
  containerFirst: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    marginTop: theme.spacing(1),
  },
  containerMid: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
  },
  containerEnd: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  auxText: {
    color: '#6E777E',
    marginLeft: theme.spacing(2),
  },
}));

export default useStyles;