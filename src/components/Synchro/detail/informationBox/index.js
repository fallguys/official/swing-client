import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import React from 'react';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import PersonIcon from '@material-ui/icons/Person';
import { getSynchroDetails } from 'redux/actions/admin/synchroObtainDetailsActions';
import { formatOnlyDate, formatOnlyTime } from 'utils/format/timer';
import { translateSynchType } from 'utils/format/translator';
import useStyles from './styles';

class InformationBox extends React.Component {
  componentDidMount() {
    const { getDetail, idSynchro } = this.props;
    getDetail(idSynchro);
  }

  render() {
    const { classes } = this.props;
    const { details } = this.props;
    return (
      <Grid container spacing={2} className={classes.root}>
        <Grid item xs={12} className={classes.containerFirst}>
          <InsertDriveFileIcon />
          <div className={classes.auxText}>
            { `${translateSynchType(details.data?.type)}` }
          </div>
        </Grid>
        <Grid item xs={12} className={classes.containerMid}>
          <PersonIcon />
          <div className={classes.auxText}>
            { details.data?.responsible }
          </div>
        </Grid>
        <Grid item xs={12} className={classes.containerEnd}>
          <QueryBuilderIcon />
          <div className={classes.auxText}>
            { `${formatOnlyTime(details.data?.initialDate)} - ${formatOnlyDate(details.data?.initialDate)}` }
          </div>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    details: state.synchroDetail.synchro,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (idSynchro) => dispatch(getSynchroDetails(idSynchro)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(InformationBox);
