import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import GroupIcon from '@material-ui/icons/Group';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PublicIcon from '@material-ui/icons/Public';
import { rowsBeneficiarie, rowsContagion, rowsWithdrawal } from '../../utils/export/rowsPerType';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(0),
    },
    display: 'flex',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    maxHeight: '40px',
  },
}));

function exportToCsv(filename, rows) {
  const processRow = function (row) {
    let finalVal = '';
    for (let j = 0; j < row.length; j++) {
      let innerValue = row[j] === null ? '' : row[j].toString();
      if (row[j] instanceof Date) {
        innerValue = row[j].toLocaleString();
      };
      let result = innerValue.replace(/"/g, '""');
      if (result.search(/("|,|\n)/g) >= 0) result = `"${result}"`;
      if (j > 0) finalVal += ',';
      finalVal += result;
    }
    return `${finalVal}\n`;
  };

  let csvFile = '';
  for (let i = 0; i < rows.length; i++) {
    csvFile += processRow(rows[i]);
  }

  const blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, filename);
  } else {
    const link = document.createElement('a');
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', filename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

export default function IconButtons() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (type) => {
    setAnchorEl(null);
  };

  const functionTest = (type) => {
    if (type === 'beneficarios') {
      exportToCsv('beneficiarios.csv', rowsBeneficiarie);
    } else if (type === 'puntos de retiro') {
      exportToCsv('puntos de retiro.csv', rowsWithdrawal);
    } else {
      exportToCsv('zonas de contagio.csv', rowsContagion);
    }
    handleClose();
  };

  return (
    <div className={classes.root}>
      <Button aria-controls="simple-menu" aria-haspopup="true" color="primary" onClick={handleClick} endIcon={<ArrowDropDownIcon />}>
        Plantilla de archivo
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={(e) => functionTest('beneficiarios')}>
          <ListItemIcon>
            <GroupIcon fontSize="small" />
          </ListItemIcon>
          Plantilla de Beneficiarios
        </MenuItem>
        <MenuItem onClick={(e) => functionTest('puntos de retiro')}>
          <ListItemIcon onClick={(e) => functionTest('zonas de contagio')}>
            <AccountBalanceIcon fontSize="small" />
          </ListItemIcon>
          Plantilla de Puntos de Retiro
        </MenuItem>
        <MenuItem onClick={(e) => functionTest('zonas de contagio')}>
          <ListItemIcon>
            <PublicIcon fontSize="small" />
          </ListItemIcon>
          Plantilla de Zonas de Contagio
        </MenuItem>
      </Menu>
    </div>
  );
}
