import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import PersonIcon from '@material-ui/icons/Person';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#ECECEC',
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
  },
  synchText1: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  synchText2: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
  },
  synchText3: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  auxText: {
    color: '#6E777E',
    marginLeft: theme.spacing(2),
  },
}));

export default function LoadInfo(props) {
  const { name = 'Rocío Muñoz' } = props;
  const classes = useStyles();
  const infoLoad = useSelector((state) => state.synchroUpload.upload);

  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className={classes.synchText1}>
            <InsertDriveFileIcon />
            <div className={classes.auxText}>
              {infoLoad.fileType !== null ? infoLoad.fileType : '--- (tipo de dato)' }
            </div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.synchText2}>
            <PersonIcon />
            <div className={classes.auxText}>
              {name}
            </div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.synchText3}>
            <QueryBuilderIcon />
            <div className={classes.auxText}>
              {infoLoad.date !== null ? infoLoad.date : '--- (hora de inicio de carga)' }
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
