import React, {useEffect} from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { useDispatch, useSelector } from 'react-redux';
import Router from 'next/router';
import { getCheckInfo } from '../../redux/actions/admin/scheduleActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

export default function SynchButton(props) {
  const { name } = props;
  const { type } = props;
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const checkInfo = useSelector((state) => state.schedule.checkInfo);

  useEffect(() => {
    dispatch(getCheckInfo());
  }, []);

  const checkUploadState = async () => {
    if (checkInfo.scheduleProcessing || checkInfo.synchrosProcessing) {
      await enqueueSnackbar(`No se pueden cargar ${type} porque hay sincronizaciones en curso, puede revisarlas desde el apartado de historial`, { variant: 'warning' });
      return;
    }
    await Router.push('/admin/synch/load');
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Button variant="contained" color="primary" disableElevation onClick={checkUploadState}>
        { name }
      </Button>
    </div>
  );
}
