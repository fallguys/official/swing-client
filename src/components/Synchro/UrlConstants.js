import config from 'config';

export const BENEFICIARIES_URL = `${config.api.baseUrl}/beneficiaries`;
export const WITHDRAWAL_URL = `${config.api.baseUrl}/withdrawalPoints`;
export const CONTAGION_URL = `${config.api.baseUrl}/contagionAreas`;
