import 'react-dropzone-uploader/dist/styles.css';
import Dropzone from 'react-dropzone-uploader';
import { SnackbarProvider, useSnackbar } from 'notistack';
import * as XLSX from 'xlsx';
import React, { useState } from 'react';
import csvValidatorBenef from 'utils/validation/csvValidatorBenef';
import csvValidatorWithdrawal from 'utils/validation/csvValidatorWithdrawal';
import csvValidatorContagion from 'utils/validation/csvValidatorContagion';
import { useDispatch, useSelector } from 'react-redux';
import { postSynchroFile, setFile } from 'redux/actions/admin/synchroUploadActions';
import PublishIcon from '@material-ui/icons/Publish';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import moment from 'moment';
import Router from 'next/router';
import { BENEFICIARIES_URL, CONTAGION_URL, WITHDRAWAL_URL } from './UrlConstants';

let typeFile = 'Initial';

function MyUploader() {
  const [columns, setColumns] = useState([]);
  const synchroFile = useSelector((state) => state.synchroUpload.upload);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const processData = (dataString) => {
    const dataStringLines = dataString.split(/\r\n|\n/);
    const headers = dataStringLines[0].split(/,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/);
    // prepare columns list from headers
    const columns = headers.map((c) => ({
      name: c,
      selector: c,
    }));
    setColumns(columns);
  };

  // specify upload params and url for your files
  const getUploadParams = ({ file, meta }) => {
    // her put conditionals for typefiles url return no necessary redux
    return { url: 'https://httpbin.org/post' };
  };
  // receives array of files that are done uploading when submit button is clicked
  const handleSubmit = async (files, allFiles) => {
    if (synchroFile.fileType === null) {
      enqueueSnackbar('¡Error al importar archivo! No es posible detectar el tipo de dato a sincronizar ', {variant: 'error'});
      allFiles.forEach((f) => f.remove());
    } else {
      if (synchroFile.fileType === 'Beneficiarios') {
        dispatch(setFile(null, 'Beneficiarios', moment().format('DD/MM/YY - hh:mm')));
        await enqueueSnackbar('¡Sincronización en marcha! La sincronización ha iniciado', {variant: 'success'});
        await allFiles.forEach((f) => dispatch(postSynchroFile(BENEFICIARIES_URL, f)));
        await Router.push('/admin/synch/history');
        allFiles.forEach((f) => f.remove());
      }
      if (synchroFile.fileType === 'Puntos de retiro') {
        dispatch(setFile(null, 'Puntos de retiro', moment().format('DD/MM/YY - hh:mm')));
        await enqueueSnackbar('¡Sincronización en marcha! La sincronización ha iniciado', {variant: 'success'});
        await allFiles.forEach((f) => dispatch(postSynchroFile(WITHDRAWAL_URL, f)));
        await Router.push('/admin/synch/history');
        allFiles.forEach((f) => f.remove());
      }
      if (synchroFile.fileType === 'Zonas de contagio') {
        dispatch(setFile(null, 'Zonas de contagio', moment().format('DD/MM/YY - hh:mm')));
        await enqueueSnackbar('¡Sincronización en marcha! La sincronización ha iniciado', { variant: 'success' });
        await allFiles.forEach((f) => dispatch(postSynchroFile(CONTAGION_URL, f)));
        await Router.push('/admin/synch/history');
        allFiles.forEach((f) => f.remove());
      }
    }
  };
  // called every time a file's `status` changes
  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === 'rejected_file_type') {
      enqueueSnackbar('¡Formato incorrecto! El archivo cargado no posee el formato .csv o excel', { variant: 'error' });console.log(csvValidatorBenef);
    } else if (status === 'removed') {
      dispatch(setFile(null, null, null));
    } else {
      const reader = new FileReader();
      reader.onload = (evt) => {
        /* Parse data */
        const bstr = evt.target.result;
        const wb = XLSX.read(bstr, { type: 'binary' });
        /* Get first worksheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        /* Convert array of arrays */
        const data = XLSX.utils.sheet_to_csv(ws, { header: 1 });
        processData(data);
      };
      reader.readAsBinaryString(file);
      if (status === 'done') {
        typeFile = 'Initial';
        if (csvValidatorBenef.length === columns.length) {
          for (let i = 0; i < csvValidatorBenef.length; i++) {
            if (csvValidatorBenef[i].name !== columns[i].name) {
              typeFile = 'Error';
              break;
            }
          }
          if (typeFile !== 'Error') {
            dispatch(setFile(file, 'Beneficiarios', '--- (hora de inicio de carga)'));
          }
        }
        if (csvValidatorWithdrawal.length === columns.length) {
          for (let i = 0; i < csvValidatorWithdrawal.length; i++) {
            if (csvValidatorWithdrawal[i].name !== columns[i].name) {
              typeFile = 'Error';
              break;
            }
          }
          if (typeFile !== 'Error') {
            dispatch(setFile(file, 'Puntos de retiro', '--- (hora de inicio de carga)'));
          };
        }
        if (csvValidatorContagion.length === columns.length) {
          for (let i = 0; i < csvValidatorContagion.length; i++) {
            if (csvValidatorContagion[i].name !== columns[i].name) {
              typeFile = 'Error';
              break;
            }
          }
          if (typeFile !== 'Error') {
            dispatch(setFile(file, 'Zonas de contagio', '--- (hora de inicio de carga)'));
          }
        }
      }
    }
  };

  return (
    <Dropzone
      getUploadParams={getUploadParams}
      onChangeStatus={handleChangeStatus}
      onSubmit={handleSubmit}
      accept=".xlsm,.csv,.xls,.xlsx"
      inputContent={(
        <Grid key="1" container direction="column" justify="center" alignItems="center">
          <Grid item xs={6}>
            <PublishIcon style={{ fontSize: 80, color: '#000000' }} />
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h6"> Arrastra hasta aquí o selecciona el archivo de datos </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="body2">Formatos permitidos (.xlsm, .csv, .xls, .xlsx)</Typography>
          </Grid>
        </Grid>
      )}
      submitButtonContent="Iniciar la carga de datos"
      maxFiles={1}
      styles={{
        dropzone: { width: '100%', height: 300, overflow: 'hidden', backgroundColor: '#ECECEC' },
        inputLabel: { color: '#707070' },
        submitButton: { backgroundColor: '#29985E', borderRadius: '0px' },
        dropzoneReject: { borderColor: '#F2DEDE', backgroundColor: '#F2DEDE' },
        dropzoneActive: { borderColor: '#29985E' },
      }}
    />
  );
};

export default function SynchDropzone() {
  const notistackRef = React.createRef();
  return (
    <SnackbarProvider
      ref={notistackRef}
      maxSnack={3}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      autoHideDuration={3000}
      action={(key) => (
        <IconButton key={key} onClick={() => { notistackRef.current.closeSnackbar(key); }} size="small">
          <CloseIcon fontSize="inherit" />
        </IconButton>
      )}
    >
      <MyUploader />
    </SnackbarProvider>
  );
}

