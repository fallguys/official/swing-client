import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { makeStyles } from '@material-ui/core/styles';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    maxHeight: '40px',
  },
}));
export default function SimpleMenu(props) {
  const { onClickPDF, onClickExcel } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const classes = useStyles();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (type) => {
    if (type === 'XLS') {
      onClickExcel('XLS');
    } else { onClickPDF('PDF'); }
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <Button aria-controls="simple-menu" aria-haspopup="true" color="primary" onClick={handleClick} endIcon={<ArrowDropDownIcon />}>
        Exportar
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => handleClose('XLS')}>
          <ListItemIcon>
            <AttachFileIcon fontSize="small" />
          </ListItemIcon>
          XLS
        </MenuItem>
        <MenuItem onClick={() => handleClose('PDF')}>
          <ListItemIcon>
            <PictureAsPdfIcon fontSize="small" />
          </ListItemIcon>
          PDF
        </MenuItem>
      </Menu>
    </div>
  );
}
