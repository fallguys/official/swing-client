import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import withStyles from '@material-ui/core/styles/withStyles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
    marginTop: theme.spacing(1),
  },
}));

const StyleChip = withStyles({
  root: {
    backgroundColor: '#245269',
    color: '#FFFFFF',
  },
})(Chip);

function SynchChips(props) {
  const classes = useStyles();
  const { date = 'ninguna' } = props;
  const headerDate = 'Última Actualización: ';
  const headerActives = 'Total de activos: ';
  const { cantActives = '0' } = props;
  return (
    <div className={classes.root}>
      <StyleChip
        icon={<AutorenewIcon />}
        label={headerDate.concat(date)}
        color="secondary"
      />
      <StyleChip
        icon={<HowToRegIcon />}
        label={headerActives.concat(cantActives)}
        color="secondary"
      />
    </div>
  );
}

export default SynchChips;
