export const SET_FAMILY_CODE = 'SET_FAMILY_CODE';
export const SET_SELECTED_POINTS_ERROR = 'SET_SELECTED_POINTS_ERROR';
export const SET_SELECTED_POINTS_SUCCESS = 'SET_SELECTED_POINTS_SUCCESS';
export const FETCH_DEPARTMENT_REQUEST = 'FETCH_DEPARTMENT_REQUEST';
export const FETCH_DEPARTMENT_SUCCESS = 'FETCH_DEPARTMENT_SUCCESS';
export const FETCH_DEPARTMENT_FAILURE = 'FETCH_DEPARTMENT_FAILURE';
export const FETCH_PROVINCE_REQUEST = 'FETCH_PROVINCE_REQUEST';
export const FETCH_PROVINCE_SUCCESS = 'FETCH_PROVINCE_SUCCESS';
export const FETCH_PROVINCE_FAILURE = 'FETCH_PROVINCE_FAILURE';
export const FETCH_DISTRICT_REQUEST = 'FETCH_DISTRICT_REQUEST';
export const FETCH_DISTRICT_SUCCESS = 'FETCH_DISTRICT_SUCCESS';
export const FETCH_DISTRICT_FAILURE = 'FETCH_DISTRICT_FAILURE';
export const FETCH_WITHDRAWAL_POINTS_REQUEST = 'FETCH_WITHDRAWAL_POINTS_REQUEST';
export const FETCH_WITHDRAWAL_POINTS_SUCCESS = 'FETCH_WITHDRAWAL_POINTS_SUCCESS';
export const FETCH_WITHDRAWAL_POINTS_FAILURE = 'FETCH_WITHDRAWAL_POINTS_FAILURE';
export const POST_SELECTED_POINTS_REQUEST = 'POST_SELECTED_POINTS_REQUEST';
export const POST_SELECTED_POINTS_SUCCESS = 'POST_SELECTED_POINTS_SUCCESS';
export const POST_SELECTED_POINTS_FAILURE = 'POST_SELECTED_POINTS_FAILURE';
