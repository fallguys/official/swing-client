import * as types from 'redux/types/admin/homeTypes';

const initialState = {
  loading: false,
  error: false,
  home: {
    data: null,
  },
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_HOME_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_HOME_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        home: {
          data: action.payload,
        },
      };
    case types.GET_HOME_DETAILS_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        synchro: {
          data: null,
        },
      };
    default:
      return { ...state };
  }
};

export default homeReducer;
