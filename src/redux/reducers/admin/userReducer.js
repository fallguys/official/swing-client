import * as types from 'redux/types/admin/userTypes';

const initialState = {
  name: 'guest',
  loading: false,
  error: '',
  posts: [{ id: 1, title: 'Hello' }],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_NAME:
      return {
        ...state,
        name: action.payload,
      };
    case types.FETCH_POSTS_REQUEST:
      return {
        ...state,
        loading: action.payload,
      };
    case types.FETCH_POSTS_SUCCESS:
      return {
        ...state,
        loading: false,
        posts: action.payload,
        error: '',
      };
    case types.FETCH_POSTS_FAILURE:
      return {
        ...state,
        loading: false,
        posts: [],
        error: action.payload,
      };
    default:
      return { ...state };
  }
};

export default userReducer;
