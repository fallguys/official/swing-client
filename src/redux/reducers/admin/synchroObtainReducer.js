import * as types from 'redux/types/admin/synchroObtainTypes';

const initialState = {
  loadingRow: false,
  response: {},
  rows: [],
  error: '',
};

const synchroObtainReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_ROWS_REQUEST:
      return {
        ...state,
        loadingRow: action.payload,
      };
    case types.FETCH_ROWS_SUCCESS:
      return {
        ...state,
        loadingRow: false,
        rows: action.payload,
        errorRow: '',
      };
    case types.FETCH_ROWS_FAILURE:
      return {
        ...state,
        loadingRow: false,
        rows: [],
        errorRow: action.payload,
      };
    default:
      return { ...state };
  }
};

export default synchroObtainReducer;

