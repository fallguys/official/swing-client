import * as types from 'redux/types/admin/incidentTypeTypes';

const initialState = {
  name: '',
  description: '',
  loadingRegister: false,
  response: {},
  loadingRow: false,
  rows: [],
  errorRegister: '',
  errorRow: '',
  posts: [{ id: 1, title: 'Hello' }],
};

const incidentTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_NAME:
      return {
        ...state,
        name: action.payload,
      };
    case types.SET_DESCRIPTION:
      return {
        ...state,
        description: action.payload,
      };
    case types.POST_INCIDENT_TYPE_REQUEST:
      return {
        ...state,
        loadingRegister: action.payload,
      };
    case types.POST_INCIDENT_TYPE_SUCCESS:
      return {
        ...state,
        loadingRegister: false,
        response: action.payload,
        errorRegister: '',
      };
    case types.POST_INCIDENT_TYPE_FAILURE:
      return {
        ...state,
        loadingRegister: false,
        response: {},
        errorRegister: action.payload,
      };
    case types.FETCH_ROWS_REQUEST:
      return {
        ...state,
        loadingRow: action.payload,
      };
    case types.FETCH_ROWS_SUCCESS:
      return {
        ...state,
        loadingRow: false,
        rows: action.payload,
        errorRow: '',
      };
    case types.FETCH_ROWS_FAILURE:
      return {
        ...state,
        loadingRow: false,
        rows: [],
        errorRow: action.payload,
      };
    default:
      return { ...state };
  }
};

export default incidentTypeReducer;
