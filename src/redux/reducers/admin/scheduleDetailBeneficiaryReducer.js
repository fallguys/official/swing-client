import * as types from 'redux/types/admin/scheduleDetailBeneficiaryTypes';

const initialState = {
  loading: false,
  response: {},
  beneficiaries: [],
  error: '',
  totalBeneficiaries: 0,
  selected: null,
  id: null,
  posts: [{ id: 1, title: 'Hello' }],
};

const scheduleDetailBeneficiaryReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_BENEFICIARY_ROWS_REQUEST:
      return {
        ...state,
        loading: action.payload,
      };
    case types.FETCH_BENEFICIARY_ROWS_SUCCESS:
      return {
        ...state,
        loading: false,
        beneficiaries: action.beneficiaries,
        totalBeneficiaries: action.totalBeneficiaries,
        error: '',
      };
    case types.FETCH_BENEFICIARY_ROWS_FAILURE:
      return {
        ...state,
        loading: false,
        beneficiaries: [],
        error: action.payload,
      };
    case types.SET_SELECTED:
      return {
        ...state,
        selected: action.payload,
      };
    case types.SET_ID:
      return {
        ...state,
        id: action.payload,
      };
    default:
      return { ...state };

  }
};

export default scheduleDetailBeneficiaryReducer;
