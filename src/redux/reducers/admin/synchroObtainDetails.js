import * as types from 'redux/types/admin/synchroObtainDetailsTypes';

const initialState = {
  loading: false,
  error: false,
  synchro: {
    data: null,
  },
};

const synchroObtainDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_SYNCRHONIZATION_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_SYNCRHONIZATION_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        synchro: {
          data: action.payload,
        },
      };
    case types.GET_SYNCRHONIZATION_DETAILS_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        synchro: {
          data: null,
        },
      };
    default:
      return { ...state };
  }
};

export default synchroObtainDetailsReducer;
