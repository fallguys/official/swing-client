import * as types from 'redux/types/admin/scheduleDetailInformationBoxTypes';

const initialState = {
  loading: false,
  informationBox: null,
  error: '',
  posts: [{ id: 1, title: 'Hello' }],
};

const scheduleDetailInformationBoxReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_INFORMATION_DETAIL_REQUEST:
      return {
        ...state,
        loading: action.payload,
      };
    case types.FETCH_INFORMATION_DETAIL_SUCCESS:
      return {
        ...state,
        loading: false,
        informationBox: action.informationBox,
        error: '',
      };
    case types.FETCH_INFORMATION_DETAIL_FAILURE:
      return {
        ...state,
        loading: false,
        informationBox: null,
        error: action.payload,
      };
    default:
      return { ...state };
  }
};

export default scheduleDetailInformationBoxReducer;
