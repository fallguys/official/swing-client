
import * as types from 'redux/types/admin/scheduleTypes';

const initialState = {
  schedules: {
    officials: [],
    totalOfficials: 0,
    tests: [],
    totalTests: 0,
  },
  error: null,
  loading: false,
  checkInfo: {
    scheduleProcessing: false,
    synchrosProcessing: false,
    data: null,
  },
  newSchedule: {
    error: null,
    loading: false,
    date: '--- - ---',
    type: null,
  },
};

const scheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SCHEDULE_GET_ALL_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.SCHEDULE_GET_ALL_OFFICIAL_SUCCESS:
      return {
        ...state,
        schedules: {
          ...state.schedules,
          officials: [...action.officials],
          totalOfficials: action.totalOfficials,
        },
        loading: false,
      };
    case types.SCHEDULE_GET_ALL_TEST_SUCCESS:
      return {
        ...state,
        schedules: {
          ...state.schedules,
          tests: [...action.tests],
          totalTests: action.totalTests,
        },
        loading: false,
      };
    case types.SCHEDULE_GET_ALL_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
    case types.SCHEDULE_GET_CHECK_INFO_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.SCHEDULE_GET_CHECK_INFO_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        checkInfo: {
          ...state.checkInfo,
          data: {},
        },
      };
    case types.SCHEDULE_GET_CHECK_INFO_SUCCESS:
      return {
        ...state,
        error: '',
        loading: false,
        checkInfo: {
          scheduleProcessing: action.scheduleProcessing,
          synchrosProcessing: action.synchrosProcessing,
          data: action.payload,
        },
      };
    case types.SCHEDULE_POST_NEW_SCHEDULE_REQUEST:
      return {
        ...state,
        newSchedule: {
          ...state,
          loading: true,
        },
      };
    case types.SCHEDULE_POST_NEW_SCHEDULE_SUCCESS:
      return {
        ...state,
        newSchedule: {
          error: null,
          loading: false,
        },
      };
    case types.SCHEDULE_POST_NEW_SCHEDULE_FAILED:
      return {
        ...state,
        newSchedule: {
          error: action.error,
          loading: false,
        },
      };
    case types.SCHEDULE_SET_DATE:
      return {
        ...state,
        newSchedule: {
          ...state,
          date: action.payload,
        },
      };
    case types.SCHEDULE_SET_TYPE:
      return {
        ...state,
        newSchedule: {
          ...state,
          type: action.payload,
        },
      };
  }
};

export default scheduleReducer;
