import * as types from 'redux/types/admin/synchroUploadTypes';

const initialState = {
  loading: false,
  response: null,
  error: null,
  upload: {
    file: null,
    fileType: null,
    date: null,
  },
};

const synchroUploadReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_FILE:
      return {
        ...state,
        upload: {
          fileType: action.payload.fileType,
          file: action.payload.file,
          date: action.payload.date,
        },
      };
    case types.POST_SYNCHRO_FILE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.POST_SYNCHRO_FILE_SUCCESS:
      return {
        ...state,
        loading: false,
        response: action.payload,
        errorRegister: null,
      };
    case types.POST_SYNCHRO_FILE_FAILURE:
      return {
        ...state,
        loadingRegister: false,
        response: null,
        errorRegister: action.payload,
      };
    default:
      return state;
  }
};

export default synchroUploadReducer;
