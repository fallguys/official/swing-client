import * as types from 'redux/types/admin/scheduleDetailWithdrawalTypes';

const initialState = {
  loading: false,
  response: {},
  withdrawals: [],
  totalWithdrawals: 0,
  error: '',
  posts: [{ id: 1, title: 'Hello' }],
};

const scheduleDetailWithdrawalReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_WITHDRAWALPOINT_ROWS_REQUEST:
      return {
        ...state,
        loading: action.payload,
      };
    case types.FETCH_WITHDRAWALPOINT_ROWS_SUCCESS:
      return {
        ...state,
        loading: false,
        withdrawals: action.withdrawals,
        totalWithdrawals: action.totalWithdrawals,
        error: '',
      };
    case types.FETCH_WITHDRAWALPOINT_ROWS_FAILURE:
      return {
        ...state,
        loading: false,
        withdrawals: [],
        error: action.payload,
      };
    default:
      return { ...state };

  }
};

export default scheduleDetailWithdrawalReducer;
