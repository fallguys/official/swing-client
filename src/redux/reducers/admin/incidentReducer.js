import * as types from 'redux/types/admin/incidentTypes';

const initialState = {
  loading: false,
  response: {},
  rows: [],
  error: '',
  posts: [{ id: 1, title: 'Hello' }],
};

const incidentReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_ROWS_REQUEST:
      return {
        ...state,
        loadingRow: action.payload,
      };
    case types.FETCH_ROWS_SUCCESS:
      return {
        ...state,
        loadingRow: false,
        rows: action.payload,
        errorRow: '',
      };
    case types.FETCH_ROWS_FAILURE:
      return {
        ...state,
        loadingRow: false,
        rows: [],
        errorRow: action.payload,
      };
    default:
      return { ...state };
  }
};

export default incidentReducer;
