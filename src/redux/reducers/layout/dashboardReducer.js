import * as types from 'redux/types/layout/dashboardTypes';

const initialState = {
  firstView: 'home',
  firstName: 'Dashboard del cronograma actual',
  secondName: 'Dashboard',
  header: {
    view1: 'Dashboard del cronograma actual',
    view2: 'Dashboard',
  },
  path: 'home',
};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SELECT_FIRST_VIEW:
      return {
        ...state,
        firstView: action.payload.firstView,
        firstName: action.payload.firstName,
      };
    case types.SELECT_SECOND_VIEW:
      return {
        ...state,
        secondName: action.payload.secondName,
        header: {
          view1: state.firstName,
          view2: action.payload.secondName,
        },
        path: action.payload.path,
      };
    default:
      return {
        ...state,
      };
  }
};

export default dashboardReducer;
