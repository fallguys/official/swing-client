import { combineReducers } from 'redux';
import userReducer from './admin/userReducer';
import incidentTypeReducer from './admin/incidentTypeReducer';
import dashboardReducer from './layout/dashboardReducer';
import incidentReducer from './admin/incidentReducer';
import synchroObtainReducer from './admin/synchroObtainReducer';
import synchroUploadReducer from './admin/synchroUploadReducer';
import consultReducer from './bonus/consultReducer';
import bankReducer from './bank/bankReducer';
import scheduleReducer from './admin/scheduleReducer';
import reportIncidentReducer from './bonus/reportIncidentReducer';
import scheduleDetailWithdrawalReducer from './admin/scheduleDetailWithdrawalReducer';
import scheduleDetailBeneficiaryReducer from './admin/scheduleDetailBeneficiaryReducer';
import scheduleDetailInformationBoxReducer from './admin/scheduleDetailInformationBoxReducer';
import synchroObtainDetailsReducer from './admin/synchroObtainDetails';
import chooseWithdrawalPointReducer from './bonus/chooseWithdrawalPointReducer';
import homeReducer from './admin/homeReducer';

const reducers = {
  user: userReducer,
  dashboard: dashboardReducer,
  incidentType: incidentTypeReducer,
  incident: incidentReducer,
  synchroObtain: synchroObtainReducer,
  synchroUpload: synchroUploadReducer,
  consult: consultReducer,
  schedule: scheduleReducer,
  bank: bankReducer,
  bonusConsult: consultReducer,
  reportIncident: reportIncidentReducer,
  scheduleDetailWithdrawal: scheduleDetailWithdrawalReducer,
  scheduleDetailBeneficiary: scheduleDetailBeneficiaryReducer,
  scheduleDetailInformationBox: scheduleDetailInformationBoxReducer,
  synchroDetail: synchroObtainDetailsReducer,
  chooseWithdrawalPoint: chooseWithdrawalPointReducer,
  home: homeReducer,
};

export default combineReducers(reducers);
