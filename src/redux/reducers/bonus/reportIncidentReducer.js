import * as types from 'redux/types/bonus/reportIncidentTypes';

const initialState = {
  familyCode: '',
  incidentsTypes: {
    data: [],
    loading: false,
    error: '',
  },
  form: {
    data: {
      incidentTypeId: null,
      message: null,
    },
    loading: false,
    error: '',
    success: '',
    alertError: false,
    alertSuccess: false,
  },
};

const reportIncidentReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ALERT_ERROR:
      return {
        ...state,
        form: {
          ...state.form,
          alertError: action.payload,
        },
      };
    case types.SET_ALERT_SUCCESS:
      return {
        ...state,
        form: {
          ...state.form,
          alertSuccess: action.payload,
        },
      };
    case types.SET_FAMILY_CODE:
      return {
        ...state,
        familyCode: action.payload,
      };
    case types.SET_INCIDENT_TYPE:
      return {
        ...state,
        form: {
          ...state.form,
          data: {
            ...state.form.data,
            incidentTypeId: action.payload,
          },
        },
      };
    case types.SET_MESSAGE:
      return {
        ...state,
        form: {
          ...state.form,
          data: {
            ...state.form.data,
            message: action.payload,
          },
        },
      };
    case types.FETCH_INCIDENTS_REQUEST:
      return {
        ...state,
        incidentsTypes: {
          ...state.incidentsTypes,
          data: [],
          loading: true,
        },
      };
    case types.FETCH_INCIDENTS_SUCCESS:
      return {
        ...state,
        incidentsTypes: {
          ...state.incidentsTypes,
          data: action.payload,
          loading: false,
          error: '',
        },
      };
    case types.FETCH_INCIDENTS_FAILURE:
      return {
        ...state,
        incidentsTypes: {
          ...state.incidentsTypes,
          loading: false,
          data: [],
          error: action.payload,
        },
      };
    case types.POST_INCIDENT_REQUEST:
      return {
        ...state,
        form: {
          ...state.form,
          loading: true,
          error: '',
          success: '',
        },
      };
    case types.POST_INCIDENT_SUCCESS:
      return {
        ...state,
        form: {
          ...state.form,
          loading: false,
          error: '',
          success: action.payload,
        },
      };
    case types.POST_INCIDENT_FAILURE:
      return {
        ...state,
        form: {
          ...state.form,
          loading: false,
          success: '',
          error: action.payload,
        },
      };
    default:
      return {
        ...state,
      };
  }
};

export default reportIncidentReducer;
