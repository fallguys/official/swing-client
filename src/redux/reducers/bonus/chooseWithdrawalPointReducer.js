import * as types from 'redux/types/bonus/chooseWithdrawalPointTypes';

const initialState = {
  familyCode: '',
  department: {
    data: [],
    loading: false,
    error: {
      status: false,
      response: null,
    },
  },
  province: {
    data: [],
    loading: false,
    error: {
      status: false,
      response: null,
    },
  },
  district: {
    data: [],
    loading: false,
    error: {
      status: false,
      response: null,
    },
  },
  withdrawalPoints: {
    data: [],
    loading: false,
    error: {
      status: false,
      response: null,
    },
  },
  selectedPoints: {
    loading: false,
    error: {
      status: false,
      response: null,
    },
    success: {
      status: false,
      response: null,
    },
  },
};

const chooseWithdrawalPointReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_FAMILY_CODE:
      return {
        ...state,
        familyCode: action.payload,
      };
    case types.SET_SELECTED_POINTS_SUCCESS:
      return {
        ...state,
        selectedPoints: {
          ...state.selectedPoints,
          success: {
            status: action.payload.status,
            response: action.payload.response,
          },
        },
      };
    case types.SET_SELECTED_POINTS_ERROR:
      return {
        ...state,
        selectedPoints: {
          ...state.selectedPoints,
          error: {
            status: action.payload.status,
            response: action.payload.response,
          },
        },
      };
    case types.FETCH_DEPARTMENT_REQUEST:
      return {
        ...state,
        department: {
          ...state.department,
          loading: true,
          error: {
            status: false,
            response: null,
          },
        },
        province: {
          ...state.province,
          data: [],
        },
        district: {
          ...state.district,
          data: [],
        },
      };
    case types.FETCH_DEPARTMENT_SUCCESS:
      return {
        ...state,
        department: {
          ...state.department,
          loading: false,
          data: action.payload,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_DEPARTMENT_FAILURE:
      return {
        ...state,
        department: {
          ...state.department,
          loading: false,
          data: [],
          error: {
            status: true,
            response: action.payload,
          },
        },
      };
    case types.FETCH_DISTRICT_REQUEST:
      return {
        ...state,
        district: {
          ...state.district,
          loading: true,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_DISTRICT_SUCCESS:
      return {
        ...state,
        district: {
          ...state.department,
          loading: false,
          data: action.payload,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_DISTRICT_FAILURE:
      return {
        ...state,
        district: {
          ...state.department,
          loading: false,
          data: [],
          error: {
            status: true,
            response: action.payload,
          },
        },
      };
    case types.FETCH_PROVINCE_REQUEST:
      return {
        ...state,
        province: {
          ...state.district,
          loading: true,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_PROVINCE_SUCCESS:
      return {
        ...state,
        province: {
          ...state.department,
          loading: false,
          data: action.payload,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_PROVINCE_FAILURE:
      return {
        ...state,
        province: {
          ...state.department,
          loading: false,
          data: [],
          error: {
            status: true,
            response: action.payload,
          },
        },
      };
    case types.FETCH_WITHDRAWAL_POINTS_REQUEST:
      return {
        ...state,
        withdrawalPoints: {
          ...state.withdrawalPoints,
          loading: true,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_WITHDRAWAL_POINTS_SUCCESS:
      return {
        ...state,
        withdrawalPoints: {
          ...state.withdrawalPoints,
          loading: false,
          data: action.payload,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_WITHDRAWAL_POINTS_FAILURE:
      return {
        ...state,
        withdrawalPoints: {
          ...state.withdrawalPoints,
          loading: false,
          data: [],
          error: {
            status: true,
            response: action.payload,
          },
        },
      };
    case types.POST_SELECTED_POINTS_REQUEST:
      return {
        ...state,
        selectedPoints: {
          ...state.selectedPoints,
          loading: true,
          error: {
            status: false,
            response: null,
          },
          success: {
            status: false,
            response: null,
          },
        },
      };
    case types.POST_SELECTED_POINTS_SUCCESS:
      return {
        ...state,
        selectedPoints: {
          ...state.selectedPoints,
          loading: false,
          error: {
            status: false,
            response: null,
          },
          success: {
            status: true,
            response: action.payload,
          },
        },
      };
    case types.POST_SELECTED_POINTS_FAILURE:
      return {
        ...state,
        selectedPoints: {
          ...state.selectedPoints,
          loading: false,
          error: {
            status: true,
            response: action.payload,
          },
          success: {
            status: false,
            response: null,
          },
        },
      };
    default:
      return {
        ...state,
      };
  }
};

export default chooseWithdrawalPointReducer;
