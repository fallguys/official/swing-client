import * as types from 'redux/types/bonus/consultTypes';

const initialState = {
  familyCode: '',
  schedule: {
    data: null,
    loading: false,
    error: {
      status: false,
      response: null,
    },
  },
};

const consultReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_FAMILY_CODE:
      return {
        ...state,
        familyCode: action.payload,
      };
    case types.SET_SCHEDULE_ERROR:
      return {
        ...state,
        schedule: {
          ...state.schedule,
          error: {
            status: action.payload,
            response: null,
          },
        },
      };
    case types.FETCH_SCHEDULE_DATA_REQUEST:
      return {
        ...state,
        schedule: {
          ...state.schedule,
          data: null,
          loading: true,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_SCHEDULE_DATA_SUCCESS:
      return {
        ...state,
        schedule: {
          ...state.schedule,
          loading: false,
          data: action.payload,
          error: {
            status: false,
            response: null,
          },
        },
      };
    case types.FETCH_SCHEDULE_DATA_FAILURE:
      return {
        ...state,
        schedule: {
          ...state.schedule,
          loading: false,
          data: null,
          error: {
            status: true,
            response: action.payload,
          },
        },
      };
    default:
      return {
        ...state,
      };
  }
};

export default consultReducer;
