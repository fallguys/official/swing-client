import * as types from 'redux/types/bank/bankTypes';

const initialState = {
  show: false,
  beneficiarieCode: '',
  withdrawalCode: '',
  retired: false,
  changed: false,
  initialWithdrawal: true,
  initialBeneficiarie: true,
  withdrawal: {
    data: null,
    loading: false,
    error: '',
  },
  beneficiarie: {
    data: null,
    loading: false,
    error: '',
  },
};

const bankReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CHANGED:
      return {
        ...state,
        changed: action.payload,
      };
    case types.SET_RETIRED:
      return {
        ...state,
        retired: action.payload,
      };
    case types.SET_INITIAL_WITHDRAWAL:
      return {
        ...state,
        initialWithdrawal: false,
      };
    case types.SET_INITIAL_BENEFICIARIE:
      return {
        ...state,
        initialBeneficiarie: false,
      };
    case types.SET_SHOW:
      return {
        ...state,
        show: action.payload,
      };
    case types.SET_BENEFICIARIE_CODE:
      return {
        ...state,
        beneficiarieCode: action.payload,
      };
    case types.FETCH_BENEFICIARIE_DATA_REQUEST:
      return {
        ...state,
        beneficiarie: {
          ...state.beneficiarie,
          loading: true,
        },
      };
    case types.FETCH_BENEFICIARIE_DATA_SUCCESS:
      return {
        ...state,
        beneficiarie: {
          ...state.beneficiarie,
          loading: false,
          data: action.payload,
          error: '',
        },
      };
    case types.FETCH_BENEFICIARIE_DATA_FAILURE:
      return {
        ...state,
        beneficiarie: {
          ...state.beneficiarie,
          loading: false,
          data: {},
          error: action.payload,
        },
      };
    default:
      return {
        ...state,
      };
    case types.SET_WITHDRAWAL_CODE:
      return {
        ...state,
        withdrawalCode: action.payload,
      };
    case types.FETCH_WITHDRAWAL_DATA_REQUEST:
      return {
        ...state,
        withdrawal: {
          ...state.withdrawal,
          loading: true,
        },
      };
    case types.FETCH_WITHDRAWAL_DATA_SUCCESS:
      return {
        ...state,
        withdrawal: {
          ...state.withdrawal,
          loading: false,
          data: action.payload,
          error: '',
        },
      };
    case types.FETCH_WITHDRAWAL_DATA_FAILURE:
      return {
        ...state,
        withdrawal: {
          ...state.withdrawal,
          loading: false,
          data: {},
          error: action.payload,
        },
      };
  }
};

export default bankReducer;
