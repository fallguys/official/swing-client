import * as types from 'redux/types/bonus/consultTypes';
import consultService from '../../../services/bonus/consult-service';

export const setFamilyCode = (familyCode) => ({
  type: types.SET_FAMILY_CODE,
  payload: familyCode,
});

export const setScheduleError = (status) => ({
  type: types.SET_SCHEDULE_ERROR,
  payload: status,
});

export const fetchScheduleDataRequest = () => ({
  type: types.FETCH_SCHEDULE_DATA_REQUEST,
});

export const fetchScheduleDataSuccess = (data) => ({
  type: types.FETCH_SCHEDULE_DATA_SUCCESS,
  payload: data,
});

export const fetchScheduleDataFailure = (error) => ({
  type: types.FETCH_SCHEDULE_DATA_FAILURE,
  payload: error,
});

export const fetchScheduleData = (familyCode) => {
  return async function (dispatch) {
    dispatch(fetchScheduleDataRequest());
    try {
      const response = await consultService.getInfoBonus(familyCode);
      dispatch(fetchScheduleDataSuccess(response.data));
      if (response.data.found === 0) {
        dispatch(setScheduleError(true));
      }
    } catch (error) {
      dispatch(fetchScheduleDataFailure(error.message));
    }
  };
};
