import * as types from 'redux/types/bonus/reportIncidentTypes';
import axios from 'axios';
import config from 'config';

export const setAlertError = (value) => ({
  type: types.SET_ALERT_ERROR,
  payload: value,
});

export const setAlertSuccess = (value) => ({
  type: types.SET_ALERT_SUCCESS,
  payload: value,
});

export const setFamilyCode = (familyCode) => ({
  type: types.SET_FAMILY_CODE,
  payload: familyCode,
});

export const setIncidentType = (incidentType) => ({
  type: types.SET_INCIDENT_TYPE,
  payload: incidentType,
});

export const setMessage = (message) => ({
  type: types.SET_MESSAGE,
  payload: message,
});

export const fetchIncidentsTypesRequest = () => ({
  type: types.FETCH_INCIDENTS_REQUEST,
});

export const fetchIncidentsTypesSuccess = (data) => ({
  type: types.FETCH_INCIDENTS_SUCCESS,
  payload: data,
});

export const fetchIncidentsTypesFailure = (error) => ({
  type: types.FETCH_INCIDENTS_FAILURE,
  payload: error,
});

export const fetchIncidentsTypes = () => {
  return async function (dispatch) {
    dispatch(fetchIncidentsTypesRequest());
    const url = `${config.api.baseUrl}/incidentType`;
    try {
      const response = await axios.get(url);
      dispatch(fetchIncidentsTypesSuccess(response.data.data));
    } catch (error) {
      dispatch(fetchIncidentsTypesFailure(error));
    }
  };
};

export const postReportIncidentRequest = () => ({
  type: types.POST_INCIDENT_REQUEST,
});

export const postReportIncidentSuccess = (response) => ({
  type: types.POST_INCIDENT_SUCCESS,
  payload: response,
});

export const postReportIncidentFailure = (error) => ({
  type: types.POST_INCIDENT_FAILURE,
  payload: error,
});

export const postReportIncident = (data) => {
  return async function (dispatch) {
    dispatch(postReportIncidentRequest());
    try {
      const url = `${config.api.baseUrl}/incident`;
      const response = await axios.post(url, data);
      dispatch(postReportIncidentSuccess(response.data));
      if (response.data >= 0) {
        dispatch(setAlertSuccess(true));
      } else {
        dispatch(setAlertError(true));
      }
    } catch (error) {
      dispatch(postReportIncidentFailure(error));
      dispatch(setAlertError(true));
    }
  };
};
