import * as types from 'redux/types/bonus/chooseWithdrawalPointTypes';
import chooseWithdrawalPointService from '../../../services/bonus/choose-withdrawal-points-service';

export const setFamilyCode = (familyCode) => ({
  type: types.SET_FAMILY_CODE,
  payload: familyCode,
});

export const setSelectedPointsSuccess = (status, response = 'Éxito') => ({
  type: types.SET_SELECTED_POINTS_SUCCESS,
  payload: {
    status,
    response,
  },
});

export const setSelectedPointsError = (status, response = 'Error') => ({
  type: types.SET_SELECTED_POINTS_ERROR,
  payload: {
    status,
    response,
  },
});

export const fetchDepartmentRequest = () => ({
  type: types.FETCH_DEPARTMENT_REQUEST,
});

export const fetchDepartmentSuccess = (data) => ({
  type: types.FETCH_DEPARTMENT_SUCCESS,
  payload: data,
});

export const fetchDepartmentFailure = (error) => ({
  type: types.FETCH_DEPARTMENT_FAILURE,
  payload: error,
});

export const fetchDepartment = () => {
  return async function (dispatch) {
    dispatch(fetchDepartmentRequest());
    try {
      const response = await chooseWithdrawalPointService.getPlace('', 'DEPARTMENT');
      dispatch(fetchDepartmentSuccess(response.data));
    } catch (error) {
      dispatch(fetchDepartmentFailure(error));
    }
  };
};

export const fetchProvinceRequest = () => ({
  type: types.FETCH_PROVINCE_REQUEST,
});

export const fetchProvinceSuccess = (data) => ({
  type: types.FETCH_PROVINCE_SUCCESS,
  payload: data,
});

export const fetchProvinceFailure = (error) => ({
  type: types.FETCH_PROVINCE_FAILURE,
  payload: error,
});

export const fetchProvince = (id) => {
  return async function (dispatch) {
    dispatch(fetchProvinceRequest());
    try {
      const response = await chooseWithdrawalPointService.getPlace(id, 'PROVINCE');
      dispatch(fetchProvinceSuccess(response.data));
    } catch (error) {
      dispatch(fetchProvinceFailure(error));
    }
  };
};

export const fetchDistrictRequest = () => ({
  type: types.FETCH_DISTRICT_REQUEST,
});

export const fetchDistrictSuccess = (data) => ({
  type: types.FETCH_DISTRICT_SUCCESS,
  payload: data,
});

export const fetchDistrictFailure = (error) => ({
  type: types.FETCH_DISTRICT_FAILURE,
  payload: error,
});

export const fetchDistrict = (id) => {
  return async function (dispatch) {
    dispatch(fetchDistrictRequest());
    try {
      const response = await chooseWithdrawalPointService.getPlace(id, 'DISTRICT');
      dispatch(fetchDistrictSuccess(response.data));
    } catch (error) {
      dispatch(fetchDistrictFailure(error));
    }
  };
};

export const fetchWithdrawalPointsRequest = () => ({
  type: types.FETCH_WITHDRAWAL_POINTS_REQUEST,
});

export const fetchWithdrawalPointsSuccess = (response) => ({
  type: types.FETCH_WITHDRAWAL_POINTS_SUCCESS,
  payload: response,
});

export const fetchWithdrawalPointsFailure = (error) => ({
  type: types.FETCH_WITHDRAWAL_POINTS_FAILURE,
  payload: error,
});

export const fetchWithdrawalPoints = (ubigee) => {
  return async function (dispatch) {
    dispatch(fetchWithdrawalPointsRequest());
    try {
      const response = await chooseWithdrawalPointService.getWithdrawalPoints(ubigee);
      console.log(response.data);
      dispatch(fetchWithdrawalPointsSuccess(response.data));
    } catch (error) {
      dispatch(fetchWithdrawalPointsFailure(error));
    }
  };
};

export const postSelectedPointsRequest = () => ({
  type: types.POST_SELECTED_POINTS_REQUEST,
});

export const postSelectedPointsSuccess = (response) => ({
  type: types.POST_SELECTED_POINTS_SUCCESS,
  payload: response,
});

export const postSelectedPointsFailure = (error) => ({
  type: types.POST_SELECTED_POINTS_FAILURE,
  payload: error,
});

export const postSelectedPoints = (data) => {
  return async function (dispatch) {
    dispatch(postSelectedPointsRequest());
    try {
      const response = await chooseWithdrawalPointService.postWithdrawalPoints(data);
      console.log(response.data);
      dispatch(postSelectedPointsSuccess(response.data));
    } catch (error) {
      console.log(error);
      dispatch(postSelectedPointsFailure('Código erróneo'));
    }
  };
};
