import * as types from 'redux/types/layout/dashboardTypes';

export const setFirstView = (view, name) => ({
  type: types.SELECT_FIRST_VIEW,
  payload: {
    firstView: view,
    firstName: name,
  },
});

export const setSecondView = (path, name) => ({
  type: types.SELECT_SECOND_VIEW,
  payload: {
    path,
    secondName: name,
  },
});
