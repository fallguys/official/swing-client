import * as types from 'redux/types/bank/bankTypes';
import axios from 'axios';
import config from 'config';

export const setShow = (show) => ({
  type: types.SET_SHOW,
  payload: show,
});

export const setRetired = (state) => ({
  type: types.SET_RETIRED,
  payload: state,
});

export const setChanged = (state) => ({
  type: types.SET_CHANGED,
  payload: state,
});

export const setBeneficiarieCode = (beneficiarieCode) => ({
  type: types.SET_BENEFICIARIE_CODE,
  payload: beneficiarieCode,
});

export const setWithdrawalCode = (withdrawalCode) => ({
  type: types.SET_WITHDRAWAL_CODE,
  payload: withdrawalCode,
});

export const setInitialWithdrawal = (withdrawalInitial) => ({
  type: types.SET_INITIAL_WITHDRAWAL,
  payload: withdrawalInitial,
});

export const setInitialBeneficiarie = (beneficiarieInitial) => ({
  type: types.SET_INITIAL_BENEFICIARIE,
  payload: beneficiarieInitial,
});

export const fetchBeneficiarieDataRequest = () => ({
  type: types.FETCH_BENEFICIARIE_DATA_REQUEST,
});

export const fetchBeneficiarieDataSuccess = (data) => ({
  type: types.FETCH_BENEFICIARIE_DATA_SUCCESS,
  payload: data,
});

export const fetchBeneficiarieDataFailure = (error) => ({
  type: types.FETCH_BENEFICIARIE_DATA_FAILURE,
  payload: error,
});

export const fetchBeneficiarieData = (code, codePoint) => {
  return async function (dispatch) {
    dispatch(fetchBeneficiarieDataRequest());
    const url = `${config.api.baseUrl}/sim/participants/beneficiaries/${code}/checking?codePoint=${codePoint}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchBeneficiarieDataSuccess(response.data));
      dispatch(setRetired(response.data.retiredBonus));
      dispatch(setBeneficiarieCode(code));
    } catch (error) {
      dispatch(fetchBeneficiarieDataFailure(error));
    }
  };
};

export const fetchWithdrawalDataRequest = () => ({
  type: types.FETCH_WITHDRAWAL_DATA_REQUEST,
});

export const fetchWithdrawalDataSuccess = (data) => ({
  type: types.FETCH_WITHDRAWAL_DATA_SUCCESS,
  payload: data,
});

export const fetchWithdrawalDataFailure = (error) => ({
  type: types.FETCH_WITHDRAWAL_DATA_FAILURE,
  payload: error,
});

export const fetchWithdrawalData = (code) => {
  return async function (dispatch) {
    dispatch(fetchWithdrawalDataRequest());
    const url = `${config.api.baseUrl}/sim/participants/withdrawalPoints?codeWithdrawalPoint=${code}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchWithdrawalDataSuccess(response.data));
      dispatch(setWithdrawalCode(response.data.code));
    } catch (error) {
      dispatch(fetchWithdrawalDataFailure(error));
    }
  };
};

export const fetchRegisterRetirement = (code, codePoint) => {
  return async function (dispatch) {
    const url = `${config.api.baseUrl}/sim/participants/beneficiaries/${code}/bonus?codePoint=${codePoint}`;
    try {
      await axios.post(url);
      setShow(false);
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchRegisterIncidence = (code, codePoint) => {
  return async function (dispatch) {
    const url = `${config.api.baseUrl}/sim/participants/beneficiaries/${code}/feedback?codePoint=${codePoint}`;
    try {
      await axios.post(url);
      setShow(false);
    } catch (error) {
      console.log(error);
    }
  };
};
