import {
  SCHEDULE_GET_ALL_FAILED,
  SCHEDULE_GET_ALL_OFFICIAL_SUCCESS,
  SCHEDULE_GET_ALL_REQUEST,
  SCHEDULE_GET_ALL_TEST_SUCCESS,
  SCHEDULE_GET_CHECK_INFO_REQUEST,
  SCHEDULE_GET_CHECK_INFO_SUCCESS,
  SCHEDULE_POST_NEW_SCHEDULE_FAILED,
  SCHEDULE_POST_NEW_SCHEDULE_REQUEST,
  SCHEDULE_POST_NEW_SCHEDULE_SUCCESS,
  SCHEDULE_SET_DATE, SCHEDULE_SET_TYPE,
} from 'redux/types/admin/scheduleTypes';
import scheduleService from 'services/admin/schedule-service';

const GET_ALL_REQUEST = () => ({
  type: SCHEDULE_GET_ALL_REQUEST,
});

const GET_ALL_OFFICIAL_SUCCESS = (payload) => ({
  type: SCHEDULE_GET_ALL_OFFICIAL_SUCCESS,
  officials: payload.schedules.data,
  totalOfficials: payload.schedules.total,
});

const GET_ALL_TEST_SUCCESS = (payload) => ({
  type: SCHEDULE_GET_ALL_TEST_SUCCESS,
  tests: payload.schedules.data,
  totalTests: payload.schedules.total,
});

const GET_ALL_FAILED = (error) => ({
  type: SCHEDULE_GET_ALL_FAILED,
  error,
});

export const getAll = ({ type = 'OFFICIAL', page, pagesize }) => async (dispatch) => {
  try {
    dispatch(GET_ALL_REQUEST());
    const { data: schedules } = await scheduleService.getScheduleByType({ type, page, pagesize });
    type === 'OFFICIAL' ?
      dispatch(GET_ALL_OFFICIAL_SUCCESS({ schedules })) :
      dispatch(GET_ALL_TEST_SUCCESS({ schedules }));

  } catch (error) {
    dispatch(GET_ALL_FAILED(error));
    console.error(error);
  }
};

const GET_CHECK_INFO_REQUEST = () => ({
  type: SCHEDULE_GET_CHECK_INFO_REQUEST,
});

const GET_CHECK_INFO_SUCCESS = (data) => ({
  type: SCHEDULE_GET_CHECK_INFO_SUCCESS,
  payload: data,
  scheduleProcessing: data.scheduleProcessing,
  synchrosProcessing: data.synchrosProcessing,
});

const GET_CHECK_INFO_FAILED = (error) => ({
  type: SCHEDULE_GET_CHECK_INFO_SUCCESS,
  payload: error,
});

export const getCheckInfo = () => async (dispatch) => {
  try {
    dispatch(GET_CHECK_INFO_REQUEST());
    const checkInfo = await scheduleService.getCheckForInfo();
    console.log('CHECK INFO', checkInfo.data);
    await dispatch(GET_CHECK_INFO_SUCCESS(checkInfo.data));
  } catch (error) {
    dispatch(GET_CHECK_INFO_FAILED(error));
    console.error(error);
  }
};

const POST_NEW_SCHEDULE_REQUEST = () => ({
  type: SCHEDULE_POST_NEW_SCHEDULE_REQUEST,
});

const POST_NEW_SCHEDULE_SUCCESS = (data) => ({
  type: SCHEDULE_POST_NEW_SCHEDULE_SUCCESS,
});

const POST_NEW_SCHEDULE_FAILED = (error) => ({
  type: SCHEDULE_POST_NEW_SCHEDULE_FAILED,
  payload: error,
});

export const postNewSchedule = (schedule) => async (dispatch) => {
  try {
    dispatch(POST_NEW_SCHEDULE_REQUEST());
    await scheduleService.postNewSchedule(schedule);
    dispatch(POST_NEW_SCHEDULE_SUCCESS());
  } catch (error) {
    dispatch(POST_NEW_SCHEDULE_FAILED(error));
    console.error(error);
  }
};

export const setDate = (date) => ({
  type: SCHEDULE_SET_DATE,
  payload: date,
});

export const setType = (type) => ({
  type: SCHEDULE_SET_TYPE,
  payload: type,
});

