import * as types from 'redux/types/admin/synchroObtainDetailsTypes';
import synchroService from 'services/admin/synchro-service';

export const getSynchroDetailRequest = () => ({
  type: types.GET_SYNCRHONIZATION_DETAILS_REQUEST,
});

export const getSynchroDetailSuccess = (details) => ({
  type: types.GET_SYNCRHONIZATION_DETAILS_SUCCESS,
  payload: details,
});

export const getSynchroDetailFailed = (error) => ({
  type: types.GET_SYNCRHONIZATION_DETAILS_FAILED,
  payload: error,
});

export const getSynchroDetails = (idSynchro) => async (dispatch) => {
  try {
    dispatch(getSynchroDetailRequest());
    const details = await synchroService.getSynchroDetails(idSynchro);
    dispatch(getSynchroDetailSuccess(details.data));
  } catch (error) {
    dispatch(getSynchroDetailFailed(error));
  }
};
