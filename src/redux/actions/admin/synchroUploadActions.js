import * as types from 'redux/types/admin/synchroUploadTypes';
import axios from 'axios';
import synchroService from '../../../services/admin/synchro-service';

export const setFile = (file, fileType, date) => ({
  type: types.SET_FILE,
  payload: { file, fileType, date },
});

export const postSynchroFileRequest = () => ({
  type: types.POST_SYNCHRO_FILE_REQUEST,
  payload: true,
});

export const postSynchroFileSuccess = (response) => ({
  type: types.POST_SYNCHRO_FILE_SUCCESS,
  payload: response,
});

export const postSynchroFileFailure = (errorRegister) => ({
  type: types.POST_SYNCHRO_FILE_FAILURE,
  payload: errorRegister,
});

export const postSynchroFile = (url, data) => {
  return async function (dispatch) {
    dispatch(postSynchroFileRequest());

    try {
      const response = await synchroService.sendSynchroData(url, data.file);
      dispatch(postSynchroFileSuccess(response.data));

    } catch (error) {
      dispatch(postSynchroFileFailure(error.message));
    }
  };
};
