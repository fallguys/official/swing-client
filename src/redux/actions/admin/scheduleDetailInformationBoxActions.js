import * as types from 'redux/types/admin/scheduleDetailInformationBoxTypes';
import scheduleService from 'services/admin/schedule-service';

export const fetchInformationDetailRequest = () => ({
  type: types.FETCH_INFORMATION_DETAIL_REQUEST,
  payload: true,
});

export const fetchInformationDetailSuccess = (payload) => ({
  type: types.FETCH_INFORMATION_DETAIL_SUCCESS,
  informationBox: payload.informationBox,
});

export const fetchInformationDetailFailure = (error) => ({
  type: types.FETCH_INFORMATION_DETAIL_FAILURE,
  payload: error,
});

export const getAllDetailInformationBox = (codSchedule) => async (dispatch) => {
  try {
    dispatch(fetchInformationDetailRequest());
    const { data: informationBox } = await scheduleService.getScheduleDetailInformationBox(codSchedule);
    dispatch(fetchInformationDetailSuccess({ informationBox }));

  } catch (error) {
    dispatch(fetchInformationDetailFailure(error));
    console.log('ENTRA EN ERROR');
    console.error(error);
  }
};
