import scheduleService from 'services/admin/schedule-service';
import * as types from 'redux/types/admin/scheduleDetailBeneficiaryTypes';

export const fetchBeneficiaryRowsRequest = () => ({
  type: types.FETCH_BENEFICIARY_ROWS_REQUEST,
  payload: true,
});

export const fetchBeneficiaryRowsSuccess = (payload) => ({
  type: types.FETCH_BENEFICIARY_ROWS_SUCCESS,
  beneficiaries: payload.beneficiaries.data,
  totalBeneficiaries: payload.beneficiaries.total,
});

export const fetchRowsFailure = (error) => ({
  type: types.FETCH_BENEFICIARY_ROWS_FAILURE,
  payload: error,
});

export const setSelected = (id) => ({
  type: types.SET_SELECTED,
  payload: id,
});

export const setId = (id) => ({
  type: types.SET_ID,
  payload: id,
});

export const getAllDetailBeneficiary = ({ codSchedule, page, pagesize, initialDate, finalDate, beneficiaryCode, ubigee, withdrawalPointCode, state }) => async (dispatch) => {
  try {
    dispatch(fetchBeneficiaryRowsRequest());
    console.log('AUXILIO', initialDate);
    const { data: beneficiaries } = await scheduleService.getScheduleDetailBeneficiary({ codSchedule, page, pagesize, initialDate, finalDate, beneficiaryCode, ubigee, withdrawalPointCode, state });
    dispatch(fetchBeneficiaryRowsSuccess({ beneficiaries }));

  } catch (error) {
    dispatch(fetchRowsFailure(error));
    console.log('ENTRA EN ERROR');
    console.error(error);
  }
};
