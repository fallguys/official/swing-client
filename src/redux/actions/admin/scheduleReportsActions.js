import FileDownload from 'js-file-download';
import scheduleService from '../../../services/admin/schedule-service';

export const getBeneficiaryReport = (codSchedule, type, selectedInitialDate, selectedFinalDate, benefCode, state, ubigee, withdrawalPointCode) => async (dispatch) => {
  try {
    const response = await scheduleService.getScheduleBeneficiaryReport(codSchedule, type, selectedInitialDate, selectedFinalDate, benefCode, state, ubigee, withdrawalPointCode);
    if (type === 'PDF') {
      FileDownload(response.data, 'Reporte_Beneficiarios.pdf');
    } else {
      FileDownload(response.data, 'Reporte_Beneficiarios.xls');
    }
  } catch (error) {
    console.error(error);
  }
};

export const getWithdrawalReport = (codSchedule, type, ubigee, withdrawalPointCode) => async (dispatch) => {
  try {
    const response = await scheduleService.getScheduleWithdrawalReport(codSchedule, type, ubigee, withdrawalPointCode);
    if (type === 'PDF') {
      FileDownload(response.data, 'Reporte_Puntos_De_Retiro.pdf');
    } else {
      FileDownload(response.data, 'Reporte_Puntos_De_Retiro.xls');
    }
  } catch (error) {
    console.error(error);
  }
};
