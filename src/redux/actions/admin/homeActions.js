import * as types from 'redux/types/admin/homeTypes';
import homeService from 'services/admin/home-service';

export const getHomeDetailRequest = () => ({
  type: types.GET_HOME_DETAILS_REQUEST,
});

export const getHomeDetailSuccess = (details) => ({
  type: types.GET_HOME_DETAILS_SUCCESS,
  payload: details,
});

export const getHomeDetailFailed = (error) => ({
  type: types.GET_HOME_DETAILS_FAILED,
  payload: error,
});

export const getHomeDetails = () => async (dispatch) => {
  try {
    dispatch(getHomeDetailRequest());
    const details = await homeService.getHomeDetails();
    dispatch(getHomeDetailSuccess(details.data));
  } catch (error) {
    dispatch(getHomeDetailFailed(error));
  }
};
