import * as types from 'redux/types/admin/synchroObtainTypes';
import axios from 'axios';
import config from 'config';

export const fetchRowsRequest = () => ({
  type: types.FETCH_ROWS_REQUEST,
  payload: true,
});

export const fetchRowsSuccess = (rows) => ({
  type: types.FETCH_ROWS_SUCCESS,
  payload: rows,
});

export const fetchRowsFailure = (errorRows) => ({
  type: types.FETCH_ROWS_FAILURE,
  payload: errorRows,
});

export const fetchRowsContagion = (page = 0, pageSize = 10) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/contagionAreas?page=${page}&pagesize=${pageSize}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};

export const fetchRowsBeneficiaries = (page, pageSize) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/beneficiaries?page=${page}&pagesize=${pageSize}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};

export const fetchRowsWithdrawals = (page, pageSize) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/withdrawalPoints?page=${page}&pagesize=${pageSize}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};

export const fetchRowsHistorial = (page = 0, pageSize = 10) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/synchros?page=${page}&pagesize=${pageSize}`;
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};