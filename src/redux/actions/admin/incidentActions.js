import * as types from 'redux/types/admin/incidentTypes';
import axios from 'axios';
import config from 'config';

export const fetchRowsRequest = () => ({
  type: types.FETCH_ROWS_REQUEST,
  payload: true,
});

export const fetchRowsSuccess = (rows) => ({
  type: types.FETCH_ROWS_SUCCESS,
  payload: rows,
});

export const fetchRowsFailure = (errorRows) => ({
  type: types.FETCH_ROWS_FAILURE,
  payload: errorRows,
});

export const fetchRows = (page = 0) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/incident`;
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};

