import * as types from 'redux/types/admin/incidentTypeTypes';
import axios from 'axios';
import config from 'config';

export const setName = (name) => ({
  type: types.SET_NAME,
  payload: name,
});

export const setDescription = (description) => ({
  type: types.SET_DESCRIPTION,
  payload: description,
});

export const postIncidentTypeRequest = () => ({
  type: types.POST_INCIDENT_TYPE_REQUEST,
  payload: true,
});

export const postIncidentTypeSuccess = (response) => ({
  type: types.POST_INCIDENT_TYPE_SUCCESS,
  payload: response,
});

export const postIncidentTypeFailure = (errorRegister) => ({
  type: types.POST_INCIDENT_TYPE_FAILURE,
  payload: errorRegister,
});

export const postIncidentType = (data) => {
  return async function (dispatch) {
    dispatch(postIncidentTypeRequest());
    try {
      const response = await axios.post(`${config.api.baseUrl}/incidentType`, data);
      dispatch(postIncidentTypeSuccess(response.data));
    } catch (error) {
      dispatch(postIncidentTypeFailure(error.message));
    }
  };
};

export const fetchRowsRequest = () => ({
  type: types.FETCH_ROWS_REQUEST,
  payload: true,
});

export const fetchRowsSuccess = (rows) => ({
  type: types.FETCH_ROWS_SUCCESS,
  payload: rows,
});

export const fetchRowsFailure = (errorRows) => ({
  type: types.FETCH_ROWS_FAILURE,
  payload: errorRows,
});

export const fetchRows = (page = 0) => {
  return async function (dispatch) {
    dispatch(fetchRowsRequest());
    const url = `${config.api.baseUrl}/incidentType?page=${page}`;
    //const url = 'http://3.88.84.147:8080/swing-app/api/incidentType';
    try {
      const response = await axios.get(url);
      dispatch(fetchRowsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRowsFailure(error.message));
    }
  };
};
