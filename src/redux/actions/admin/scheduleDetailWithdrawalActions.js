import scheduleService from 'services/admin/schedule-service';
import * as types from 'redux/types/admin/scheduleDetailWithdrawalTypes';

export const fetchWithdrawalRowsRequest = () => ({
  type: types.FETCH_WITHDRAWALPOINT_ROWS_REQUEST,
  payload: true,
});

export const fetchWithdrawalRowsSuccess = (payload) => ({
  type: types.FETCH_WITHDRAWALPOINT_ROWS_SUCCESS,
  withdrawals: payload.withdrawals.data,
  totalWithdrawals: payload.withdrawals.total,
});

export const fetchRowsWithdrawalFailure = (errorRows) => ({
  type: types.FETCH_WITHDRAWALPOINT_ROWS_FAILURE,
  payload: errorRows,
});

export const getAllDetailWithdrawal = ({ codSchedule, page, pagesize, ubigee, withdrawalPointCode }) => async (dispatch) => {
  try {
    dispatch(fetchWithdrawalRowsRequest());
    const { data: withdrawals } = await scheduleService.getScheduleDetailWithdrawal({ codSchedule, page, pagesize, ubigee, withdrawalPointCode });
    console.log('putnos de retiro en actions', withdrawals.total);
    dispatch(fetchWithdrawalRowsSuccess({ withdrawals }));

  } catch (error) {
    dispatch(fetchRowsWithdrawalFailure(error));
    console.error(error);
  }
};
