import * as types from 'redux/types/admin/userTypes';
//import sca from '../../effects/services-control-administration';
import axios from 'axios';
//import config from '../../../config';

export const setInfo = (name) => ({
  type: types.SET_NAME,
  payload: name,
});

export const fetchPostsRequest = () => ({
  type: types.FETCH_POSTS_REQUEST,
  payload: true,
});

export const fetchPostSuccess = (posts) => ({
  type: types.FETCH_POSTS_SUCCESS,
  payload: posts,
});

export const fetchPostFailure = (error) => ({
  type: types.FETCH_POSTS_FAILURE,
  payload: error,
});

export const fetchPosts = (path) => {
  return function (dispatch) {
    dispatch(fetchPostsRequest());
    //${config.api.baseUrl}
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then((response) => {
        dispatch(fetchPostSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchPostFailure(error.message));
      });
    /*
    try {
      const response = await sca.getPosts(path);
      console.log('fetch successful');
      console.log(response.data);
      dispatch(fetchPostSuccess(response.data));
    } catch (error) {
      dispatch(fetchPostFailure(error));
      console.log(error);
    }
     */
  };
};
