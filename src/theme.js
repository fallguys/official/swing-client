import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const prevTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#29985E',
    },
    secondary: {
      main: '#BBDC2F',
    },
    tertiary: {
      main: '#303030',
      dark: '#1E864F',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

const theme = responsiveFontSizes(prevTheme);

export default theme;
