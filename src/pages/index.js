import styles from 'styles/Home.module.css';
import Default from '../layouts/Default';

export default function Index() {
  const title = 'Inicio';

  return (
    <Default title={title}>
      <div className={styles.container}>
        <main className={styles.main}>
          <p>Landscape</p>
        </main>
      </div>
    </Default>
  );
}
