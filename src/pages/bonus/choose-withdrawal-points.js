import React from 'react';
import { Grid } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Default from '../../layouts/Default';
import FormZone from '../../components/bonus/highComponents/choose-withdrawal-points/FormZone';
import RetirementPointsZone from '../../components/bonus/highComponents/choose-withdrawal-points/RetirementPointsZone';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  content: {
    height: 'calc(100vh - 120px)',
    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
}));

export default function chooseWithdrawalPoints(props) {
  const classes = useStyles();

  return (
    <Default title="Elegir puntos de retiro">
      <Box display="flex" alignItems="center" justifyContent="center" className={classes.content}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={4}
        >
          <Grid item md={5} xs={12}>
            <FormZone />
          </Grid>
          <Grid item xs={12} md={7}>
            <RetirementPointsZone />
          </Grid>
        </Grid>
      </Box>
    </Default>
  );
}
