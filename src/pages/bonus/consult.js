import React from 'react';
import Box from '@material-ui/core/Box';
import Default from '../../layouts/Default';
import ButtonsZone from '../../components/bonus/highComponents/consult/ButtonsZone';
import ConsultZone from '../../components/bonus/highComponents/consult/ConsultZone';
import ScheduleZone from '../../components/bonus/highComponents/consult/ScheduleZone';


export default function consult() {

  return (
    <Default title="Consulta">
      <ButtonsZone />
      <Box display="flex" alignItems="center" justifyContent="center">
        <Box mb={10}>
          <ConsultZone />
        </Box>
      </Box>
      <Box display="flex" justifyContent="center" alignItems="center">
        <ScheduleZone />
      </Box>
    </Default>
  );
}
