import React from 'react';
import Default from '../../layouts/Default';
import IncidentForm from '../../components/bonus/highComponents/incident/IncidentForm';

export default function incident(props) {
  return (
    <Default title="Incidentes">
      <IncidentForm />
    </Default>
  );
}
