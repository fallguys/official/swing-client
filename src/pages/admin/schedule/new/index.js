import Dashboard from 'layouts/Dashboard';
import React from 'react';
import InformationBox from 'components/admin/schedule/InformationBox';
import ConfigurationBox from 'components/admin/schedule/ConfigurationBox';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';

function ScheduleNew() {
  return (
    <Dashboard title="home" headers={['Cronograma', 'Nuevo Cronograma']}>
      <InformationBox />
      <Typography variant="h6" gutterBottom>
        Configuración
      </Typography>
      <Divider />
      <ConfigurationBox />
    </Dashboard>
  );
}

export default ScheduleNew;
