import Dashboard from 'layouts/Dashboard';
import React from 'react';
import Tabs from 'components/admin/tabs/HistoryTabs';
import { Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
}));

function ScheduleHistory() {
  const classes = useStyles();
  return (
    <Dashboard title="historial de cronogramas">
      <Divider />
      <Tabs className={classes.root} />
    </Dashboard>
  );
}

export default ScheduleHistory;
