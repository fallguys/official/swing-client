import Dashboard from 'layouts/Dashboard';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import DetailScheduleImage from 'components/admin/schedule/DetailSchedule/DetailScheduleImage';
import DetailScheduleInformation from 'components/admin/schedule/DetailSchedule/DetailScheduleInformation';
import { useDispatch, useSelector } from 'react-redux';
import { getAllDetailInformationBox } from 'redux/actions/admin/scheduleDetailInformationBoxActions';

function ScheduleDetail(props) {
  const dispatch = useDispatch();
  const informationBox = useSelector((state) => state.scheduleDetailInformationBox.informationBox);
  const router = useRouter();
  // const { id } = router.query;
  // console.log(id);
  const [queryId, setQueryId] = useState(null);

  useEffect(() => {
    if (router && router.query) {
      setQueryId(router.query.id);
      dispatch(getAllDetailInformationBox(router.query.id));
      console.log('EFFECTS', router.query.id);
    };
  }, [router]);

  console.log('INFORMATION BOX IN ID', informationBox);
  const inProcess = informationBox?.state === 'PROCESSING';

  const { codSchedule } = props;
  return (
    <Dashboard title="historial de cronogramas" headers={['Cronograma', 'Historial']}>
      { inProcess ? <DetailScheduleImage /> : <DetailScheduleInformation informationBox={informationBox} codSchedule={codSchedule} /> }
    </Dashboard>
  );
}

ScheduleDetail.getInitialProps = (ctx) => {
  const codSchedule = ctx.query.id;
  return { codSchedule };
};

export default ScheduleDetail;
