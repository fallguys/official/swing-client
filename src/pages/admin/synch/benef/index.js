import Dashboard from 'layouts/Dashboard';
import React from 'react';
import Chips from 'components/Synchro/SynchChips';
import DataTable from 'components/DataTable';
import Dropdown from 'components/Synchro/DropdownList';
import SynchSearch from 'components/Synchro/SynchSearch';
import SynchDateSearch from 'components/Synchro/SynchDateSearch';
import SynchButton from 'components/Synchro/SynchButton';
import { connect } from 'react-redux';
import { fetchRowsBeneficiaries } from 'redux/actions/admin/synchroObtainActions';
import { formatOnlyDate } from 'utils/format/timer';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { SnackbarProvider } from 'notistack';
import withStyles from '@material-ui/core/styles/withStyles';
import { compose } from 'redux';

const useStyles = ((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
}));

const columns = [
  { id: 'code', label: 'Código', minWidth: 40 },
  { id: 'ubigee', label: 'Ubigeo', minWidth: 100 },
  { id: 'department', label: 'Departamento', minWidth: 100 },
  { id: 'province', label: 'Provincia', minWidth: 100 },
  { id: 'district', label: 'Distrito', minWidth: 100 },
  { id: 'registerDate', label: 'Fecha de registro', minWidth: 120, format: (value) => formatOnlyDate(value) },
  { id: 'updatedDate', label: 'Fecha de actualización', minWidth: 100, format: (value) => formatOnlyDate(value) },
  { id: 'state', label: 'Estado', minWidth: 50, format: (value) => (value === 'ACTIVE' ? 'ACTIVO' : 'INACTIVO') },
];

class Benef extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pageSize) {
    const { getBeneficiaries } = this.props;
    getBeneficiaries(page, pageSize);
  }

  componentDidMount() {
    const { getBeneficiaries } = this.props;
    getBeneficiaries();
  }

  render() {
    const { beneficiaries } = this.props;
    const { classes } = this.props;
    const notistackRef = React.createRef();

    return (
      <SnackbarProvider
        ref={notistackRef}
        maxSnack={3}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        autoHideDuration={3000}
        action={(key) => (
          <IconButton key={key} onClick={() => { notistackRef.current.closeSnackbar(key); }} size="small">
            <CloseIcon fontSize="inherit" />
          </IconButton>
        )}
      >
        <Dashboard title="beneficaries" headers={['Sincronización', 'Beneficiarios']}>
          <Chips date={formatOnlyDate(beneficiaries.rows.lastUpdated)} cantActives={beneficiaries.rows.totalActive} />
          <div className={classes.root}>
            <SynchSearch text="Buscar por código, departamento, provincia o distrito" />
            <div className={classes.divider} />
            <SynchDateSearch />
            <Dropdown />
          </div>
          <DataTable
            rows={beneficiaries.rows.beneficiariesPaginator?.data}
            columns={columns}
            total={beneficiaries.rows.totalActive}
            pageSize={10}
            onChangePage={this.handleChangePage}
          />
          <SynchButton name="Cargar Beneficiarios" type="beneficiarios" />
        </Dashboard>
      </SnackbarProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    beneficiaries: state.synchroObtain,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBeneficiaries: (page, pageSize) => dispatch(fetchRowsBeneficiaries(page, pageSize)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(Benef);
