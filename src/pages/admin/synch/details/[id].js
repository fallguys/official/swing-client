import Dashboard from 'layouts/Dashboard';
import React, { useEffect, useState } from 'react';
import StateSynchroInfo from 'components/Synchro/detail/stateSynchroInfo';
import InformationBox from 'components/Synchro/detail/informationBox';
import Results from 'components/Synchro/detail/results';
import { useRouter } from 'next/router';

function Details(props) {
  const router = useRouter();
  const { id } = router.query;
  const [queryId, setQueryId] = useState(null);

  useEffect(() => {
    if (router && router.query) {
      setQueryId(router.query.id);
      //dispatch(getAllDetailInformationBox(router.query.id));
    };
  }, [router]);

  const { codSynchro } = props;
  return (
    <Dashboard title="Detalles de sincronización" headers={['Sincronización', 'Detalles de sincronización', router.query.id]}>
      <StateSynchroInfo idSynchro={codSynchro} />
      <InformationBox idSynchro={codSynchro} />
      <Results idSynchro={codSynchro} />
    </Dashboard>
  );
}

Details.getInitialProps = (ctx) => {
  const codSynchro = ctx.query.id;
  return { codSynchro };
};

export default Details;
