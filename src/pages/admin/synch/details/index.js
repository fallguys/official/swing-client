import Dashboard from 'layouts/Dashboard';
import React from 'react';
import StateSynchroInfo from 'components/Synchro/detail/stateSynchroInfo';
import InformationBox from 'components/Synchro/detail/informationBox';
import Results from 'components/Synchro/detail/results';

function Details() {
  return (
    <Dashboard title="Detalles de sincronización">
      <StateSynchroInfo />
      <InformationBox />
      <Results />
    </Dashboard>
  );
}

export default Details;
