import Dashboard from 'layouts/Dashboard';
import React from 'react';
import Chips from 'components/Synchro/SynchChips';
import Dropdown from 'components/Synchro/DropdownList';
import SynchSearch from 'components/Synchro/SynchSearch';
import SynchDateSearch from 'components/Synchro/SynchDateSearch';
import SynchButton from 'components/Synchro/SynchButton';
import { connect } from 'react-redux';
import { fetchRowsWithdrawals } from 'redux/actions/admin/synchroObtainActions';
import DataTable from 'components/DataTable';
import { formatOnlyDate } from 'utils/format/timer';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { SnackbarProvider } from 'notistack';
import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';

const useStyles = ((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
}));

const columns = [
  { id: 'code', label: 'Código', minWidth: 10 },
  { id: 'agency', label: 'Agencia', minWidth: 100 },
  { id: 'ubigee', label: 'Ubigeo', minWidth: 30 },
  { id: 'department', label: 'Depart.', minWidth: 60 },
  { id: 'province', label: 'Provincia', minWidth: 50 },
  { id: 'district', label: 'Distrito', minWidth: 60 },
  { id: 'address', label: 'Dirección', minWidth: 100 },
  { id: 'hoursWeekday', label: 'Horario L-V', minWidth: 60 },
  { id: 'hoursWeekend', label: 'Horario Sábado', minWidth: 60 },
  { id: 'registerDate', label: 'Fecha de registro', minWidth: 45, format: (value) => formatOnlyDate(value) },
  { id: 'updatedDate', label: 'Fecha de actualización', minWidth: 45, format: (value) => formatOnlyDate(value) },
  { id: 'state', label: 'Estado', minWidth: 60, format: (value) => (value === 'ACTIVE' ? 'ACTIVO' : 'INACTIVO') },
];

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pageSize) {
    const { getPoints } = this.props;
    getPoints(page, pageSize);
  }

  componentDidMount() {
    const { getPoints } = this.props;
    getPoints();
  }

  render() {
    const { points } = this.props;
    const { classes } = this.props;
    const notistackRef = React.createRef();

    return (
      <SnackbarProvider
        ref={notistackRef}
        maxSnack={3}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        autoHideDuration={3000}
        action={(key) => (
          <IconButton key={key} onClick={() => { notistackRef.current.closeSnackbar(key); }} size="small">
            <CloseIcon fontSize="inherit" />
          </IconButton>
        )}
      >
        <Dashboard title="points" headers={['Sincronización', 'Puntos de retiro']}>
          <Chips date={formatOnlyDate(points.rows.lastUpdated)} cantActives={points.rows.totalActive} />
          <div className={classes.root}>
            <SynchSearch text="Buscar por código, departamento, provincia o distrito" />
            <div className={classes.divider} />
            <SynchDateSearch />
            <Dropdown />
          </div>
          <DataTable
            rows={points.rows.pointsPaginator?.data}
            columns={columns}
            total={points.rows.totalActive}
            pageSize={10}
            onChangePage={this.handleChangePage}
          />
          <SynchButton name="Cargar Puntos de Retiro" type="puntos de retiro" />
        </Dashboard>
      </SnackbarProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    points: state.synchroObtain,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPoints: (page, pageSize) => dispatch(fetchRowsWithdrawals(page, pageSize)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(Points);
