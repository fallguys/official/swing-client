import Dashboard from 'layouts/Dashboard';
import React, { useState } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import SynchLoadInfo from 'components/Synchro/SynchLoadInfo';
import SynchIconButton from 'components/Synchro/SynchIconButton';
import SynchDropzone from 'components/Synchro/SynchDropzone';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginTop: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5),
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
  space: {
    marginTop: theme.spacing(4),
  },
}));

function Load() {
  const [name, setName] = useState('');
  const classes = useStyles();
  return (
    <Dashboard title="sincro datos" headers={['Sincronización', 'Carga de datos']}>
      <div className={classes.root}>
        Carga el archivo con los datos a sincronizar: puntos de retiro, beneficiarios o zonas de contagio.
        <div className={classes.divider} />
        <SynchIconButton />
      </div>
      <SynchLoadInfo user={name} />
      <div className={classes.space} />
      <SynchDropzone />
    </Dashboard>
  );
}
export default Load;
