import Dashboard from 'layouts/Dashboard';
import React from 'react';
import Chips from 'components/Synchro/SynchChips';
import Dropdown from 'components/Synchro/DropdownList';
import SynchSearch from 'components/Synchro/SynchSearch';
import SynchDateSearch from 'components/Synchro/SynchDateSearch';
import { compose } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';
import Router from 'next/router';
import SynchroDataTable from 'components/Synchro/dataTable';
import { formatOnlyDate, formatOnlyTime } from 'utils/format/timer';
import { fetchRowsHistorial } from 'redux/actions/admin/synchroObtainActions';

export const columns = [
  { id: 'id', label: 'Código', minWidth: 10 },
  { id: 'responsible', label: 'Responsable', minWidth: 80 },
  { id: 'successfullRegistries', label: 'Resultados de la sincronización', minWidth: 140, format: (value) => { return value !== null ? `${value} registros sincronizados` : '0 registros sincronizados' ; } },
  { id: 'date', label: 'Fecha de sincronización', minWidth: 60, format: (value) => `${formatOnlyDate(value)} - ${formatOnlyTime(value)}` },
  { id: 'fileType', label: 'Recurso', minWidth: 60, format: (value) => { return value === 'BENEFICIARY' ? 'Beneficiarios' : value === 'WITHDRAWAL_POINT' ? 'Puntos de Retiro' : 'Zonas de Contagio'; } },
  { id: 'state', label: 'Estado', minWidth: 30, format: (value) => { return value === 'COMPLETED' ? 'COMPLETADO' : value === 'IN_PROCESS' ? 'EN PROCESO' : value === 'INCOMPLETE' ? 'INCOMPLETO' : 'FALLIDO'; } },
];

const useStyles = ((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
}));

class History extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pageSize) {
    const { getHistory } = this.props;
    getHistory(page, pageSize);
  }

  handleSelected(event) {
    console.log(event.target.id);
    Router.push('/admin/synch/details/[id]', `/admin/synch/details/${event.target.id}`);
  }

  componentDidMount() {
    const { getHistory } = this.props;
    getHistory();
  }

  render() {
    const { classes } = this.props;
    const { history } = this.props;

    return (
      <Dashboard title="beneficaries" headers={['Sincronización', 'Historial de Sincronización']}>
        <Chips date={formatOnlyDate(history.rows?.lastUpdated)} cantActives={history.rows?.total} />
        <div className={classes.root}>
          <SynchSearch />
          <div className={classes.divider} />
          <SynchDateSearch />
          <Dropdown />
        </div>
        <SynchroDataTable
          columns={columns}
          rows={history.rows?.data}
          total={history.rows?.total}
          pageSize={10}
          handleSelected={this.handleSelected}
          onChangePage={this.handleChangePage}
        />
      </Dashboard>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    history: state.synchroObtain,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHistory: (page, pageSize) => dispatch(fetchRowsHistorial(page, pageSize)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(History);
