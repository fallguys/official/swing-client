import Dashboard from 'layouts/Dashboard';
import React from 'react';
import Chips from 'components/Synchro/SynchChips';
import Dropdown from 'components/Synchro/DropdownList';
import SynchSearch from 'components/Synchro/SynchSearch';
import SynchDateSearch from 'components/Synchro/SynchDateSearch';
import SynchButton from 'components/Synchro/SynchButton';
import DataTable from 'components/DataTable';
import { connect } from 'react-redux';
import { formatOnlyDate } from 'utils/format/timer';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { SnackbarProvider } from 'notistack';
import withStyles from '@material-ui/core/styles/withStyles';
import { compose } from 'redux';
import { fetchRowsContagion } from 'redux/actions/admin/synchroObtainActions';

const useStyles = ((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginTop: theme.spacing(2),
  },
  divider: {
    display: 'flex',
    flex: 1,
  },
}));

const columns = [
  { id: 'id', label: 'Código', minWidth: 40 },
  { id: 'ubigee', label: 'Ubigeo', minWidth: 100 },
  { id: 'department', label: 'Departamento', minWidth: 100 },
  { id: 'province', label: 'Provincia', minWidth: 50 },
  { id: 'district', label: 'Distrito', minWidth: 120 },
  { id: 'quantity', label: 'Porcentaje (%)', minWidth: 50 },
  { id: 'registerDate', label: 'Fecha de registro', minWidth: 100, format: (value) => formatOnlyDate(value) },
  { id: 'updatedDate', label: 'Fecha de actualización', minWidth: 60, format: (value) => formatOnlyDate(value) },
];

class Areas extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  handleChangePage(page, pageSize) {
    const { getAreas } = this.props;
    getAreas(page, pageSize);
  }

  componentDidMount() {
    const { getAreas } = this.props;
    getAreas();
  }

  render() {
    const { areas } = this.props;
    const { classes } = this.props;
    const notistackRef = React.createRef();

    return (
      <SnackbarProvider
        ref={notistackRef}
        maxSnack={3}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        autoHideDuration={3000}
        action={(key) => (
          <IconButton key={key} onClick={() => { notistackRef.current.closeSnackbar(key); }} size="small">
            <CloseIcon fontSize="inherit" />
          </IconButton>
        )}
      >
        <Dashboard title="areas" headers={['Sincronización', 'Zonas de Contagio']}>
          <Chips date={formatOnlyDate(areas.rows.lastUpdated)} cantActives={areas.rows.totalActive} />
          <div className={classes.root}>
            <SynchSearch text="Buscar por código, departamento, provincia o distrito" />
            <div className={classes.divider} />
            <SynchDateSearch />
            <Dropdown />
          </div>
          <DataTable
            rows={areas.rows.contagionAreasPaginator?.data}
            columns={columns}
            total={areas.rows.totalActive}
            pageSize={10}
            onChangePage={this.handleChangePage}
          />
          <SynchButton name="Cargar Zonas de Contagio" type="zonas de contagio" />
        </Dashboard>
      </SnackbarProvider>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    areas: state.synchroObtain,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAreas: (page, pageSize) => dispatch(fetchRowsContagion(page, pageSize)),
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps),
)(Areas);
