import Dashboard from 'layouts/Dashboard';
import Search from 'components/Synchro/SynchSearch';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Dropdown from 'components/Synchro/DropdownList';
import DataTable from 'components/DataTable';
import DateSearch from 'components/Synchro/SynchDateSearch';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchRows } from '../../../../redux/actions/admin/incidentActions';
import { formatOnlyDate, formatOnlyTime } from '../../../../utils/format/timer';

const useStyles = makeStyles((theme) => ({
  search: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
}));

function incident() {
  const classes = useStyles();
  const incident = useSelector((state) => state.incident);
  const dispatch = useDispatch();
  const columns = [
    { id: 'id', label: 'Código de Beneficiario', minWidth: 100 },
    { id: 'type', label: 'Tipo de Indicencia', minWidth: 100 },
    { id: 'message', label: 'Mensaje', minWidth: 100 },
    { id: 'date', label: 'Fecha de Reporte', minWidth: 100, format: (value) => { return `${formatOnlyDate(value)} - ${formatOnlyTime(value)}`; } },
  ];
  /*const rows = [
    { num: 1, cod: '000320', incidentType: 'Mi punto de retiro no existe', message: 'La dirección está mal tipeada', dateReport: '05/11/2020' },
    { num: 2, cod: '000351', incidentType: 'Mi punto de retiro no existe', message: 'La dirección está mal tipeada', dateReport: '05/11/2020' },
    { num: 3, cod: '000356', incidentType: 'Mi punto de retiro no existe', message: 'La dirección está mal tipeada', dateReport: '05/11/2020' },
  ];*/
  /*const rowss = [
    { 'id': 1, 'message': 'incident', 'reportedDate': null, 'type': { 'id': 1, 'name': 'string', 'description': 'string', 'createdAt': '2020-11-14T18:22:12.553+00:00', 'state': 'ENABLED' } },
  ];*/
  const handleChangePage = (page, pagesize) => {
    dispatch(fetchRows(page));
  };
  useEffect(() => {
    dispatch(fetchRows());
  }, []);
  return (
    <Dashboard title="home" headers={['Incidencias', 'Incidencias Reportadas']}>
      <div>
        <Divider />
        <Grid container spacing={1} className={classes.search}>
          <Grid item xs={6}>
            <Search text="Buscar por código de beneficiario y Tipo de incidencia" />
          </Grid>
          <Grid item xs={3}>
            <DateSearch />
          </Grid>
        </Grid>
        <DataTable
          rows={incident?.rows.data}
          total={incident?.rows.total}
          columns={columns}
          onChangePage={handleChangePage}
        />
      </div>
    </Dashboard>

  );
}

export default incident;
