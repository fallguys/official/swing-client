import Dashboard from 'layouts/Dashboard';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Search from 'components/Synchro/SynchSearch';
import Dropdown from 'components/Synchro/DropdownList';
import DataTable from 'components/DataTable';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import React, { useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchRows,
  postIncidentType,
  setDescription,
  setName,
} from '../../../../redux/actions/admin/incidentTypeActions';

const useStyles = makeStyles((theme) => ({
  search: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  footer_register: {
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    width: '100%',
    alignItems: 'center',
  },
  content_body: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  container: {
    width: '600px',
    display: 'block',
    margin: 'auto',
  },
}));

function type() {
  const classes = useStyles();
  const incidentType = useSelector((state) => state.incidentType);
  const dispatch = useDispatch();
  /*const [dataType, setDataType] = useState({
    name: '',
    description: '',
  });*/
  const columns = [
    { id: 'id', label: 'Código', minWidth: 40 },
    { id: 'name', label: 'Nombre', minWidth: 100 },
    { id: 'description', label: 'Descripción', minWidth: 100 },
  ];
  /*const rows = [
    { num: 1, cod: '00001', name: 'Mi punto de retiro no existe', description: 'La dirección está mal tipeada' },
    { num: 2, cod: '00002', name: 'Idea 2', description: 'La descripción de idea 2' },
    { num: 3, cod: '00003', name: 'Idea 3', description: 'La descripción de idea 3' },
  ];*/
  const [open, setOpen] = React.useState(false);
  const putOpen = () => {
    setOpen(true);
  };
  const handleChangePage = (page, pagesize) => {
    dispatch(fetchRows(page));
  };
  const putClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    dispatch(fetchRows());
  }, []);
  /*const registerType = () => {
    console.log(dataType.name, dataType.description, rows.length);
    const data = {
      num: (rows.length + 1),
      code: (rows[rows.length - 1].cod + 1),
      name: dataType.name,
      description: dataType.description,
    };
    rows.push(data);
    setOpen(false);
    console.log(rows);
  };*/
  /*const getInputsType = (event) => {
    setName({
      ...dataType,
      [event.target.name]: event.target.value,
    });
  };*/

  return (
    <Dashboard title="home" headers={['Incidencias', 'Tipos de incidencias']}>
      <div>
        <Divider />
        <Grid container spacing={1} className={classes.search}>
          <Grid item xs={8}>
            <Search text="Buscar por código y Nombre" />
          </Grid>
          <Grid item xs={2}>
            <Button variant="contained" color="primary" onClick={putOpen} disableElevation>
              Nuevo Tipo
            </Button>
          </Grid>
        </Grid>
        <div>
          {open && (
            <div className={classes.container}>
              <Typography variant="h5" gutterBottom align="center">
                Registrar nuevo tipo de incidente
              </Typography>
              <Divider />
              <div className={classes.content_body}>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <Typography variant="h6">
                      Nombre
                    </Typography>
                  </Grid>
                  <Grid item xs={8}>
                    <TextField
                      onChange={(event) => dispatch(setName(event.target.value))}
                      variant="outlined"
                      fullWidth={true}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant="h6">
                      Descripción
                    </Typography>
                  </Grid>
                  <Grid item xs={8}>
                    <TextField
                      onChange={(event) => dispatch(setDescription(event.target.value))}
                      variant="outlined"
                      fullWidth={true}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={classes.footer_register}>
                <Grid container spacing={1}>
                  <Grid item xs={3}> </Grid>
                  <Grid item xs={3}>
                    <Button onClick={putClose} color="primary">
                      Cancelar
                    </Button>
                  </Grid>
                  <Grid item xs={3}>
                    <Button
                      onClick={async () => {
                        await dispatch(postIncidentType({
                          name: incidentType.name,
                          description: incidentType.description,
                        }));
                        await dispatch(fetchRows());
                        putClose();
                      }}
                      color="primary"
                      variant="contained"
                    >
                      Registrar
                    </Button>
                  </Grid>
                  <Grid item xs={3}> </Grid>
                </Grid>
              </div>
            </div>
          )}
        </div>
        <DataTable
          rows={incidentType.rows.data}
          columns={columns}
          total={incidentType.rows.total}
          onChangePage={handleChangePage}
        />
      </div>
    </Dashboard>

  );
}
export default type;

