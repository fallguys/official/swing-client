import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import FormLogin from 'components/admin/login';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(20),
    alignItems: 'center',
    width: '100%',
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  },
  text: {
    display: 'flex',
    alignItems: 'center',
  },
  formRegister: {
    padding: '40px',
  },
}));

function login() {
  const classes = useStyles();
  return (
    <div>
      <Grid container style={{ minHeight: '100vh' }}>
        <Grid container item xs={12} sm={4} className={classes.formRegister}>
          <FormLogin />
        </Grid>
        <Grid item xs={12} sm={8}>
          <img src="/imgs/login.png" alt="brand" className={classes.image} />
        </Grid>
      </Grid>
    </div>
  );
}

export default login;
