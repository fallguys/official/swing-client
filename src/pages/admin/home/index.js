import Dashboard from 'layouts/Dashboard';
import React, { useEffect } from 'react';
import ScheduleImage from 'components/admin/home/ScheduleImage';
import ButtonsZoneSynch from 'components/admin/home/ButtonsZoneSynch';
import { useDispatch, useSelector } from 'react-redux';
import { getHomeDetails } from 'redux/actions/admin/homeActions';

function Home({ user, setInfo }) {
  const home = useSelector((state) => state.home.home);
  const dispatch = useDispatch();
  const value = home.data?.scheduleActive;
  useEffect(() => {
    dispatch(getHomeDetails());
  }, []);

  return (
    <Dashboard title="home" headers={['Dashboard', 'Principal']}>
      { value ? <ScheduleImage existSchedule={home.data?.scheduleActive} /> : <ButtonsZoneSynch valueBenef={home.data?.beneficiarySynchroActive} valueWithdrawal={home.data?.withdrawalPointSynchroActive} valueContagion={home.data?.contagionAreaSynchroActive} /> }
    </Dashboard>
  );
}

export default Home;
