import React from 'react';
import Default from 'layouts/Default';
import Grid from '@material-ui/core/Grid';
import WithdrawalPanel from 'components/simulation/withdrawalPanel';
import BeneficiariePanel from 'components/simulation/beneficiariePanel';
import Typography from '@material-ui/core/Typography';
import Clock from 'components/simulation/clock';
import { SnackbarProvider } from 'notistack';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    alignItems: 'center',
  },
  snackBar: {
    success: { backgroundColor: 'purple' },
    error: { backgroundColor: 'blue' },
    warning: { backgroundColor: 'green' },
    info: { backgroundColor: 'yellow' },
  },
}));

export default function Login() {
  const classes = useStyles();

  return (
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      autoHideDuration={2500}
      classes={{
        variantSuccess: classes.success,
        variantError: classes.error,
        variantWarning: classes.warning,
        variantInfo: classes.info,
      }}
    >
      <Default title="Simulación">
        <div className={classes.root}>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="center"
            spacing={0}
          >
            <Grid item xs={6}>
              <Typography variant="h3" align="left">
                Simulador de bancos
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Clock />
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="center"
            spacing={3}
          >
            <WithdrawalPanel />
            <BeneficiariePanel />
          </Grid>
        </div>
      </Default>
    </SnackbarProvider>
  );
}
