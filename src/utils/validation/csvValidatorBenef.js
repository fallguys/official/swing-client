const csvValidatorBenef = [
  { name: 'CO_HOGAR', selector: 'CO_HOGAR' },
  { name: 'UBIGEO', selector: 'UBIGEO' },
  { name: 'DE_DEPARTAMENTO', selector: 'DE_DEPARTAMENTO' },
  { name: 'DE_PROVINCIA', selector: 'DE_PROVINCIA' },
  { name: 'DE_DISTRITO', selector: 'DE_DISTRITO' },
  { name: 'DE_GENERO', selector: 'DE_GENERO' },
  { name: 'CO_RESTRI', selector: 'CO_RESTRI' },
  { name: 'FLAG_PADRON_OLD', selector: 'FLAG_PADRON_OLD' },
  { name: 'FLAG_DISCAP_SEVERA', selector: 'FLAG_DISCAP_SEVERA' },
  { name: 'FLAG_MAYEDAD', selector: 'FLAG_MAYEDAD' },
];

export default csvValidatorBenef;
