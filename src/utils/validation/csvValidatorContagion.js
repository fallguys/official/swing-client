const csvValidatorContagion = [
  { name: 'FECHA_CORTE', selector: 'FECHA_CORTE' },
  { name: 'UUID', selector: 'UUID' },
  { name: 'DEPARTAMENTO', selector: 'DEPARTAMENTO' },
  { name: 'UBIGEO', selector: 'UBIGEO' },
  { name: 'PROVINCIA', selector: 'PROVINCIA' },
  { name: 'DISTRITO', selector: 'DISTRITO' },
  { name: 'METODODX', selector: 'METODODX' },
  { name: 'EDAD', selector: 'EDAD' },
  { name: 'SEXO', selector: 'SEXO' },
  { name: 'FECHA_RESULTADO', selector: 'FECHA_RESULTADO' },
];

export default csvValidatorContagion;
