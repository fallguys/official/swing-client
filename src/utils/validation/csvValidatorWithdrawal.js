const csvValidatorWithdrawal = [
  { name: 'CODIGO', selector: 'CODIGO' },
  { name: 'NOMBRE DE LA AGENCIA', selector: 'NOMBRE DE LA AGENCIA' },
  { name: 'UBIGEO', selector: 'UBIGEO' },
  { name: 'DEPARTAMENTO', selector: 'DEPARTAMENTO' },
  { name: 'PROVINCIA', selector: 'PROVINCIA' },
  { name: 'DISTRITO', selector: 'DISTRITO' },
  { name: 'DIRECCIÓN', selector: 'DIRECCIÓN' },
  { name: 'HORA_INICIO_L_V', selector: 'HORA_INICIO_L_V' },
  { name: 'HORA_FIN_L_V', selector: 'HORA_FIN_L_V' },
  { name: 'HORA_INICIO_SABADO', selector: 'HORA_INICIO_SABADO' },
  { name: 'HORA_FIN_SABADO', selector: 'HORA_FIN_SABADO' },
];

export default csvValidatorWithdrawal;
