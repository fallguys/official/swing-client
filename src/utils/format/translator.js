import moment from "moment";

export function translateScheduleState(state) {
  //TODO: CAMBIAR ESTADO DISCARED POR DISCARTED, ACUERDO CON BACKEND
  switch (state) {
    case 'AVAILABLE':
      return 'DISPONIBLE';
    case 'DISCARDED':
      return 'DESCARTADO';
    case 'CANCELED':
      return 'CANCELADO';
    case 'PROCESSING':
      return 'EN PROCESO';
    case 'PROGRAMMED':
      return 'PROGRAMADO';
    case 'ACTIVE':
      return 'VIGENTE';
    case 'FINISHED':
      return 'FINALIZADO';
    default:
      return '';
  }
}

export function translateSynchType(type) {
  switch (type) {
    case 'CONTAGION_AREA':
      return 'Zonas de Contagio';
    case 'BENEFICIARY':
      return 'Beneficiarios';
    case 'WITHDRAWAL_POINT':
      return 'Puntos de Retiro';
    default:
      return '';
  }
}
