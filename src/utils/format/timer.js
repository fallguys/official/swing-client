import moment from 'moment';

export function formatOnlyDate(date) {
  if (date === null) { return '---'; }
  return moment(date).format('DD/MM/YYYY');
}

export function formatYear(date) {
  if (date === null) { return '---'; }
  return moment(date).format('YYYY/MM/DD');
}

export function formatOnlyTime(dateTime) {
  if (dateTime === null) { return '---'; }
  return moment(dateTime).format('h:mm a');
}

export function formatHoursInterval(date) {
  if (date === null) { return '---'; }
  return `${moment(date.initialDate).format('HH:mm')} - ${moment(date.finalDate).format('HH:mm')}`;
}

export function formatDateDatePicker(date) {
  if (date === null) { return '---'; }
  return moment(date).format('YYYY-MM-DD');
}
