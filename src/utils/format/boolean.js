
export function validationBoolean(value) {
  if (value === true) {
    return 'Si';
  }
  if (value === false) {
    return 'No';
  }
}

export function validationNumber(number) {
  if (number === null) {
    return 0;
  }
  return number;
}
