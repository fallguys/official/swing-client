export const rowsBeneficiarie = [
  ['Codigo', 'Genero', 'Preferencia', 'Ubigeo', 'Departamento', 'Provincia', 'Distrito', 'Fecha de registro', 'Fecha de acutalizacion', 'Estado'],
];
export const rowsWithdrawal = [
  ['Codigo', 'Agencia', 'Ubigeo', 'Departamento', 'Provincia', 'Distrito', 'Direccion', 'Horario L-V', 'Horario Sábado', 'Fecha de registro', 'Fecha de acutalizacion', 'Estado'],
];
export const rowsContagion = [
  ['Codigo', 'Ubigeo', 'Departamento', 'Provincia', 'Distrito', 'Fecha de registro', 'Fecha de acutalizacion'],
];
