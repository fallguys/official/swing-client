import moment from 'moment';
import { formatDateDatePicker } from './format/timer';

export default function progressPercentage(initialDay, finalDay) {
  const current = moment().format('YYYY-MM-DD');
  const currentDay = moment(current);

  //primer caso, cuando current está en medio
  if ((currentDay.diff(initialDay, 'days') >= 0) && (finalDay.diff(currentDay, 'days') >= 0)) {
    const cantDays = finalDay.diff(initialDay, 'days');
    const cantDaysCurrent = currentDay.diff(initialDay, 'days');
    return (cantDaysCurrent * 100) / cantDays;
  }
  //segundo caso, cuando current está antes de inicial
  if ((initialDay.diff(currentDay, 'days')) >= 0) {
    return 0;
  }
  //tercer caso, cuando current está después de final
  if ((currentDay.diff(finalDay, 'days')) >= 0) {
    return 100;
  }
  return 0;
};

export function getDayBefore(value) {
  const dateToday = new Date();
  const today = formatDateDatePicker(dateToday);
  const dayBefore = moment(today).subtract(1, 'd').format('YYYY-MM-DD');
  const valor = value === true;
  console.log('VALOR', valor);
  if (value) {
    return dayBefore;
  };
  return today;
};
