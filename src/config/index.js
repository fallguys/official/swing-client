
const config = {
  app: {
    name: process.env.NEXT_PUBLIC_APP_NAME,
    allowScheduleInPast: process.env.NEXT_PUBLIC_ALLOW_REGISTER_SCHEDULE_IN_PAST,
    incidentID: process.env.NEXT_PUBLIC_APP_INCIDENT_ID,
    adminName: process.env.NEXT_PUBLIC_ADMINISTRATOR_NAME,
  },
  api: {
    baseUrl: process.env.NEXT_PUBLIC_API_URL,
  },
};

export default config;
