FROM node:14.15-alpine as base
WORKDIR /app


## Download dependencies to build
FROM base as dependencies
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile


## Build project with dependencies
FROM base AS builder
ENV NODE_ENV=production
WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules /app/node_modules
RUN yarn build


## Release project app
FROM base AS release

EXPOSE 3000
ENV NODE_ENV=production

WORKDIR /app
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules

CMD ["node_modules/.bin/next", "start"]
